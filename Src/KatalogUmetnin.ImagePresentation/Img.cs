using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace KatalogUmetnin.ImagePresentation
{
	public class Img
	{
		private static string _thumbLocation = "";

		private static string _originalLocation = "";

		private static string _compositePrintLocation = "";

		private static string _compositePreviewLocation = "";

		private static string _imageArchiveLocation = "";        
        
        protected Size _imageSize;       

	    public static string ThumbLocation
		{
			get
			{
				return Img._thumbLocation;
			}
		}

		public static string OriginalLocation
		{
			get
			{
				return Img._originalLocation;
			}
		}

		public static string CompositePrintLocation
		{
			get
			{
				return Img._compositePrintLocation;
			}
		}

		public static string CompositePreviewLocation
		{
			get
			{
				return Img._compositePreviewLocation;
			}
		}

		public static string ImageArchiveLocation
		{
			get
			{
				return Img._imageArchiveLocation;
			}
		}
        		
		public string Name
		{
			get
			{
				return Path.GetFileNameWithoutExtension(this.FileName);
			}
		}				

		public System.Drawing.Size Size
		{
			get
			{
				return this._imageSize;
			}
		}				

		public string FileName
		{
			get;
			set;
		}

		public string FullFileName
		{
			get
			{
				return Path.Combine(Settings.Get.StoragePath, this.FileName);
			}
		}

		public string FullOriginalName
		{
			get
			{
				return Path.Combine(Img.OriginalLocation, this.FileName);
			}
		}

		public static void Initialize(string imgLocation)
		{
			Img._thumbLocation = Path.Combine(imgLocation, "Thumbnails");
			Img._originalLocation = Path.Combine(imgLocation, "Originals");
			Img._imageArchiveLocation = Path.Combine(imgLocation, "Archive");
			Img._compositePrintLocation = Path.Combine(imgLocation, "Composits\\Print");
			Img._compositePreviewLocation = Path.Combine(imgLocation, "Composits\\Preview");
			Img.CreateDirectory(imgLocation);
			Img.CreateDirectory(Img._thumbLocation);
			Img.CreateDirectory(Img._originalLocation);
			Img.CreateDirectory(Img._compositePrintLocation);
			Img.CreateDirectory(Img._compositePreviewLocation);
			Img.CreateDirectory(Img._imageArchiveLocation);
		}

		private static void CreateDirectory(string path)
		{
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
		}		

		public Image LoadData(bool InvalidateThumbs = false)
		{
            try {
                string origFile = Path.Combine(Img._originalLocation, this.FileName);
                if (!File.Exists(origFile)) {
                    File.Copy(this.FullFileName, origFile, true);
                }

                Image image = GetImage();
                _imageSize = image.Size;
                return image;
            }
            catch {
                return null;
            }
        }

        public Image GetImage()
        {
            MemoryStream rawData = new MemoryStream(File.ReadAllBytes(this.FullFileName));
            Image image = System.Drawing.Image.FromStream(rawData);
            return this.SetImageResolution(image, 96);
        }

        public Image GetThumbnailImage(int mw, int mh, bool InvalidateThumbs)
        {
            Thumbnail thumb = new Thumbnail(mw, mh, this.FullFileName, InvalidateThumbs);
            return thumb.GetImage();
        }

		public void Rotate(System.Drawing.RotateFlipType type)
		{
            {
                Image img = GetImage();
                img.RotateFlip(type);
                img.Save(this.FullFileName);
            }
            Settings.FreeGarbage();
            // UNCOM tole gre bolje
            this.LoadData(true);
            Settings.FreeGarbage();
        }

		private System.Drawing.Image SetImageResolution(System.Drawing.Image img, int res)
		{
			return this.SetImageResolution(img, res, res);
		}

		private System.Drawing.Image SetImageResolution(System.Drawing.Image img, int hRes, int vRes)
		{
			System.Drawing.Bitmap b = (System.Drawing.Bitmap)img;
			float desiredHRes = 96f;
			float desiredVRes = 96f;
			int cr = img.HorizontalResolution.CompareTo(desiredHRes);
			if (img.HorizontalResolution != desiredHRes || img.VerticalResolution != desiredVRes)
			{
				b.SetResolution(desiredHRes, desiredVRes);
			}
			return b;
		}		

		public static string GetCompositionByID(int id)
		{
			return Path.Combine(Img._compositePrintLocation, "Kompozicija_" + id.ToString() + ".png");
		}
	}
}
