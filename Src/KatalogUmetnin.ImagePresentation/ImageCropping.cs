﻿using KatalogUmetnin.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace KatalogUmetnin.ImagePresentation
{
	public class ImageCropping : System.Windows.Forms.Form
	{
		private bool maintaninAspect = false;

		private bool _selecting;

		private System.Drawing.Rectangle _selection;

		private System.Drawing.Point initSelection;

		private System.Drawing.Point GlobalStartPoint;

		private System.Drawing.Point GlobalLastPoint;

		private System.Drawing.Rectangle imageSelection;

        private System.Drawing.Rectangle absoluteSelection;

		private IContainer components = null;

		private System.Windows.Forms.PictureBox pictureBox1;

		private System.Windows.Forms.Button button1;

		private System.Windows.Forms.Button button2;
        private Button button4;
        private System.Windows.Forms.Button button3;

		private CanvasImg Img
		{
			get;
			set;
		}

		public System.Drawing.Image CroppedImage
		{
			get;
			set;
		}

		public ImageCropping(CanvasImg img)
		{
			this.InitializeComponent();
			this.Img = img;
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            Settings.SetTexts(this);
		}

		private void ImageCropping_Load(object sender, EventArgs e)
		{
			this.pictureBox1.Image = this.Img.GetImage();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			this.CroppedImage = this.pictureBox1.Image;
			base.DialogResult = System.Windows.Forms.DialogResult.OK;
		}

		private void button3_Click(object sender, EventArgs e)
		{
			base.DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		private void pictureBox1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Left)
			{
				if (this.isCurrentPositionInsideImageArea(e.Location))
				{
					this._selecting = true;
					this.initSelection = e.Location;
					this._selection = new System.Drawing.Rectangle(e.Location, default(System.Drawing.Size));
					this.GlobalStartPoint = System.Windows.Forms.Cursor.Position;
				}
			}
			else if (e.Button == System.Windows.Forms.MouseButtons.Right)
			{
				this._selecting = false;
				this._selection = default(System.Drawing.Rectangle);
				this.pictureBox1.Refresh();
			}
		}

		private void pictureBox1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (this.isCurrentPositionInsideImageArea(e.Location))
			{
				if (e.Button == System.Windows.Forms.MouseButtons.Left && this._selecting)
				{
					this.pictureBox1.Refresh();
					this._selecting = false;
					this.doCrop();
				}
			}
			else
			{
				this._selecting = false;
			}
		}

		private void doCrop()
		{
			if (this._selection.Width > 0)
			{
				this.imageSelection = this.returnImageRectangle(this._selection);
                if (this.absoluteSelection.Width == 0 && this.absoluteSelection.Height == 0)
                    this.absoluteSelection = this.imageSelection;
                else
                {
                    Rectangle selection = absoluteSelection;
                    double ratioW = (double)pictureBox1.Image.Width / absoluteSelection.Width;
                    double ratioH = (double)pictureBox1.Image.Height / absoluteSelection.Height;
                    ratioW = 1.0 / ratioW;
                    ratioH = 1.0 / ratioH;

                    selection.Width = (int)(imageSelection.Width * ratioW);
                    selection.Height = (int)(imageSelection.Height * ratioH);
                    selection.X += (int)(imageSelection.X * ratioW);
                    selection.Y += (int)(imageSelection.Y * ratioH);

                    absoluteSelection = selection;
                }
				this.pictureBox1.Image = System.Drawing.Image.FromStream(this.CropPicture(this.pictureBox1.Image, this.imageSelection));
				this._selection = default(System.Drawing.Rectangle);
			}
		}

        private void button4_Click(object sender, EventArgs e)
        {
            this.pictureBox1.Image = System.Drawing.Image.FromStream(this.CropPicture(this.Img.GetImage(), this.absoluteSelection));
        }

        public MemoryStream CropPicture(System.Drawing.Image imgPhoto, System.Drawing.Rectangle r)
		{
			return this.CropPicture(imgPhoto, r.Width, r.Height, r.X, r.Y);
		}

		public MemoryStream CropPicture(System.Drawing.Image imgPhoto, int targetW, int targetH, int targetX, int targetY)
		{
			System.Drawing.Bitmap bmpPhoto = new System.Drawing.Bitmap(targetW, targetH, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
			bmpPhoto.SetResolution(80f, 60f);
			System.Drawing.Graphics gfxPhoto = System.Drawing.Graphics.FromImage(bmpPhoto);
			gfxPhoto.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
			gfxPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
			gfxPhoto.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
			gfxPhoto.DrawImage(imgPhoto, new System.Drawing.Rectangle(0, 0, targetW, targetH), targetX, targetY, targetW, targetH, System.Drawing.GraphicsUnit.Pixel);
			MemoryStream mm = new MemoryStream();
			bmpPhoto.Save(mm, System.Drawing.Imaging.ImageFormat.Tiff);
			return mm;
		}

		private System.Drawing.Rectangle returnImageRectangle(System.Drawing.Rectangle rec)
		{
			System.Drawing.Point p = this.TranslateZoomMousePosition(rec.Location);
			System.Drawing.Point e = this.TranslateZoomMousePosition(new System.Drawing.Point(rec.X + rec.Width, rec.Y + rec.Height));
			return new System.Drawing.Rectangle(p, new System.Drawing.Size(e.X - p.X, e.Y - p.Y));
		}

		private System.Drawing.Rectangle returnRectangleFrom2Points(System.Drawing.Point p1, System.Drawing.Point p2)
		{
			System.Drawing.Rectangle rc = default(System.Drawing.Rectangle);
			if (p1.X <= p2.X)
			{
				rc.X = p1.X;
				rc.Width = p2.X - p1.X;
			}
			else
			{
				rc.X = p2.X;
				rc.Width = p1.X - p2.X;
			}
			if (p1.Y <= p2.Y)
			{
				rc.Y = p1.Y;
				rc.Height = p2.Y - p1.Y;
			}
			else
			{
				rc.Y = p2.Y;
				rc.Height = p1.Y - p2.Y;
			}
			return rc;
		}

		private void pictureBox1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (this.isCurrentPositionInsideImageArea(e.Location))
			{
				if ((this.GlobalLastPoint.X == System.Windows.Forms.Cursor.Position.X && this.GlobalLastPoint.Y == System.Windows.Forms.Cursor.Position.Y) || !this.maintaninAspect)
				{
					if (this._selecting)
					{
						System.Drawing.Rectangle r = this._selection;
						this._selection = this.returnRectangleFrom2Points(this.initSelection, new System.Drawing.Point(e.X, e.Y));
						this.pictureBox1.Refresh();
					}
				}
			}
		}

		private bool isCurrentPositionInsideImageArea(System.Drawing.Point p)
		{
			bool result;
			try
			{
				System.Drawing.Point tmp = this.TranslateZoomMousePosition(p);
				if (tmp.X < 0 || tmp.Y < 0)
				{
					result = false;
				}
				else if (tmp.X > this.pictureBox1.Image.Width || tmp.Y > this.pictureBox1.Image.Height)
				{
					result = false;
				}
				else
				{
					result = true;
				}
			}
			catch (Exception)
			{
				result = false;
			}
			return result;
		}

		protected System.Drawing.Point TranslateZoomMousePosition(System.Drawing.Point coordinates)
		{
			System.Drawing.Point result;
			if (this.pictureBox1.Image == null)
			{
				result = coordinates;
			}
			else if (this.pictureBox1.Width == 0 || this.pictureBox1.Height == 0 || this.pictureBox1.Image.Width == 0 || this.pictureBox1.Image.Height == 0)
			{
				result = coordinates;
			}
			else
			{
				float imageAspect = (float)this.pictureBox1.Image.Width / (float)this.pictureBox1.Image.Height;
				float controlAspect = (float)this.pictureBox1.Width / (float)this.pictureBox1.Height;
				float newX = (float)coordinates.X;
				float newY = (float)coordinates.Y;
				if (imageAspect > controlAspect)
				{
					float ratioWidth = (float)this.pictureBox1.Image.Width / (float)base.Width;
					newX *= ratioWidth;
					float scale = (float)this.pictureBox1.Width / (float)this.pictureBox1.Image.Width;
					float displayHeight = scale * (float)this.pictureBox1.Image.Height;
					float diffHeight = (float)this.pictureBox1.Height - displayHeight;
					diffHeight /= 2f;
					newY -= diffHeight;
					newY /= scale;
				}
				else
				{
					float ratioHeight = (float)this.pictureBox1.Image.Height / (float)this.pictureBox1.Height;
					newY *= ratioHeight;
					float scale = (float)this.pictureBox1.Height / (float)this.pictureBox1.Image.Height;
					float displayWidth = scale * (float)this.pictureBox1.Image.Width;
					float diffWidth = (float)this.pictureBox1.Width - displayWidth;
					diffWidth /= 2f;
					newX -= diffWidth;
					newX /= scale;
				}
				result = new System.Drawing.Point((int)newX, (int)newY);
			}
			return result;
		}

		private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			if (this._selecting)
			{
				System.Drawing.Pen pen = System.Drawing.Pens.Red;
				e.Graphics.DrawRectangle(pen, this._selection);
			}
		}

		private void pictureBox1_MouseEnter(object sender, EventArgs e)
		{
			this.Cursor = System.Windows.Forms.Cursors.Cross;
		}

		private void pictureBox1_MouseLeave(object sender, EventArgs e)
		{
			this.Cursor = System.Windows.Forms.Cursors.Default;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			MemoryStream ms = new MemoryStream(File.ReadAllBytes(this.Img.FullOriginalName));
			this.pictureBox1.Image = System.Drawing.Image.FromStream(ms);
            this.absoluteSelection = default(System.Drawing.Rectangle);
        }

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageCropping));
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(12, 674);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(169, 32);
            this.button1.TabIndex = 1;
            this.button1.Text = "Povrni na original";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(937, 674);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(124, 32);
            this.button3.TabIndex = 3;
            this.button3.Text = "Prekliči";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(788, 674);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(143, 32);
            this.button2.TabIndex = 2;
            this.button2.Text = "Potrdi";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1049, 656);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(187, 674);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(126, 32);
            this.button4.TabIndex = 4;
            this.button4.Text = "Test";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // ImageCropping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1073, 709);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "ImageCropping";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Obrezovanje";
            this.Load += new System.EventHandler(this.ImageCropping_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

		}        
    }
}
