using KatalogUmetnin.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace KatalogUmetnin.ImagePresentation
{
	public class ImageItemListUC : System.Windows.Forms.UserControl
	{
		public delegate void ImageDataChangedHandler(object sender, EventArgs e);

		public delegate void SelectionChangedHandler(object sender, EventArgs e);

		public delegate void ToolModeChangedHandler(object sender, ToolModeEventArs e);

		private IContainer components = null;

		private System.Windows.Forms.Button button1;

		private System.Windows.Forms.Button button2;

		public System.Windows.Forms.FlowLayoutPanel List;

		private System.Windows.Forms.Button button3;

		public event ImageItemListUC.ImageDataChangedHandler OnImageDataChanged;

		public event ImageItemListUC.SelectionChangedHandler OnSelectionChanged;

		public event ImageItemListUC.ToolModeChangedHandler OnToolModeChanged;

		public ImageItem SelectedImageItem
		{
			get;
			set;
		}

		public int ID
		{
			get;
			set;
		}

		public List<ImageItem> ImageItems
		{
			get
			{
				List<ImageItem> r = new List<ImageItem>();
				foreach (ImageItem i in this.List.Controls)
				{
					r.Add(i);
				}
				return r;
			}
		}

		public int ImagesCount
		{
			get
			{
				return this.List.Controls.Count;
			}
		}

		public virtual void ImageDataChanged()
		{
			if (this.OnImageDataChanged != null)
			{
				this.OnImageDataChanged(this, new EventArgs());
			}
		}

		public virtual void SelectionChanged()
		{
			if (this.OnSelectionChanged != null)
			{
				this.OnSelectionChanged(this, new EventArgs());
			}
		}

		public virtual void ToolModeChanged(ToolMode tm)
		{
			if (this.OnToolModeChanged != null)
			{
				this.OnToolModeChanged(this, new ToolModeEventArs(tm));
			}
		}

		public ImageItemListUC()
		{
			this.InitializeComponent();
            Settings.SetTexts(this);
			this.SelectedImageItem = null;
		}

		private void ImageItemListUC_Load(object sender, EventArgs e)
		{
		}

		public void LoadImages(List<CanvasImg> Images, int id)
		{
			this.ID = id;
			this.List.Controls.Clear();
			IOrderedEnumerable<CanvasImg> sortedImages = from p in Images
			orderby p.Index
			select p;
			foreach (CanvasImg img in sortedImages)
			{
				this.AddImage(img);
			}
			this.ImageDataChanged();
		}

		public void Duplicate(CanvasImg img)
		{
			ImportImages ii = new ImportImages(this.ID);
			string ni = ii.Import(img.FullOriginalName);
			CanvasImg ci = new CanvasImg(ni, this.List.Controls.Count, -1.0);
			this.AddImage(ci);
			this.SetIndexes();
			this.ImageDataChanged();
		}

		public void AddImage(CanvasImg img)
		{
			ImageItem ii = new ImageItem(img, this.ID);
			ii.OnImageDataChanged += new ImageItem.ImageDataChangedHandler(this.ii_OnImageDataChanged);
			ii.OnClicked += new ImageItem.ClickedHandler(this.ii_OnClicked);
			this.List.Controls.Add(ii);
		}

		private void ii_OnToolModeChanged(object sender, ToolModeEventArs e)
		{
			this.ToolModeChanged(e.Mode);
		}

		private void ii_OnClicked(object sender, EventArgs e)
		{
			this.SelectImageItem((ImageItem)sender);
		}

		public void SelectImageItem(ImageItem ii)
		{
			foreach (ImageItem i in this.List.Controls)
			{
				if (ii.Image.GUID == i.Image.GUID)
				{
					i.SetSelected(true);
					this.SelectedImageItem = i;
				}
				else if (i.IsSelected)
				{
					i.SetSelected(false);
				}
			}
			this.SelectionChanged();
		}

		private void ii_OnImageDataChanged(object sender, EventArgs e)
		{
			this.ImageDataChanged();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (this.SelectedImageItem != null)
			{
				if (this.SelectedImageItem.Index > 0)
				{
					for (int i = 0; i < this.List.Controls.Count; i++)
					{
						if (((ImageItem)this.List.Controls[i]).Image.GUID == this.SelectedImageItem.Image.GUID)
						{
							System.Windows.Forms.Control temp = this.List.Controls[i];
							this.List.Controls.SetChildIndex(temp, i - 1);
							this.ImageDataChanged();
							break;
						}
					}
				}
			}
			this.SetIndexes();
		}

		public void SetIndexes()
		{
			int i = 0;
			foreach (ImageItem ii in this.List.Controls)
			{
				ii.Index = i++;
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			if (this.SelectedImageItem != null)
			{
				if (this.List.Controls.IndexOf(this.SelectedImageItem) < this.List.Controls.Count - 1)
				{
					for (int i = 0; i < this.List.Controls.Count; i++)
					{
						if (((ImageItem)this.List.Controls[i]).Image.GUID == this.SelectedImageItem.Image.GUID)
						{
							System.Windows.Forms.Control temp = this.List.Controls[i];
							this.List.Controls.SetChildIndex(temp, i + 1);
							this.ImageDataChanged();
							break;
						}
					}
				}
			}
			this.SetIndexes();
		}

		private void List_Click(object sender, EventArgs e)
		{
			this.SelectedImageItem = null;
			foreach (ImageItem i in this.List.Controls)
			{
				if (i.IsSelected)
				{
					i.SetSelected(false);
				}
			}
			this.SelectionChanged();
		}

		internal void SelectByCoordinates(System.Drawing.Point p)
		{
			bool found = false;
			foreach (ImageItem ii in this.List.Controls)
			{
				if (ii.IsVisible)
				{
					System.Drawing.Rectangle r = ii.Image.SelectedRectangle;
					if (p.X >= r.Location.X && p.Y >= r.Location.Y && !found)
					{
						if (p.X <= r.Location.X + r.Size.Width && p.Y <= r.Location.Y + r.Size.Height)
						{
							this.SelectedImageItem = ii;
							ii.SetSelected(true);
							found = true;
						}
						else
						{
							ii.SetSelected(false);
						}
					}
					else
					{
						ii.SetSelected(false);
					}
				}
				else
				{
					ii.SetSelected(false);
				}
			}
			if (!found)
			{
				this.SelectedImageItem = null;
			}
			this.SelectionChanged();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			if (this.SelectedImageItem != null)
			{
				if (System.Windows.Forms.MessageBox.Show(MessageStrings.ImageItemDeleteText, MessageStrings.ImageItemDeleteTitle, System.Windows.Forms.MessageBoxButtons.YesNoCancel, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
				{
					string orig = Path.Combine(Img.OriginalLocation, this.SelectedImageItem.Image.FileName);
					string arch = Path.Combine(Img.ImageArchiveLocation, this.SelectedImageItem.Image.Name + "_" + Guid.NewGuid().ToString() + ".jpg");
					File.Copy(orig, arch, true);
					File.Delete(orig);
					FileInfo[] files = new DirectoryInfo(Img.ThumbLocation).GetFiles(this.SelectedImageItem.Image.FileName, SearchOption.AllDirectories);
					for (int i = 0; i < files.Length; i++)
					{
						FileInfo fi = files[i];
						fi.Delete();
					}
					File.Delete(this.SelectedImageItem.Image.FullFileName);
					this.SelectedImageItem.Dispose();
					this.SelectedImageItem = null;
					this.SelectionChanged();
					this.SetIndexes();
				}
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageItemListUC));
            this.List = new System.Windows.Forms.FlowLayoutPanel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // List
            // 
            this.List.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.List.AutoScroll = true;
            this.List.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.List.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.List.Location = new System.Drawing.Point(2, 31);
            this.List.Margin = new System.Windows.Forms.Padding(2);
            this.List.Name = "List";
            this.List.Size = new System.Drawing.Size(223, 446);
            this.List.TabIndex = 1;
            this.List.WrapContents = false;
            this.List.Click += new System.EventHandler(this.List_Click);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(32, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(23, 23);
            this.button2.TabIndex = 3;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(23, 23);
            this.button1.TabIndex = 2;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(201, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(23, 23);
            this.button3.TabIndex = 4;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // ImageItemListUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.List);
            this.Name = "ImageItemListUC";
            this.Size = new System.Drawing.Size(227, 479);
            this.Load += new System.EventHandler(this.ImageItemListUC_Load);
            this.ResumeLayout(false);

		}

        public void RealignComponent(int delta) {
            button3.Left = Width - button3.Width - delta;
            List.Left = button1.Left;
            List.Top = button1.Top + button1.Height + delta;
            List.Width = button3.Left + button3.Width - List.Left;
            List.Height = Bottom - delta - List.Top;
            List.Visible = true;
            List.Anchor = AnchorStyles.Top | AnchorStyles.Right;       

            foreach(Control c in List.Controls) {
                c.Width = List.Width - c.Left - delta * 2;
            }
        }
    }
}
