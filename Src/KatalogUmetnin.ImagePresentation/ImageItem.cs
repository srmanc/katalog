using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace KatalogUmetnin.ImagePresentation
{
	public class ImageItem : System.Windows.Forms.UserControl
	{
		public delegate void ImageDataChangedHandler(object sender, EventArgs e);

		public delegate void SelectionChangedHandler(object sender, EventArgs e);

		public delegate void ClickedHandler(object sender, EventArgs e);

		private bool _isSelected = false;

		private IContainer components = null;

		private System.Windows.Forms.PictureBox pictureBox1;

		private System.Windows.Forms.CheckBox checkBox1;

		private System.Windows.Forms.LinkLabel linkLabel1;

		public event ImageItem.ImageDataChangedHandler OnImageDataChanged;

		public event ImageItem.SelectionChangedHandler OnSelectionChanged;

		public event ImageItem.ClickedHandler OnClicked;

		public int Index
		{
			get
			{
				return this.Image.Index;
			}
			set
			{
				this.Image.Index = value;
			}
		}

		public bool IsVisible
		{
			get
			{
				return this.Image.Visible;
			}
			set
			{
				this.Image.Visible = value;
				this.checkBox1.Checked = value;
			}
		}

		public bool IsSelected
		{
			get
			{
				return this._isSelected;
			}
			set
			{
				this._isSelected = value;
			}
		}

		public CanvasImg Image
		{
			get;
			set;
		}

		public int ID
		{
			get;
			set;
		}

		public virtual void ImageDataChanged()
		{
			if (this.OnImageDataChanged != null)
			{
				this.OnImageDataChanged(this, new EventArgs());
			}
		}

		public virtual void SelectionChanged()
		{
		}

		public virtual void Clicked()
		{
			if (this.OnClicked != null)
			{
				this.OnClicked(this, new EventArgs());
			}
		}

		public ImageItem(CanvasImg image, int id)
		{
			this.ID = id;
			this.Image = image;
			this.InitializeComponent();
            Settings.SetTexts(this);
			this.Init();
			base.Click += new EventHandler(this.ImageItem_Click);
			this.pictureBox1.Click += new EventHandler(this.ImageItem_Click);
			this.checkBox1.CheckedChanged += new EventHandler(this.checkBox1_CheckedChanged);            
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			this.Image.Visible = this.checkBox1.Checked;
			this.ImageDataChanged();
		}

		public void Init()
		{
			this.pictureBox1.Image = this.Image.GetThumbnailImage(120, 120, false);
			this.checkBox1.Text = this.Image.FileName;
			this.checkBox1.Checked = this.Image.Visible;
		}

		private void ImageItem_Click(object sender, EventArgs e)
		{
			this.Clicked();
		}

		private void ImageItem_Load(object sender, EventArgs e)
		{
		}

		public void UncheckResizeButton()
		{
		}

		public void SetSelected(bool sel)
		{
			if (this.IsSelected != sel)
			{
				this.IsSelected = sel;
				if (sel)
				{
					this.BackColor = System.Drawing.Color.DarkGray;
				}
				else
				{
					this.BackColor = System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.Control);
				}
				if (sel)
				{
					this.SelectionChanged();
				}
			}
		}

		private void linkLabel1_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
		}

		public void Crop()
		{
			ImageCropping ic = new ImageCropping(this.Image);
			if (ic.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
			{
				try
				{                                                                                
					if (File.Exists(this.Image.FullFileName))
					{
						File.Delete(this.Image.FullFileName);
					}
					ic.CroppedImage.Save(this.Image.FullFileName);
					this.Image.LoadData(true);
                    Settings.FreeGarbage();
                    this.Init();
					this.ImageDataChanged();
                    Settings.FreeGarbage(); //UNCOM tole tudi gre bolje
                }
				catch (Exception ex)
				{
					System.Windows.Forms.MessageBox.Show(MessageStrings.Error.ToUpper() + ": " + ex.Message);
				}
			}
		}

		private void checkBox2_CheckedChanged(object sender, EventArgs e)
		{
			ImageItemListUC p = (ImageItemListUC)base.Parent.Parent;
			p.SelectImageItem(this);
		}

		private void button1_Click(object sender, EventArgs e)
		{
			this.Image.BlurVal = 1;
			this.ImageDataChanged();
		}

		private void linkLabel1_LinkClicked_1(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			ImageItemListUC p = (ImageItemListUC)base.Parent.Parent;
			p.Duplicate(this.Image);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(2, 2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(90, 79);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(100, 2);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(80, 17);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(97, 68);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(105, 13);
            this.linkLabel1.TabIndex = 2;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Skopiraj v novo sliko";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked_1);
            // 
            // ImageItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.pictureBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ImageItem";
            this.Size = new System.Drawing.Size(249, 84);
            this.Load += new System.EventHandler(this.ImageItem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
		}
	}
}
