using System;

namespace KatalogUmetnin.ImagePresentation
{
	public class ToolModeEventArs : EventArgs
	{
		public ToolMode Mode
		{
			get;
			set;
		}

		public ToolModeEventArs()
		{
			this.Mode = ToolMode.Move;
		}

		public ToolModeEventArs(ToolMode mode)
		{
			this.Mode = mode;
		}
	}
}
