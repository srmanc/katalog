using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace KatalogUmetnin.ImagePresentation
{
	public class ImportImages
	{
		private int ID
		{
			get;
			set;
		}

		public ImportImages(int id)
		{
			this.ID = id;
		}

		public string Import(string f)
		{
			string newName = this.GetNewImageName();
			string i = Path.Combine(Settings.Get.StoragePath, newName);
			f = f.ToLower();
			if (!f.EndsWith(".jpg") && !f.EndsWith(".jpeg"))
			{
				System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(f);
				bmp.Save(i, System.Drawing.Imaging.ImageFormat.Jpeg);
			}
			else
			{
				File.Copy(f, i, true);
			}
			return newName;
		}

		public string GetNewImageName()
		{
			int i = 1;
			string a = string.Concat(new string[]
			{
				"Slika_",
				this.ID.ToString(),
				"_",
				i.ToString(),
				".jpg"
			});
			while (File.Exists(Path.Combine(Settings.Get.StoragePath, a)))
			{
				i++;
				a = string.Concat(new string[]
				{
					"Slika_",
					this.ID.ToString(),
					"_",
					i.ToString(),
					".jpg"
				});
			}
			return a;
		}
	}
}
