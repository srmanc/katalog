using System;
using System.Drawing;

namespace KatalogUmetnin.ImagePresentation
{
	public class ImageTitle
	{
		public string Text
		{
			get;
			set;
		}

		public int FontSize
		{
			get;
			set;
		}

		public System.Drawing.Color ForeColor
		{
			get;
			set;
		}

		public System.Drawing.Font FontFam
		{
			get;
			set;
		}

		public System.Drawing.Point Location
		{
			get;
			set;
		}

		public ImageTitle()
		{
			this.Text = "";
			this.FontSize = 10;
			this.ForeColor = System.Drawing.Color.Black;
			this.FontFam = new System.Drawing.Font("Arial", (float)this.FontSize);
			this.Location = new System.Drawing.Point(0, 0);
		}

		public static ImageTitle GetDefault()
		{
			return new ImageTitle();
		}

		public static ImageTitle GetFromDBRow(KatalogUmetnin_dbDataSet.NapisiRow r)
		{
			return new ImageTitle
			{
				Text = r.Tekst,
				FontSize = r.Size,
				ForeColor = System.Drawing.Color.FromName(r.Color),
				FontFam = new System.Drawing.Font(r.Font, (float)r.Size),
				Location = new System.Drawing.Point(r.X, r.Y)
			};
		}
	}
}
