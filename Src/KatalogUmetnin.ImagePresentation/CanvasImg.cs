using System;
using System.Drawing;

namespace KatalogUmetnin.ImagePresentation
{
	public class CanvasImg : Img
	{
		public string GUID = Guid.NewGuid().ToString();

		public static System.Drawing.Point ImageTitleTextOffset = new System.Drawing.Point(10, 10);

		public static int ImageTitleFontSize = 14;

		public static string ImageTitleFontFamily = "Arial";

		private System.Drawing.Image _zoomedImage = null;

		public double Zoom
		{
			get;
			set;
		}

		public System.Drawing.Point Location
		{
			get;
			set;
		}

		public System.Drawing.Size ZoomedSize
		{
			get;
			set;
		}

		public int Index
		{
			get;
			set;
		}

		public int BlurVal
		{
			get;
			set;
		}

		public bool Visible
		{
			get;
			set;
		}

		public System.Drawing.Rectangle SelectedRectangle
		{
			get
			{
				return new System.Drawing.Rectangle(this.Location, this.ZoomedSize);
			}
		}

		public System.Drawing.Image ZoomedImage
		{
			get
			{
				return this._zoomedImage;
			}
		}

        public int Rotation { get; internal set; }
        public int Nr { get; internal set; }

        public CanvasImg(string filename, int idx, double zoom = -1.0)
		{
			this.Index = idx;
			base.FileName = filename;
			this.Visible = true;
			Image img = base.LoadData(false);
            this.Zoom = zoom;
            this.Location = new System.Drawing.Point(0, 0);
            this.SetZoomedSize(zoom, img);
            Settings.FreeGarbage();
        }
        
		public double GetZoomByFactor(double factor, System.Drawing.Size canvasSize)
		{
			double result;
			if (base.Size.Width > base.Size.Height)
			{
				double z = (double)canvasSize.Width / (double)base.Size.Width * factor;
				result = z;
			}
			else
			{
				double z = (double)canvasSize.Height / (double)base.Size.Height * factor;
				result = z;
			}
			return result;
		}

		public double GetZoomByWidth(int w)
		{
			double origW = (double)base._imageSize.Width;
			return (double)w / origW;
		}
		
		public void SetZoomedSize(double zoom, Image img = null)
		{
            bool resize = false;
			int w = base.Size.Width;
			int h = base.Size.Height;
			if (zoom > 0.0)
			{
				this.Zoom = zoom;
				w = Convert.ToInt32((double)w * zoom);
				h = Convert.ToInt32((double)base.Size.Height * zoom);
                if (w != ZoomedSize.Width || h != ZoomedSize.Height)
                {
                    this.ZoomedSize = new System.Drawing.Size(w, h);
                    resize = true;
                }
			}
            if (resize || _zoomedImage == null)
            {
                if (w == 0 || h == 0)
                    return;

                System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(w, h);
                System.Drawing.Graphics graphic = System.Drawing.Graphics.FromImage(bmp);
                bool garbage = false;
                if (img == null) {
                    img = base.GetImage();
                    garbage = true;
                }
                graphic.DrawImage(img, 0, 0, w, h);
                if (garbage) {
                    img = null;
                    Settings.FreeGarbage();
                }
                if (this.BlurVal > 0)
                {
                    this._zoomedImage = bmp.ImageBlurFilter(Blur.BlurType.GaussianBlur5x5, this.BlurVal);
                    Settings.FreeGarbage();
                }
                else
                {
                    this._zoomedImage = bmp;
                }
            }
		}        
	}
}
