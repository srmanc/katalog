using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace KatalogUmetnin.ImagePresentation
{
	public class Thumbnail
	{
		private string _thumbnailPath = "";

		private string ThumbnailFullName
		{
			get
			{
				return this._thumbnailPath;
			}
		}

		private string OriginalFullName
		{
			get
			{
				return this.OriginalPath;
			}
		}

		private string OriginalPath
		{
			get;
			set;
		}

		public int MaxWidth
		{
			get;
			set;
		}

		public int MaxHeight
		{
			get;
			set;
		}		

		public Thumbnail(int MaxWidth, int MaxHeight, string originalPath, bool invalidateThumb = false)
		{
			this.OriginalPath = originalPath;
			this.MaxHeight = MaxHeight;
			this.MaxWidth = MaxWidth;
			this._thumbnailPath = Path.Combine(Img.ThumbLocation, string.Concat(new string[]
			{
				MaxWidth.ToString(),
				"x",
				MaxHeight.ToString(),
				"\\",
				Path.GetFileName(this.OriginalPath)
			}));
			string ThumbDir = Path.GetDirectoryName(this.ThumbnailFullName);
			if (File.Exists(this.ThumbnailFullName) && invalidateThumb)
			{
				File.Delete(this.ThumbnailFullName);
			}
			if (!File.Exists(this.ThumbnailFullName))
			{
				if (!Directory.Exists(ThumbDir))
				{
					Directory.CreateDirectory(ThumbDir);
				}
				this.GenerateThumbnail();
			}
		}

        public Image GetImage()
        {
            try {
                Image img = Image.FromStream(new MemoryStream(File.ReadAllBytes(_thumbnailPath)));
                return img;
            }
            catch {
                return null;
            }
        }

		private bool GenerateThumbnail()
		{
			bool result;
			try
			{
				System.Drawing.Bitmap b = this.LoadBitmapFromFile(this.OriginalFullName);
				decimal fak;
				if (this.MaxWidth == 0)
				{
					fak = (decimal)this.MaxHeight / b.Height;
				}
				else if (this.MaxHeight == 0)
				{
					fak = (decimal)this.MaxWidth / b.Width;
				}
				else
				{
					decimal fak2 = (decimal)this.MaxWidth / b.Width;
					decimal fak3 = (decimal)this.MaxHeight / b.Height;
					if (fak2 < fak3)
					{
						fak = fak2;
					}
					else
					{
						fak = fak3;
					}
				}
				if (fak >= 1m)
				{
					File.Copy(this.OriginalFullName, this.ThumbnailFullName, true);
				}
				else
				{
					int neww = Convert.ToInt32(b.Width * fak);
					int newh = Convert.ToInt32(b.Height * fak);
					b = this.ResizeBitmap(b, neww, newh);
					this.SaveBitmapToFile(b, this.ThumbnailFullName);
				}
				result = true;
				return result;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			result = false;
			return result;
		}

		private System.Drawing.Bitmap ResizeBitmap(System.Drawing.Bitmap b, int nWidth, int nHeight)
		{
			System.Drawing.Bitmap result = new System.Drawing.Bitmap(nWidth, nHeight);
			using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(result))
			{
				g.DrawImage(b, 0, 0, nWidth, nHeight);
			}
			return result;
		}

		private System.Drawing.Bitmap LoadBitmapFromFile(string file)
		{
			System.Drawing.Bitmap result;
			if (File.Exists(file))
			{
				FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read);
				System.Drawing.Bitmap b = new System.Drawing.Bitmap(System.Drawing.Image.FromStream(fs));
				fs.Flush();
				fs.Close();
				result = b;
			}
			else
			{
				result = null;
			}
			return result;
		}

		private bool SaveBitmapToFile(System.Drawing.Bitmap bmp, string file)
		{
			return this.SaveBitmapToFile(bmp, file, System.Drawing.Imaging.ImageFormat.Jpeg);
		}

		private bool SaveBitmapToFile(System.Drawing.Bitmap bmp, string file, System.Drawing.Imaging.ImageFormat imageFormat)
		{
			bool result;
			try
			{
				if (File.Exists(file))
				{
					File.Delete(file);
				}
				FileStream fs = new FileStream(file, FileMode.CreateNew);
				bmp.Save(fs, imageFormat);
				fs.Flush();
				fs.Close();
				result = true;
			}
			catch (Exception ex_39)
			{
				result = false;
			}
			return result;
		}
	}
}
