using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace KatalogUmetnin.ImagePresentation
{
	public class ExportCanvas
	{
		public int ID
		{
			get;
			set;
		}

		public System.Drawing.Size CanvasSize
		{
			get;
			set;
		}

		public List<CanvasImg> Images
		{
			get;
			set;
		}

		public string Opis
		{
			get;
			set;
		}

		public ExportCanvas(int id, System.Drawing.Size canvasSize, System.Windows.Forms.Control.ControlCollection controls, string opis)
		{
			this.ID = id;
			this.CanvasSize = canvasSize;
			this.Images = new List<CanvasImg>();
			foreach (ImageItem c in controls)
			{
				this.Images.Add(c.Image);
			}
			this.Opis = opis;
		}

		public ExportCanvas(int id, System.Drawing.Size canvasSize, List<CanvasImg> imgs, string opis)
		{
			System.Windows.Forms.FlowLayoutPanel ow = new System.Windows.Forms.FlowLayoutPanel();
			this.ID = id;
			this.CanvasSize = canvasSize;
			this.Images = imgs;
			this.Opis = opis;
		}

		public void Export()
		{
			this.ExportPrint();
			this.ExportPreview();
		}

		private void ExportPreview()
		{
			this.Export(1, Path.Combine(Img.CompositePreviewLocation, "Kompozicija_" + this.ID.ToString() + ".png"));
		}

		public void ExportPrint()
		{
			this.Export(4, Path.Combine(Img.CompositePrintLocation, "Kompozicija_" + this.ID.ToString() + ".png"));
		}

		public void Export(int factor, string outFile)
		{
			System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(this.CanvasSize.Width * factor, this.CanvasSize.Height * factor);
			System.Drawing.Graphics graphic = System.Drawing.Graphics.FromImage(bmp);
			System.Drawing.SolidBrush whiteBrush = new System.Drawing.SolidBrush(System.Drawing.Color.White);
			System.Drawing.Rectangle rect = new System.Drawing.Rectangle(new System.Drawing.Point(0, 0), new System.Drawing.Size(bmp.Size.Width, bmp.Size.Height));
			graphic.FillRectangle(whiteBrush, rect);
            for(int i = this.Images.Count-1; i >= 0; i--)
			//foreach (CanvasImg ii in this.Images)
			{
                CanvasImg ii = this.Images[i];
				if (ii.Visible)
				{
                    if (ii.ZoomedImage != null)
                    {
                        Image img = ii.GetImage();
                        if (ii.Rotation != 0)
                        {
                            graphic.TranslateTransform((ii.Location.X + ii.ZoomedSize.Width / 2) * factor,
                                (ii.Location.Y + ii.ZoomedSize.Height / 2) * factor);
                            graphic.RotateTransform(ii.Rotation);
                            graphic.DrawImage(ii.ZoomedImage, (-ii.ZoomedSize.Width / 2) * factor,
                                (-ii.ZoomedSize.Height / 2) * factor,
                                ii.ZoomedSize.Width * factor, ii.ZoomedSize.Height * factor);
                            graphic.ResetTransform();
                        }
                        else
                            graphic.DrawImage(img, ii.Location.X * factor, ii.Location.Y * factor, ii.ZoomedSize.Width * factor, ii.ZoomedSize.Height * factor);
                    }
                    Settings.FreeGarbage();
                }                

            }
			System.Drawing.Font imgTextFont = new System.Drawing.Font(CanvasImg.ImageTitleFontFamily, (float)CanvasImg.ImageTitleFontSize * (float)factor);
			System.Drawing.SizeF imgTextDim = graphic.MeasureString(this.Opis, imgTextFont);
			float imgTextX = (float)CanvasImg.ImageTitleTextOffset.X * (float)factor;
			float imgTextY = (float)bmp.Height - (float)CanvasImg.ImageTitleTextOffset.Y * (float)factor - imgTextDim.Height;
			//graphic.DrawString(this.Opis, imgTextFont, System.Drawing.Brushes.Black, new System.Drawing.PointF(imgTextX, imgTextY));
			if (File.Exists(outFile))
			{
				File.Delete(outFile);
			}
			bmp.Save(outFile, System.Drawing.Imaging.ImageFormat.Png);
		}
	}
}
