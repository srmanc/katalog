using KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters;
using KatalogUmetnin.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Data;

namespace KatalogUmetnin.ImagePresentation
{
    public class ImagePresentationUC : System.Windows.Forms.UserControl
    {
        public ToolMode Mode = ToolMode.Move;

        public ImageTitle Title = new ImageTitle();

        private bool _moving = false;

        private bool _reallyMoving = false;

        private System.Drawing.Point _startPoint = default(System.Drawing.Point);

        private System.Drawing.Point offSet = default(System.Drawing.Point);

        private System.Drawing.Point _resizeStartPoint = default(System.Drawing.Point);

        private System.Drawing.Point resizeOffSet = default(System.Drawing.Point);

        private System.Drawing.Rectangle _resizeRectangle = default(System.Drawing.Rectangle);

        private IContainer components = null;

        private System.Windows.Forms.PictureBox pictureBox1;

        private System.Windows.Forms.Button button1;

        private ImageItemListUC imgList;

        private System.Windows.Forms.Button button2;

        private System.Windows.Forms.OpenFileDialog openFileDialog1;

        private System.Windows.Forms.Panel panel1;

        private System.Windows.Forms.RadioButton bMove;

        private System.Windows.Forms.RadioButton bCrop;

        private System.Windows.Forms.RadioButton bResize;

        private System.Windows.Forms.Button button5;

        private System.Windows.Forms.Button button4;

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem skrijSlikoToolStripMenuItem;

        public int ID {
            get;
            set;
        }

        public DataView Slike {
            get;
            set;
        }

        public System.Drawing.Size CanvasSize {
            get {
                return this.pictureBox1.Size;
            }
        }

        public string Opis {
            get;
            set;
        }

        public List<CanvasImg> CanvasImages {
            get {
                List<CanvasImg> r = new List<CanvasImg>();
                foreach (ImageItem i in this.imgList.ImageItems) {
                    r.Add(i.Image);
                }
                return r;
            }
        }

        public ImagePresentationUC() {
            this.InitializeComponent();
            Settings.SetTexts(this);
        }

        private void ImagePresentationUC_Load(object sender, EventArgs e) {
            this.bMove.CheckedChanged += new EventHandler(this.toolButton_CheckedChanged);
            this.bCrop.CheckedChanged += new EventHandler(this.toolButton_CheckedChanged);
            this.bResize.CheckedChanged += new EventHandler(this.toolButton_CheckedChanged);
            this.button1.Visible = Debugger.IsAttached;
        }

        private void toolButton_CheckedChanged(object sender, EventArgs e) {
            if (this.bResize.Checked) {
                this.Mode = ToolMode.Resize;
            }
            else if (this.bCrop.Checked) {
                if (this.imgList.SelectedImageItem != null) {
                    this.imgList.SelectedImageItem.Crop();
                }
                this.bMove.Checked = true;
            }
            else {
                this.Mode = ToolMode.Move;
            }
        }

        public void LoadData(int id) {
            this.ID = id;
            this.Init();
        }

        internal void LoadData(int id, DataView s, string opis) {
            this.ID = id;
            this.Slike = s;
            this.Opis = opis;
            this.Init();
        }

        private void Init() {
            this.LoadImages();
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            this.pictureBox1.MouseLeave += new EventHandler(this.pictureBox1_MouseLeave);
        }

        private void LoadImageTitle() {
            KatalogUmetnin_dbDataSet.NapisiDataTable dt = new NapisiTableAdapter().GetDataByID(this.ID, 0);
            if (dt.Rows.Count == 1) {
            }
        }

        public void LoadImages() {
            List<CanvasImg> Images = new List<CanvasImg>();
            if (this.Slike != null) {
                foreach (DataRow r in this.Slike.Table.Rows) {
                    CanvasImg c = new CanvasImg((string)r["Ime"], (int)r["VrstniRed"], (double)r["Zoom"]);
                    c.Location = new System.Drawing.Point((int)r["X"], (int)r["Y"]);
                    c.Zoom = (double)r["Zoom"];
                    c.Visible = (bool)r["Visible"];
                    c.Rotation = (int)r["Rotation"];
                    c.Nr = (int)r["Nr"];
                    Images.Add(c);
                }
            }
            else {
                int i = 0;
                FileInfo[] files = new DirectoryInfo(Settings.Get.StoragePath).GetFiles("Slika_" + this.ID.ToString() + "_*.*");
                for (int j = 0; j < files.Length; j++) {
                    FileInfo f = files[j];
                    CanvasImg c = new CanvasImg(f.Name, i++, -1.0);
                    Images.Add(c);
                }
            }
            this.imgList.LoadImages(Images, this.ID);
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e) {
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e) {
            Graphics g = e.Graphics;

            for (int i = this.imgList.List.Controls.Count - 1; i >= 0; i--) {
                ImageItem ii = (ImageItem)this.imgList.List.Controls[i];
                if (ii.IsVisible) {
                    double fac = ii.Image.Zoom; 
                    if (fac == -1.0) {
                        fac = ii.Image.GetZoomByFactor(0.25, this.pictureBox1.Size);
                    }
                    ii.Image.SetZoomedSize(fac);

                    if (ii.Image.ZoomedImage != null) {
                        if (ii.Image.Rotation != 0) {
                            g.TranslateTransform(ii.Image.Location.X + ii.Image.ZoomedSize.Width / 2, ii.Image.Location.Y + ii.Image.ZoomedSize.Height / 2);
                            g.RotateTransform(ii.Image.Rotation);
                            g.DrawImage(ii.Image.ZoomedImage, -ii.Image.ZoomedSize.Width / 2, -ii.Image.ZoomedSize.Height / 2, ii.Image.ZoomedSize.Width, ii.Image.ZoomedSize.Height);
                            g.ResetTransform();
                        }
                        else
                            g.DrawImage(ii.Image.ZoomedImage, ii.Image.Location.X, ii.Image.Location.Y, ii.Image.ZoomedSize.Width, ii.Image.ZoomedSize.Height);
                    }
                }
            }
            System.Drawing.Font imgTextFont = new System.Drawing.Font(CanvasImg.ImageTitleFontFamily, (float)CanvasImg.ImageTitleFontSize);
            System.Drawing.SizeF imgTextDim = e.Graphics.MeasureString(this.Opis, imgTextFont);
            //g.DrawString(this.Opis, imgTextFont, System.Drawing.Brushes.Black, new System.Drawing.PointF((float)CanvasImg.ImageTitleTextOffset.X, (float)this.pictureBox1.Height - imgTextDim.Height - (float)CanvasImg.ImageTitleTextOffset.Y));
            if (this.imgList.SelectedImageItem != null) {
                Pen PenSelected = new Pen(Pens.Red.Color, 2.5f);
                Rectangle r = this.imgList.SelectedImageItem.Image.SelectedRectangle;
                int rotation = this.imgList.SelectedImageItem.Image.Rotation;
                if (this.Mode == ToolMode.Resize)
                    r = _resizeRectangle;

                if (rotation != 0) {
                    g.TranslateTransform(r.X + r.Width / 2, r.Y + r.Height / 2);
                    g.RotateTransform(rotation);
                    g.DrawRectangle(PenSelected, new Rectangle(-r.Width / 2, -r.Height / 2, r.Width, r.Height));
                    g.ResetTransform();
                }
                else
                    g.DrawRectangle(PenSelected, r);
            }
        }

        private void pictureBox1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e) {
            if (this.imgList.SelectedImageItem != null) {
                if (this.Mode == ToolMode.Move) {
                    System.Drawing.Rectangle r = this.imgList.SelectedImageItem.Image.SelectedRectangle;
                    if (e.Location.X >= r.Location.X && e.Location.Y >= r.Location.Y) {
                        if (e.Location.X <= r.Location.X + r.Size.Width && e.Location.Y <= r.Location.Y + r.Size.Height) {
                            this.Cursor = System.Windows.Forms.Cursors.SizeAll;
                        }
                        else {
                            this.Cursor = System.Windows.Forms.Cursors.Default;
                        }
                    }
                    else {
                        this.Cursor = System.Windows.Forms.Cursors.Default;
                    }
                    if (this._moving) {
                        this._reallyMoving = true;
                        int x = e.Location.X - this.offSet.X;
                        int y = e.Location.Y - this.offSet.Y;
                        this.imgList.SelectedImageItem.Image.Location = new System.Drawing.Point(x, y);
                        this.pictureBox1.Refresh();
                    }
                }
                else if (this.Mode == ToolMode.Resize) {
                    if (this._moving) {
                        this._reallyMoving = true;
                        int w = e.Location.X - this._startPoint.X;
                        double factor = (double)w / (double)this.imgList.SelectedImageItem.Image.ZoomedSize.Width;
                        int h = Convert.ToInt32(factor * (double)this.imgList.SelectedImageItem.Image.ZoomedSize.Height);
                        this._resizeRectangle = new System.Drawing.Rectangle(this._startPoint.X, this._startPoint.Y, w, h);
                        this.pictureBox1.Refresh();
                    }
                }
            }
        }

        private void pictureBox1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e) {
            if (this.imgList.SelectedImageItem != null) {
                if (this.Mode == ToolMode.Move) {
                    if (e.Button == System.Windows.Forms.MouseButtons.Left) {
                        System.Drawing.Rectangle r = this.imgList.SelectedImageItem.Image.SelectedRectangle;
                        if (e.Location.X >= r.Location.X && e.Location.Y >= r.Location.Y) {
                            if (e.Location.X <= r.Location.X + r.Size.Width && e.Location.Y <= r.Location.Y + r.Size.Height) {
                                this._moving = true;
                                this._startPoint = e.Location;
                                this.offSet = new System.Drawing.Point(e.Location.X - r.Location.X, e.Location.Y - r.Location.Y);
                            }
                        }
                    }
                    else if (e.Button == System.Windows.Forms.MouseButtons.Right) {
                        this.contextMenuStrip1.Show((System.Windows.Forms.Control)sender, e.X, e.Y);
                    }
                }
                else if (this.Mode == ToolMode.Resize) {
                    this._moving = true;
                    this._startPoint = e.Location;
                }
            }
        }

        private void pictureBox1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e) {
            if (this.imgList.SelectedImageItem != null) {
                if (this.Mode == ToolMode.Move) {
                    this._moving = false;
                }
                else if (this.Mode == ToolMode.Resize) {
                    this._moving = false;
                    this.imgList.SelectedImageItem.Image.Location = this._resizeRectangle.Location;
                    double factor = this.imgList.SelectedImageItem.Image.GetZoomByWidth(this._resizeRectangle.Width);
                    this.imgList.SelectedImageItem.Image.SetZoomedSize(factor);
                    this.imgList.SelectedImageItem.UncheckResizeButton();
                    this.pictureBox1.Refresh();
                    this._resizeRectangle = default(System.Drawing.Rectangle);
                    this.bMove.Checked = true;
                }
            }
            if (!this._reallyMoving) {
                this.imgList.SelectByCoordinates(e.Location);
            }
            else {
                this._reallyMoving = false;
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            this.ExportComposition();
        }

        public void ExportComposition() {
            ExportCanvas ec = new ExportCanvas(this.ID, this.pictureBox1.Size, this.imgList.List.Controls, this.Opis);
            ec.Export();
        }

        private void imgList_OnSelectionChanged(object sender, EventArgs e) {
            this.pictureBox1.Refresh();
        }

        private void imgList_OnImageDataChanged(object sender, EventArgs e) {
            this.pictureBox1.Refresh();
        }

        private void imgList_OnToolModeChanged(object sender, ToolModeEventArs e) {
            this.Mode = e.Mode;
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e) {
            if (this.Mode == ToolMode.Resize) {
                this.Cursor = System.Windows.Forms.Cursors.Cross;
            }
            else {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            this.openFileDialog1.FileName = "";
            if (this.openFileDialog1.ShowDialog(this) == System.Windows.Forms.DialogResult.OK) {
                if (base.Parent is Umetnisko_delo) {
                    Umetnisko_delo u = (Umetnisko_delo)base.Parent;
                    if (u.IsNew) {
                        u.Save(false);
                    }
                }
                ImportImages ii = new ImportImages(this.ID);
                string[] fileNames = this.openFileDialog1.FileNames;
                for (int i = 0; i < fileNames.Length; i++) {
                    string f = fileNames[i];
                    string a = ii.Import(f);
                    this.imgList.AddImage(new CanvasImg(a, this.imgList.ImagesCount, -1.0));
                }
                this.pictureBox1.Refresh();
            }
            this.imgList.SetIndexes();
        }

        private void button5_Click(object sender, EventArgs e) {
            if (this.imgList.SelectedImageItem != null) {
                /*if (comboBoxDeg.SelectedIndex >= 0)*/ {
                    int iDeg = 90;
                    /*string sDeg = comboBoxDeg.Text.Substring(0, comboBoxDeg.Text.IndexOf("°"));
                    int iDeg;
                    if (int.TryParse(sDeg, out iDeg))*/ {
                        imgList.SelectedImageItem.Image.Rotation -= iDeg;

                        this.pictureBox1.Refresh();
                    }
                }
            }
        }

        private void button4_Click(object sender, EventArgs e) {
            if (this.imgList.SelectedImageItem != null) {
                /*if (comboBoxDeg.SelectedIndex >= 0)*/ {
                    int iDeg = 90;
                    /*string sDeg = comboBoxDeg.Text.Substring(0, comboBoxDeg.Text.IndexOf("°"));
                    int iDeg;
                    if (int.TryParse(sDeg, out iDeg))*/ {
                        imgList.SelectedImageItem.Image.Rotation += iDeg;

                        this.pictureBox1.Refresh();
                    }
                }
            }
        }

        private void ImagePresentationUC_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e) {
            if (this.imgList.SelectedImageItem != null && this.Mode == ToolMode.Move) {
                if (e.KeyCode == System.Windows.Forms.Keys.Left) {
                    this.imgList.SelectedImageItem.Location = new System.Drawing.Point(this.imgList.SelectedImageItem.Location.X - 10, this.imgList.SelectedImageItem.Location.Y);
                }
                else if (e.KeyCode == System.Windows.Forms.Keys.Right) {
                    this.imgList.SelectedImageItem.Location = new System.Drawing.Point(this.imgList.SelectedImageItem.Location.X + 10, this.imgList.SelectedImageItem.Location.Y);
                }
                else if (e.KeyCode == System.Windows.Forms.Keys.Up) {
                    this.imgList.SelectedImageItem.Location = new System.Drawing.Point(this.imgList.SelectedImageItem.Location.X, this.imgList.SelectedImageItem.Location.Y - 10);
                }
                else if (e.KeyCode == System.Windows.Forms.Keys.Down) {
                    this.imgList.SelectedImageItem.Location = new System.Drawing.Point(this.imgList.SelectedImageItem.Location.X, this.imgList.SelectedImageItem.Location.Y + 10);
                }
            }
        }

        private void skrijSlikoToolStripMenuItem_Click(object sender, EventArgs e) {
            if (this.imgList.SelectedImageItem != null) {
                this.imgList.SelectedImageItem.IsVisible = false;
                this.imgList.SelectedImageItem.SetSelected(false);
                this.imgList.SelectedImageItem = null;
                this.pictureBox1.Refresh();
            }
        }

        private void imgList_Load(object sender, EventArgs e) {
        }

        internal void UpdateOpis(string opis) {
            this.Opis = opis;
            this.pictureBox1.Refresh();
        }

        protected override void Dispose(bool disposing) {
            if (disposing && this.components != null) {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void ShiftButton(Control button, ref int x, ref int y, int delta) {
            button.Left = x;
            button.Top = y;
            x += button.Width + delta;
        }

        public void RealignComponents() {            
            int delta = 5;
            int x = button2.Left;
            int y = button2.Top;
            ShiftButton(button2, ref x, ref y, delta);
            ShiftButton(button1, ref x, ref y, delta);            
            ShiftButton(panel1, ref x, ref y, delta);
            panel1.Anchor = AnchorStyles.Left | AnchorStyles.Top;
            y -= delta / 2;
            ShiftButton(imgList, ref x, ref y, delta);
            imgList.Width = Width - imgList.Left - delta;
            imgList.Height = Height - imgList.Top;
            imgList.RealignComponent(delta);
            imgList.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;

            x = bMove.Left;
            y = bMove.Top;
            ShiftButton(bMove, ref x, ref y, delta);
            ShiftButton(bCrop, ref x, ref y, delta);
            ShiftButton(bResize, ref x, ref y, delta);
            ShiftButton(button5, ref x, ref y, delta);
            ShiftButton(button4, ref x, ref y, delta);

            button1.Visible = true;
        }

        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImagePresentationUC));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.bResize = new System.Windows.Forms.RadioButton();
            this.bCrop = new System.Windows.Forms.RadioButton();
            this.bMove = new System.Windows.Forms.RadioButton();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.skrijSlikoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imgList = new KatalogUmetnin.ImagePresentation.ImageItemListUC();
            this.panel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Slikovne datoteke|*.jpg;*.jpeg|Vse datoteke|*";
            this.openFileDialog1.Multiselect = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.bResize);
            this.panel1.Controls.Add(this.bCrop);
            this.panel1.Controls.Add(this.bMove);
            this.panel1.Location = new System.Drawing.Point(217, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(441, 23);
            this.panel1.TabIndex = 8;
            // 
            // button5
            // 
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.Location = new System.Drawing.Point(391, 0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(23, 23);
            this.button5.TabIndex = 4;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(414, 0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(23, 23);
            this.button4.TabIndex = 3;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // bResize
            // 
            this.bResize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bResize.Appearance = System.Windows.Forms.Appearance.Button;
            this.bResize.Image = ((System.Drawing.Image)(resources.GetObject("bResize.Image")));
            this.bResize.Location = new System.Drawing.Point(213, 0);
            this.bResize.Name = "bResize";
            this.bResize.Size = new System.Drawing.Size(98, 23);
            this.bResize.TabIndex = 2;
            this.bResize.Text = "Raztezovanje";
            this.bResize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bResize.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bResize.UseVisualStyleBackColor = true;
            // 
            // bCrop
            // 
            this.bCrop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bCrop.Appearance = System.Windows.Forms.Appearance.Button;
            this.bCrop.Image = ((System.Drawing.Image)(resources.GetObject("bCrop.Image")));
            this.bCrop.Location = new System.Drawing.Point(114, 0);
            this.bCrop.Name = "bCrop";
            this.bCrop.Size = new System.Drawing.Size(93, 23);
            this.bCrop.TabIndex = 1;
            this.bCrop.Text = "Obrezovanje";
            this.bCrop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bCrop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bCrop.UseVisualStyleBackColor = true;
            // 
            // bMove
            // 
            this.bMove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bMove.Appearance = System.Windows.Forms.Appearance.Button;
            this.bMove.Checked = true;
            this.bMove.Image = ((System.Drawing.Image)(resources.GetObject("bMove.Image")));
            this.bMove.Location = new System.Drawing.Point(13, 0);
            this.bMove.Name = "bMove";
            this.bMove.Size = new System.Drawing.Size(95, 23);
            this.bMove.TabIndex = 0;
            this.bMove.TabStop = true;
            this.bMove.Text = "Premikanje";
            this.bMove.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bMove.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bMove.UseVisualStyleBackColor = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.skrijSlikoToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(124, 26);
            // 
            // skrijSlikoToolStripMenuItem
            // 
            this.skrijSlikoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("skrijSlikoToolStripMenuItem.Image")));
            this.skrijSlikoToolStripMenuItem.Name = "skrijSlikoToolStripMenuItem";
            this.skrijSlikoToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.skrijSlikoToolStripMenuItem.Text = "Skrij sliko";
            this.skrijSlikoToolStripMenuItem.Click += new System.EventHandler(this.skrijSlikoToolStripMenuItem_Click);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(2, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(87, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Uvozi sliko";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(94, 3);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Izvozi kompozicijo";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(2, 30);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(560, 602);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            // 
            // imgList
            // 
            this.imgList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.imgList.ID = 0;
            this.imgList.Location = new System.Drawing.Point(660, -1);
            this.imgList.Name = "imgList";
            this.imgList.SelectedImageItem = null;
            this.imgList.Size = new System.Drawing.Size(278, 635);
            this.imgList.TabIndex = 4;
            this.imgList.OnImageDataChanged += new KatalogUmetnin.ImagePresentation.ImageItemListUC.ImageDataChangedHandler(this.imgList_OnImageDataChanged);
            this.imgList.OnSelectionChanged += new KatalogUmetnin.ImagePresentation.ImageItemListUC.SelectionChangedHandler(this.imgList_OnSelectionChanged);
            this.imgList.OnToolModeChanged += new KatalogUmetnin.ImagePresentation.ImageItemListUC.ToolModeChangedHandler(this.imgList_OnToolModeChanged);
            // 
            // ImagePresentationUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.imgList);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "ImagePresentationUC";
            this.Size = new System.Drawing.Size(941, 634);
            this.Load += new System.EventHandler(this.ImagePresentationUC_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ImagePresentationUC_KeyUp);
            this.panel1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
