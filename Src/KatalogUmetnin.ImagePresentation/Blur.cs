using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace KatalogUmetnin.ImagePresentation
{
	public static class Blur
	{
		public enum BlurType
		{
			Mean3x3,
			Mean5x5,
			Mean7x7,
			Mean9x9,
			GaussianBlur3x3,
			GaussianBlur5x5,
			MotionBlur5x5,
			MotionBlur5x5At45Degrees,
			MotionBlur5x5At135Degrees,
			MotionBlur7x7,
			MotionBlur7x7At45Degrees,
			MotionBlur7x7At135Degrees,
			MotionBlur9x9,
			MotionBlur9x9At45Degrees,
			MotionBlur9x9At135Degrees,
			Median3x3,
			Median5x5,
			Median7x7,
			Median9x9,
			Median11x11
		}

		public static System.Drawing.Bitmap ImageBlurFilter(this System.Drawing.Bitmap sourceBitmap, Blur.BlurType blurType, int iterations)
		{
			System.Drawing.Bitmap resultBitmap = sourceBitmap;
			for (int i = 0; i < iterations; i++)
			{
				resultBitmap = resultBitmap.ImageBlurFilter(Blur.BlurType.GaussianBlur5x5);
			}
			return resultBitmap;
		}

		public static System.Drawing.Bitmap ImageBlurFilter(this System.Drawing.Bitmap sourceBitmap, Blur.BlurType blurType)
		{
			System.Drawing.Bitmap resultBitmap = null;
			switch (blurType)
			{
			case Blur.BlurType.Mean3x3:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.Mean3x3, 0.1111111111111111, 0);
				break;
			case Blur.BlurType.Mean5x5:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.Mean5x5, 0.04, 0);
				break;
			case Blur.BlurType.Mean7x7:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.Mean7x7, 0.020408163265306121, 0);
				break;
			case Blur.BlurType.Mean9x9:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.Mean9x9, 0.012345679012345678, 0);
				break;
			case Blur.BlurType.GaussianBlur3x3:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.GaussianBlur3x3, 0.0625, 0);
				break;
			case Blur.BlurType.GaussianBlur5x5:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.GaussianBlur5x5, 0.0062893081761006293, 0);
				break;
			case Blur.BlurType.MotionBlur5x5:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.MotionBlur5x5, 0.1, 0);
				break;
			case Blur.BlurType.MotionBlur5x5At45Degrees:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.MotionBlur5x5At45Degrees, 0.2, 0);
				break;
			case Blur.BlurType.MotionBlur5x5At135Degrees:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.MotionBlur5x5At135Degrees, 0.2, 0);
				break;
			case Blur.BlurType.MotionBlur7x7:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.MotionBlur7x7, 0.071428571428571425, 0);
				break;
			case Blur.BlurType.MotionBlur7x7At45Degrees:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.MotionBlur7x7At45Degrees, 0.14285714285714285, 0);
				break;
			case Blur.BlurType.MotionBlur7x7At135Degrees:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.MotionBlur7x7At135Degrees, 0.14285714285714285, 0);
				break;
			case Blur.BlurType.MotionBlur9x9:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.MotionBlur9x9, 0.055555555555555552, 0);
				break;
			case Blur.BlurType.MotionBlur9x9At45Degrees:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.MotionBlur9x9At45Degrees, 0.1111111111111111, 0);
				break;
			case Blur.BlurType.MotionBlur9x9At135Degrees:
				resultBitmap = sourceBitmap.ConvolutionFilter(Matrix.MotionBlur9x9At135Degrees, 0.1111111111111111, 0);
				break;
			case Blur.BlurType.Median3x3:
				resultBitmap = sourceBitmap.MedianFilter(3);
				break;
			case Blur.BlurType.Median5x5:
				resultBitmap = sourceBitmap.MedianFilter(5);
				break;
			case Blur.BlurType.Median7x7:
				resultBitmap = sourceBitmap.MedianFilter(7);
				break;
			case Blur.BlurType.Median9x9:
				resultBitmap = sourceBitmap.MedianFilter(9);
				break;
			case Blur.BlurType.Median11x11:
				resultBitmap = sourceBitmap.MedianFilter(11);
				break;
			}
			return resultBitmap;
		}

		private static System.Drawing.Bitmap ConvolutionFilter(this System.Drawing.Bitmap sourceBitmap, double[,] filterMatrix, double factor = 1.0, int bias = 0)
		{
			System.Drawing.Imaging.BitmapData sourceData = sourceBitmap.LockBits(new System.Drawing.Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];
			byte[] resultBuffer = new byte[sourceData.Stride * sourceData.Height];
			Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
			sourceBitmap.UnlockBits(sourceData);
			int filterWidth = filterMatrix.GetLength(1);
			int filterHeight = filterMatrix.GetLength(0);
			int filterOffset = (filterWidth - 1) / 2;
			for (int offsetY = filterOffset; offsetY < sourceBitmap.Height - filterOffset; offsetY++)
			{
				for (int offsetX = filterOffset; offsetX < sourceBitmap.Width - filterOffset; offsetX++)
				{
					double blue = 0.0;
					double green = 0.0;
					double red = 0.0;
					int byteOffset = offsetY * sourceData.Stride + offsetX * 4;
					for (int filterY = -filterOffset; filterY <= filterOffset; filterY++)
					{
						for (int filterX = -filterOffset; filterX <= filterOffset; filterX++)
						{
							int calcOffset = byteOffset + filterX * 4 + filterY * sourceData.Stride;
							blue += (double)pixelBuffer[calcOffset] * filterMatrix[filterY + filterOffset, filterX + filterOffset];
							green += (double)pixelBuffer[calcOffset + 1] * filterMatrix[filterY + filterOffset, filterX + filterOffset];
							red += (double)pixelBuffer[calcOffset + 2] * filterMatrix[filterY + filterOffset, filterX + filterOffset];
						}
					}
					blue = factor * blue + (double)bias;
					green = factor * green + (double)bias;
					red = factor * red + (double)bias;
					blue = ((blue > 255.0) ? 255.0 : ((blue < 0.0) ? 0.0 : blue));
					green = ((green > 255.0) ? 255.0 : ((green < 0.0) ? 0.0 : green));
					red = ((red > 255.0) ? 255.0 : ((red < 0.0) ? 0.0 : red));
					resultBuffer[byteOffset] = (byte)blue;
					resultBuffer[byteOffset + 1] = (byte)green;
					resultBuffer[byteOffset + 2] = (byte)red;
					resultBuffer[byteOffset + 3] = 255;
				}
			}
			System.Drawing.Bitmap resultBitmap = new System.Drawing.Bitmap(sourceBitmap.Width, sourceBitmap.Height);
			System.Drawing.Imaging.BitmapData resultData = resultBitmap.LockBits(new System.Drawing.Rectangle(0, 0, resultBitmap.Width, resultBitmap.Height), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			Marshal.Copy(resultBuffer, 0, resultData.Scan0, resultBuffer.Length);
			resultBitmap.UnlockBits(resultData);
			return resultBitmap;
		}

		public static System.Drawing.Bitmap MedianFilter(this System.Drawing.Bitmap sourceBitmap, int matrixSize)
		{
			System.Drawing.Imaging.BitmapData sourceData = sourceBitmap.LockBits(new System.Drawing.Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];
			byte[] resultBuffer = new byte[sourceData.Stride * sourceData.Height];
			Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
			sourceBitmap.UnlockBits(sourceData);
			int filterOffset = (matrixSize - 1) / 2;
			List<int> neighbourPixels = new List<int>();
			for (int offsetY = filterOffset; offsetY < sourceBitmap.Height - filterOffset; offsetY++)
			{
				for (int offsetX = filterOffset; offsetX < sourceBitmap.Width - filterOffset; offsetX++)
				{
					int byteOffset = offsetY * sourceData.Stride + offsetX * 4;
					neighbourPixels.Clear();
					for (int filterY = -filterOffset; filterY <= filterOffset; filterY++)
					{
						for (int filterX = -filterOffset; filterX <= filterOffset; filterX++)
						{
							int calcOffset = byteOffset + filterX * 4 + filterY * sourceData.Stride;
							neighbourPixels.Add(BitConverter.ToInt32(pixelBuffer, calcOffset));
						}
					}
					neighbourPixels.Sort();
					byte[] middlePixel = BitConverter.GetBytes(neighbourPixels[filterOffset]);
					resultBuffer[byteOffset] = middlePixel[0];
					resultBuffer[byteOffset + 1] = middlePixel[1];
					resultBuffer[byteOffset + 2] = middlePixel[2];
					resultBuffer[byteOffset + 3] = middlePixel[3];
				}
			}
			System.Drawing.Bitmap resultBitmap = new System.Drawing.Bitmap(sourceBitmap.Width, sourceBitmap.Height);
			System.Drawing.Imaging.BitmapData resultData = resultBitmap.LockBits(new System.Drawing.Rectangle(0, 0, resultBitmap.Width, resultBitmap.Height), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			Marshal.Copy(resultBuffer, 0, resultData.Scan0, resultBuffer.Length);
			resultBitmap.UnlockBits(resultData);
			return resultBitmap;
		}
	}
}
