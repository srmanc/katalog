using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace KatalogUmetnin.Properties
{
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode, CompilerGenerated]
	internal class Resources
	{
		private static ResourceManager resourceMan;

		private static CultureInfo resourceCulture;

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static ResourceManager ResourceManager
		{
			get
			{
				if (object.ReferenceEquals(Resources.resourceMan, null))
				{
					ResourceManager temp = new ResourceManager("KatalogUmetnin.Properties.Resources", typeof(Resources).Assembly);
					Resources.resourceMan = temp;
				}
				return Resources.resourceMan;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return Resources.resourceCulture;
			}
			set
			{
				Resources.resourceCulture = value;
			}
		}

		internal static System.Drawing.Bitmap Actions_document_open_icon
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("Actions_document_open_icon", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap Actions_document_save_icon
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("Actions_document_save_icon", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap Actions_document_save_icon_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("Actions_document_save_icon_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap add_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("add_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap back
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("back", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap cancl_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("cancl_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap CheckMark_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("CheckMark_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap close_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("close_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap crop_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("crop_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap crop_32
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("crop_32", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap cursor_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("cursor_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap cursor_32
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("cursor_32", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap database_down
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("database_down", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap database_search
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("database_search", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap delete_icon
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("delete_icon", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap delete_x_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("delete_x_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap display_log
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("display_log", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap docs_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("docs_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap down
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("down", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap Exclamation_gray_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("Exclamation_gray_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap Exclamation_red_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("Exclamation_red_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap export_icon
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("export-icon", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap File_Pictures_icon_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("File_Pictures_icon_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap Folders_New_Folder_icon
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("Folders_New_Folder_icon", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap Graphics_Painting_icon
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("Graphics_Painting_icon", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap ICO_Save_ico_16x16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("ICO_Save_ico_16x16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap impt_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("impt_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap next
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("next", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap open_document_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("open_document_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap printer_blue_icon
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("printer-blue-icon", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap Purse_16x16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("Purse_16x16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap Purse_icon_32
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("Purse-icon_32", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap ref_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("ref_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap remove
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("remove", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap remove_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("remove_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap resize_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("resize_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap resize_32
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("resize_32", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap rotate_ccw_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("rotate_ccw_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap rotate_ccw_32
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("rotate_ccw_32", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap rotate_cw_16
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("rotate_cw_16", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap rotate_cw_32
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("rotate_cw_32", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap Search_icon
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("Search-icon", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap server_database_icon
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("server_database_icon", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap settings_icon
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("settings-icon", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap slideshow_icon
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("slideshow_icon", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap Status_battery_charging_icon
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("Status_battery_charging_icon", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap ui_menu_blue_icon
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("ui_menu_blue_icon", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap undo
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("undo", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal static System.Drawing.Bitmap up
		{
			get
			{
				object obj = Resources.ResourceManager.GetObject("up", Resources.resourceCulture);
				return (System.Drawing.Bitmap)obj;
			}
		}

		internal Resources()
		{
		}
	}
}
