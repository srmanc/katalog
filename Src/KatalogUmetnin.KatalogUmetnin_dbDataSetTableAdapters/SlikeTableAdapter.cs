using KatalogUmetnin.Properties;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Diagnostics;

namespace KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters
{
	[DataObject(true), HelpKeyword("vs.data.TableAdapter"), Designer("Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), DesignerCategory("code"), ToolboxItem(true)]
	public class SlikeTableAdapter : Component
	{
		private OleDbDataAdapter _adapter;

		private OleDbConnection _connection;

		private OleDbTransaction _transaction;

		private OleDbCommand[] _commandCollection;

		private bool _clearBeforeFill;

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected internal OleDbDataAdapter Adapter
		{
			get
			{
				if (this._adapter == null)
				{
					this.InitAdapter();
				}
				return this._adapter;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		internal OleDbConnection Connection
		{
			get
			{
				if (this._connection == null)
				{
					this.InitConnection();
				}
				return this._connection;
			}
			set
			{
				this._connection = value;
				if (this.Adapter.InsertCommand != null)
				{
					this.Adapter.InsertCommand.Connection = value;
				}
				if (this.Adapter.DeleteCommand != null)
				{
					this.Adapter.DeleteCommand.Connection = value;
				}
				if (this.Adapter.UpdateCommand != null)
				{
					this.Adapter.UpdateCommand.Connection = value;
				}
				for (int i = 0; i < this.CommandCollection.Length; i++)
				{
					if (this.CommandCollection[i] != null)
					{
						this.CommandCollection[i].Connection = value;
					}
				}
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		internal OleDbTransaction Transaction
		{
			get
			{
				return this._transaction;
			}
			set
			{
				this._transaction = value;
				for (int i = 0; i < this.CommandCollection.Length; i++)
				{
					this.CommandCollection[i].Transaction = this._transaction;
				}
				if (this.Adapter != null && this.Adapter.DeleteCommand != null)
				{
					this.Adapter.DeleteCommand.Transaction = this._transaction;
				}
				if (this.Adapter != null && this.Adapter.InsertCommand != null)
				{
					this.Adapter.InsertCommand.Transaction = this._transaction;
				}
				if (this.Adapter != null && this.Adapter.UpdateCommand != null)
				{
					this.Adapter.UpdateCommand.Transaction = this._transaction;
				}
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected OleDbCommand[] CommandCollection
		{
			get
			{
				if (this._commandCollection == null)
				{
					this.InitCommandCollection();
				}
				return this._commandCollection;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public bool ClearBeforeFill
		{
			get
			{
				return this._clearBeforeFill;
			}
			set
			{
				this._clearBeforeFill = value;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public SlikeTableAdapter()
		{
			this.ClearBeforeFill = true;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void InitAdapter()
		{
			this._adapter = new OleDbDataAdapter();
			DataTableMapping tableMapping = new DataTableMapping();
			tableMapping.SourceTable = "Table";
			tableMapping.DataSetTable = "Slike";
			tableMapping.ColumnMappings.Add("ID", "ID");
			tableMapping.ColumnMappings.Add("Nr", "Nr");
			tableMapping.ColumnMappings.Add("Ime", "Ime");
			tableMapping.ColumnMappings.Add("VrstniRed", "VrstniRed");
			tableMapping.ColumnMappings.Add("Zoom", "Zoom");
			tableMapping.ColumnMappings.Add("X", "X");
			tableMapping.ColumnMappings.Add("Y", "Y");
			tableMapping.ColumnMappings.Add("Visible", "Visible");
			this._adapter.TableMappings.Add(tableMapping);
			this._adapter.DeleteCommand = new OleDbCommand();
			this._adapter.DeleteCommand.Connection = this.Connection;
			this._adapter.DeleteCommand.CommandText = "DELETE FROM `Slike` WHERE ((`ID` = ?) AND ((? = 1 AND `Nr` IS NULL) OR (`Nr` = ?)) AND ((? = 1 AND `Ime` IS NULL) OR (`Ime` = ?)) AND ((? = 1 AND `VrstniRed` IS NULL) OR (`VrstniRed` = ?)) AND ((? = 1 AND `Zoom` IS NULL) OR (`Zoom` = ?)) AND ((? = 1 AND `X` IS NULL) OR (`X` = ?)) AND ((? = 1 AND `Y` IS NULL) OR (`Y` = ?)) AND ((? = 1 AND `Visible` IS NULL) OR (`Visible` = ?)))";
			this._adapter.DeleteCommand.CommandType = CommandType.Text;
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_ID", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "ID", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_Ime", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Ime", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_Ime", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "Ime", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_VrstniRed", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "VrstniRed", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_VrstniRed", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "VrstniRed", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_Zoom", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Zoom", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_Zoom", OleDbType.Double, 0, ParameterDirection.Input, 0, 0, "Zoom", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_X", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "X", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_X", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "X", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_Y", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Y", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_Y", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Y", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_Visible", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Visible", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_Visible", OleDbType.Boolean, 0, ParameterDirection.Input, 0, 0, "Visible", DataRowVersion.Original, false, null));
			this._adapter.InsertCommand = new OleDbCommand();
			this._adapter.InsertCommand.Connection = this.Connection;
			this._adapter.InsertCommand.CommandText = "INSERT INTO `Slike` (`Nr`, `Ime`, `VrstniRed`, `Zoom`, `X`, `Y`, `Visible`) VALUES (?, ?, ?, ?, ?, ?, ?)";
			this._adapter.InsertCommand.CommandType = CommandType.Text;
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("Ime", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "Ime", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("VrstniRed", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "VrstniRed", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("Zoom", OleDbType.Double, 0, ParameterDirection.Input, 0, 0, "Zoom", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("X", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "X", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("Y", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Y", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("Visible", OleDbType.Boolean, 0, ParameterDirection.Input, 0, 0, "Visible", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand = new OleDbCommand();
			this._adapter.UpdateCommand.Connection = this.Connection;
			this._adapter.UpdateCommand.CommandText = "UPDATE `Slike` SET `Nr` = ?, `Ime` = ?, `VrstniRed` = ?, `Zoom` = ?, `X` = ?, `Y` = ?, `Visible` = ? WHERE ((`ID` = ?) AND ((? = 1 AND `Nr` IS NULL) OR (`Nr` = ?)) AND ((? = 1 AND `Ime` IS NULL) OR (`Ime` = ?)) AND ((? = 1 AND `VrstniRed` IS NULL) OR (`VrstniRed` = ?)) AND ((? = 1 AND `Zoom` IS NULL) OR (`Zoom` = ?)) AND ((? = 1 AND `X` IS NULL) OR (`X` = ?)) AND ((? = 1 AND `Y` IS NULL) OR (`Y` = ?)) AND ((? = 1 AND `Visible` IS NULL) OR (`Visible` = ?)))";
			this._adapter.UpdateCommand.CommandType = CommandType.Text;
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Ime", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "Ime", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("VrstniRed", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "VrstniRed", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Zoom", OleDbType.Double, 0, ParameterDirection.Input, 0, 0, "Zoom", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("X", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "X", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Y", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Y", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Visible", OleDbType.Boolean, 0, ParameterDirection.Input, 0, 0, "Visible", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_ID", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "ID", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_Ime", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Ime", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_Ime", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "Ime", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_VrstniRed", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "VrstniRed", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_VrstniRed", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "VrstniRed", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_Zoom", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Zoom", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_Zoom", OleDbType.Double, 0, ParameterDirection.Input, 0, 0, "Zoom", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_X", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "X", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_X", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "X", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_Y", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Y", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_Y", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Y", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_Visible", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Visible", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_Visible", OleDbType.Boolean, 0, ParameterDirection.Input, 0, 0, "Visible", DataRowVersion.Original, false, null));
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void InitConnection()
		{
			this._connection = new OleDbConnection();
			this._connection.ConnectionString = Settings.Get.DbConnectionString;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void InitCommandCollection()
		{
			this._commandCollection = new OleDbCommand[2];
			this._commandCollection[0] = new OleDbCommand();
			this._commandCollection[0].Connection = this.Connection;
			this._commandCollection[0].CommandText = "SELECT        ID, Nr, Ime, VrstniRed, Zoom, X, Y, Visible\r\nFROM            Slike\r\n";
			this._commandCollection[0].CommandType = CommandType.Text;
			this._commandCollection[1] = new OleDbCommand();
			this._commandCollection[1].Connection = this.Connection;
			this._commandCollection[1].CommandText = "SELECT ID, Ime, Nr, Visible, VrstniRed, X, Y, Zoom, Visible FROM Slike WHERE (Nr = ?) ORDER BY VrstniRed";
			this._commandCollection[1].CommandType = CommandType.Text;
			this._commandCollection[1].Parameters.Add(new OleDbParameter("Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Current, false, null));
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Fill, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Fill(KatalogUmetnin_dbDataSet.SlikeDataTable dataTable)
		{
			this.Adapter.SelectCommand = this.CommandCollection[0];
			if (this.ClearBeforeFill)
			{
				dataTable.Clear();
			}
			return this.Adapter.Fill(dataTable);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Select, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual KatalogUmetnin_dbDataSet.SlikeDataTable GetData()
		{
			this.Adapter.SelectCommand = this.CommandCollection[0];
			KatalogUmetnin_dbDataSet.SlikeDataTable dataTable = new KatalogUmetnin_dbDataSet.SlikeDataTable();
			this.Adapter.Fill(dataTable);
			return dataTable;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Fill, false), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int FillByNr(KatalogUmetnin_dbDataSet.SlikeDataTable dataTable, int? Nr)
		{
			this.Adapter.SelectCommand = this.CommandCollection[1];
			if (Nr.HasValue)
			{
				this.Adapter.SelectCommand.Parameters[0].Value = Nr.Value;
			}
			else
			{
				this.Adapter.SelectCommand.Parameters[0].Value = DBNull.Value;
			}
			if (this.ClearBeforeFill)
			{
				dataTable.Clear();
			}
			return this.Adapter.Fill(dataTable);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Select, false), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual KatalogUmetnin_dbDataSet.SlikeDataTable GetDataByNr(int? Nr)
		{
			this.Adapter.SelectCommand = this.CommandCollection[1];
			if (Nr.HasValue)
			{
				this.Adapter.SelectCommand.Parameters[0].Value = Nr.Value;
			}
			else
			{
				this.Adapter.SelectCommand.Parameters[0].Value = DBNull.Value;
			}
			KatalogUmetnin_dbDataSet.SlikeDataTable dataTable = new KatalogUmetnin_dbDataSet.SlikeDataTable();
			this.Adapter.Fill(dataTable);
			return dataTable;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(KatalogUmetnin_dbDataSet.SlikeDataTable dataTable)
		{
			return this.Adapter.Update(dataTable);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(KatalogUmetnin_dbDataSet dataSet)
		{
			return this.Adapter.Update(dataSet, "Slike");
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(DataRow dataRow)
		{
			return this.Adapter.Update(new DataRow[]
			{
				dataRow
			});
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(DataRow[] dataRows)
		{
			return this.Adapter.Update(dataRows);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Delete, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Delete(int Original_ID, int? Original_Nr, string Original_Ime, int? Original_VrstniRed, double? Original_Zoom, int? Original_X, int? Original_Y, bool Original_Visible)
		{
			this.Adapter.DeleteCommand.Parameters[0].Value = Original_ID;
			if (Original_Nr.HasValue)
			{
				this.Adapter.DeleteCommand.Parameters[1].Value = 0;
				this.Adapter.DeleteCommand.Parameters[2].Value = Original_Nr.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[1].Value = 1;
				this.Adapter.DeleteCommand.Parameters[2].Value = DBNull.Value;
			}
			if (Original_Ime == null)
			{
				this.Adapter.DeleteCommand.Parameters[3].Value = 1;
				this.Adapter.DeleteCommand.Parameters[4].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[3].Value = 0;
				this.Adapter.DeleteCommand.Parameters[4].Value = Original_Ime;
			}
			if (Original_VrstniRed.HasValue)
			{
				this.Adapter.DeleteCommand.Parameters[5].Value = 0;
				this.Adapter.DeleteCommand.Parameters[6].Value = Original_VrstniRed.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[5].Value = 1;
				this.Adapter.DeleteCommand.Parameters[6].Value = DBNull.Value;
			}
			if (Original_Zoom.HasValue)
			{
				this.Adapter.DeleteCommand.Parameters[7].Value = 0;
				this.Adapter.DeleteCommand.Parameters[8].Value = Original_Zoom.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[7].Value = 1;
				this.Adapter.DeleteCommand.Parameters[8].Value = DBNull.Value;
			}
			if (Original_X.HasValue)
			{
				this.Adapter.DeleteCommand.Parameters[9].Value = 0;
				this.Adapter.DeleteCommand.Parameters[10].Value = Original_X.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[9].Value = 1;
				this.Adapter.DeleteCommand.Parameters[10].Value = DBNull.Value;
			}
			if (Original_Y.HasValue)
			{
				this.Adapter.DeleteCommand.Parameters[11].Value = 0;
				this.Adapter.DeleteCommand.Parameters[12].Value = Original_Y.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[11].Value = 1;
				this.Adapter.DeleteCommand.Parameters[12].Value = DBNull.Value;
			}
			this.Adapter.DeleteCommand.Parameters[13].Value = 0;
			this.Adapter.DeleteCommand.Parameters[14].Value = Original_Visible;
			ConnectionState previousConnectionState = this.Adapter.DeleteCommand.Connection.State;
			if ((this.Adapter.DeleteCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				this.Adapter.DeleteCommand.Connection.Open();
			}
			int result;
			try
			{
				int returnValue = this.Adapter.DeleteCommand.ExecuteNonQuery();
				result = returnValue;
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					this.Adapter.DeleteCommand.Connection.Close();
				}
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Insert, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Insert(int? Nr, string Ime, int? VrstniRed, double? Zoom, int? X, int? Y, bool Visible)
		{
			if (Nr.HasValue)
			{
				this.Adapter.InsertCommand.Parameters[0].Value = Nr.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[0].Value = DBNull.Value;
			}
			if (Ime == null)
			{
				this.Adapter.InsertCommand.Parameters[1].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[1].Value = Ime;
			}
			if (VrstniRed.HasValue)
			{
				this.Adapter.InsertCommand.Parameters[2].Value = VrstniRed.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[2].Value = DBNull.Value;
			}
			if (Zoom.HasValue)
			{
				this.Adapter.InsertCommand.Parameters[3].Value = Zoom.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[3].Value = DBNull.Value;
			}
			if (X.HasValue)
			{
				this.Adapter.InsertCommand.Parameters[4].Value = X.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[4].Value = DBNull.Value;
			}
			if (Y.HasValue)
			{
				this.Adapter.InsertCommand.Parameters[5].Value = Y.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[5].Value = DBNull.Value;
			}
			this.Adapter.InsertCommand.Parameters[6].Value = Visible;
			ConnectionState previousConnectionState = this.Adapter.InsertCommand.Connection.State;
			if ((this.Adapter.InsertCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				this.Adapter.InsertCommand.Connection.Open();
			}
			int result;
			try
			{
				int returnValue = this.Adapter.InsertCommand.ExecuteNonQuery();
				result = returnValue;
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					this.Adapter.InsertCommand.Connection.Close();
				}
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Update, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(int? Nr, string Ime, int? VrstniRed, double? Zoom, int? X, int? Y, bool Visible, int Original_ID, int? Original_Nr, string Original_Ime, int? Original_VrstniRed, double? Original_Zoom, int? Original_X, int? Original_Y, bool Original_Visible)
		{
			if (Nr.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[0].Value = Nr.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[0].Value = DBNull.Value;
			}
			if (Ime == null)
			{
				this.Adapter.UpdateCommand.Parameters[1].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[1].Value = Ime;
			}
			if (VrstniRed.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[2].Value = VrstniRed.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[2].Value = DBNull.Value;
			}
			if (Zoom.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[3].Value = Zoom.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[3].Value = DBNull.Value;
			}
			if (X.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[4].Value = X.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[4].Value = DBNull.Value;
			}
			if (Y.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[5].Value = Y.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[5].Value = DBNull.Value;
			}
			this.Adapter.UpdateCommand.Parameters[6].Value = Visible;
			this.Adapter.UpdateCommand.Parameters[7].Value = Original_ID;
			if (Original_Nr.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[8].Value = 0;
				this.Adapter.UpdateCommand.Parameters[9].Value = Original_Nr.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[8].Value = 1;
				this.Adapter.UpdateCommand.Parameters[9].Value = DBNull.Value;
			}
			if (Original_Ime == null)
			{
				this.Adapter.UpdateCommand.Parameters[10].Value = 1;
				this.Adapter.UpdateCommand.Parameters[11].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[10].Value = 0;
				this.Adapter.UpdateCommand.Parameters[11].Value = Original_Ime;
			}
			if (Original_VrstniRed.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[12].Value = 0;
				this.Adapter.UpdateCommand.Parameters[13].Value = Original_VrstniRed.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[12].Value = 1;
				this.Adapter.UpdateCommand.Parameters[13].Value = DBNull.Value;
			}
			if (Original_Zoom.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[14].Value = 0;
				this.Adapter.UpdateCommand.Parameters[15].Value = Original_Zoom.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[14].Value = 1;
				this.Adapter.UpdateCommand.Parameters[15].Value = DBNull.Value;
			}
			if (Original_X.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[16].Value = 0;
				this.Adapter.UpdateCommand.Parameters[17].Value = Original_X.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[16].Value = 1;
				this.Adapter.UpdateCommand.Parameters[17].Value = DBNull.Value;
			}
			if (Original_Y.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[18].Value = 0;
				this.Adapter.UpdateCommand.Parameters[19].Value = Original_Y.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[18].Value = 1;
				this.Adapter.UpdateCommand.Parameters[19].Value = DBNull.Value;
			}
			this.Adapter.UpdateCommand.Parameters[20].Value = 0;
			this.Adapter.UpdateCommand.Parameters[21].Value = Original_Visible;
			ConnectionState previousConnectionState = this.Adapter.UpdateCommand.Connection.State;
			if ((this.Adapter.UpdateCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				this.Adapter.UpdateCommand.Connection.Open();
			}
			int result;
			try
			{
				int returnValue = this.Adapter.UpdateCommand.ExecuteNonQuery();
				result = returnValue;
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					this.Adapter.UpdateCommand.Connection.Close();
				}
			}
			return result;
		}
	}
}
