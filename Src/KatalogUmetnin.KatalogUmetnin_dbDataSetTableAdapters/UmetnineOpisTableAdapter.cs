using KatalogUmetnin.Properties;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Diagnostics;

namespace KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters
{
	[DataObject(true), HelpKeyword("vs.data.TableAdapter"), Designer("Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), DesignerCategory("code"), ToolboxItem(true)]
	public class UmetnineOpisTableAdapter : Component
	{
		private OleDbDataAdapter _adapter;

		private OleDbConnection _connection;

		private OleDbTransaction _transaction;

		private OleDbCommand[] _commandCollection;

		private bool _clearBeforeFill;

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected internal OleDbDataAdapter Adapter
		{
			get
			{
				if (this._adapter == null)
				{
					this.InitAdapter();
				}
				return this._adapter;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		internal OleDbConnection Connection
		{
			get
			{
				if (this._connection == null)
				{
					this.InitConnection();
				}
				return this._connection;
			}
			set
			{
				this._connection = value;
				if (this.Adapter.InsertCommand != null)
				{
					this.Adapter.InsertCommand.Connection = value;
				}
				if (this.Adapter.DeleteCommand != null)
				{
					this.Adapter.DeleteCommand.Connection = value;
				}
				if (this.Adapter.UpdateCommand != null)
				{
					this.Adapter.UpdateCommand.Connection = value;
				}
				for (int i = 0; i < this.CommandCollection.Length; i++)
				{
					if (this.CommandCollection[i] != null)
					{
						this.CommandCollection[i].Connection = value;
					}
				}
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		internal OleDbTransaction Transaction
		{
			get
			{
				return this._transaction;
			}
			set
			{
				this._transaction = value;
				for (int i = 0; i < this.CommandCollection.Length; i++)
				{
					this.CommandCollection[i].Transaction = this._transaction;
				}
				if (this.Adapter != null && this.Adapter.DeleteCommand != null)
				{
					this.Adapter.DeleteCommand.Transaction = this._transaction;
				}
				if (this.Adapter != null && this.Adapter.InsertCommand != null)
				{
					this.Adapter.InsertCommand.Transaction = this._transaction;
				}
				if (this.Adapter != null && this.Adapter.UpdateCommand != null)
				{
					this.Adapter.UpdateCommand.Transaction = this._transaction;
				}
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected OleDbCommand[] CommandCollection
		{
			get
			{
				if (this._commandCollection == null)
				{
					this.InitCommandCollection();
				}
				return this._commandCollection;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public bool ClearBeforeFill
		{
			get
			{
				return this._clearBeforeFill;
			}
			set
			{
				this._clearBeforeFill = value;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public UmetnineOpisTableAdapter()
		{
			this.ClearBeforeFill = true;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void InitAdapter()
		{
			this._adapter = new OleDbDataAdapter();
			DataTableMapping tableMapping = new DataTableMapping();
			tableMapping.SourceTable = "Table";
			tableMapping.DataSetTable = "UmetnineOpis";
			tableMapping.ColumnMappings.Add("Nr", "Nr");
			tableMapping.ColumnMappings.Add("opis", "opis");
			this._adapter.TableMappings.Add(tableMapping);
			this._adapter.DeleteCommand = new OleDbCommand();
			this._adapter.DeleteCommand.Connection = this.Connection;
			this._adapter.DeleteCommand.CommandText = "DELETE FROM `Umetnine` WHERE ((`Nr` = ?) AND ((? = 1 AND `opis` IS NULL) OR (`opis` = ?)))";
			this._adapter.DeleteCommand.CommandType = CommandType.Text;
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_opis", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_opis", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Original, false, null));
			this._adapter.InsertCommand = new OleDbCommand();
			this._adapter.InsertCommand.Connection = this.Connection;
			this._adapter.InsertCommand.CommandText = "INSERT INTO `Umetnine` (`opis`) VALUES (?)";
			this._adapter.InsertCommand.CommandType = CommandType.Text;
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("opis", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand = new OleDbCommand();
			this._adapter.UpdateCommand.Connection = this.Connection;
			this._adapter.UpdateCommand.CommandText = "UPDATE `Umetnine` SET `opis` = ? WHERE ((`Nr` = ?) AND ((? = 1 AND `opis` IS NULL) OR (`opis` = ?)))";
			this._adapter.UpdateCommand.CommandType = CommandType.Text;
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("opis", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_opis", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_opis", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Original, false, null));
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void InitConnection()
		{
			this._connection = new OleDbConnection();
			this._connection.ConnectionString = Settings.Get.DbConnectionString;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void InitCommandCollection()
		{
			this._commandCollection = new OleDbCommand[1];
			this._commandCollection[0] = new OleDbCommand();
			this._commandCollection[0].Connection = this.Connection;
			this._commandCollection[0].CommandText = "SELECT        Nr, opis\r\nFROM            Umetnine\r\nWHERE        (opis <> '')";
			this._commandCollection[0].CommandType = CommandType.Text;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Fill, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Fill(KatalogUmetnin_dbDataSet.UmetnineOpisDataTable dataTable)
		{
			this.Adapter.SelectCommand = this.CommandCollection[0];
			if (this.ClearBeforeFill)
			{
				dataTable.Clear();
			}
			return this.Adapter.Fill(dataTable);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Select, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual KatalogUmetnin_dbDataSet.UmetnineOpisDataTable GetData()
		{
			this.Adapter.SelectCommand = this.CommandCollection[0];
			KatalogUmetnin_dbDataSet.UmetnineOpisDataTable dataTable = new KatalogUmetnin_dbDataSet.UmetnineOpisDataTable();
			this.Adapter.Fill(dataTable);
			return dataTable;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(KatalogUmetnin_dbDataSet.UmetnineOpisDataTable dataTable)
		{
			return this.Adapter.Update(dataTable);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(KatalogUmetnin_dbDataSet dataSet)
		{
			return this.Adapter.Update(dataSet, "UmetnineOpis");
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(DataRow dataRow)
		{
			return this.Adapter.Update(new DataRow[]
			{
				dataRow
			});
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(DataRow[] dataRows)
		{
			return this.Adapter.Update(dataRows);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Delete, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Delete(int Original_Nr, string Original_opis)
		{
			this.Adapter.DeleteCommand.Parameters[0].Value = Original_Nr;
			if (Original_opis == null)
			{
				this.Adapter.DeleteCommand.Parameters[1].Value = 1;
				this.Adapter.DeleteCommand.Parameters[2].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[1].Value = 0;
				this.Adapter.DeleteCommand.Parameters[2].Value = Original_opis;
			}
			ConnectionState previousConnectionState = this.Adapter.DeleteCommand.Connection.State;
			if ((this.Adapter.DeleteCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				this.Adapter.DeleteCommand.Connection.Open();
			}
			int result;
			try
			{
				int returnValue = this.Adapter.DeleteCommand.ExecuteNonQuery();
				result = returnValue;
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					this.Adapter.DeleteCommand.Connection.Close();
				}
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Insert, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Insert(string opis)
		{
			if (opis == null)
			{
				this.Adapter.InsertCommand.Parameters[0].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[0].Value = opis;
			}
			ConnectionState previousConnectionState = this.Adapter.InsertCommand.Connection.State;
			if ((this.Adapter.InsertCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				this.Adapter.InsertCommand.Connection.Open();
			}
			int result;
			try
			{
				int returnValue = this.Adapter.InsertCommand.ExecuteNonQuery();
				result = returnValue;
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					this.Adapter.InsertCommand.Connection.Close();
				}
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Update, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(string opis, int Original_Nr, string Original_opis)
		{
			if (opis == null)
			{
				this.Adapter.UpdateCommand.Parameters[0].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[0].Value = opis;
			}
			this.Adapter.UpdateCommand.Parameters[1].Value = Original_Nr;
			if (Original_opis == null)
			{
				this.Adapter.UpdateCommand.Parameters[2].Value = 1;
				this.Adapter.UpdateCommand.Parameters[3].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[2].Value = 0;
				this.Adapter.UpdateCommand.Parameters[3].Value = Original_opis;
			}
			ConnectionState previousConnectionState = this.Adapter.UpdateCommand.Connection.State;
			if ((this.Adapter.UpdateCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				this.Adapter.UpdateCommand.Connection.Open();
			}
			int result;
			try
			{
				int returnValue = this.Adapter.UpdateCommand.ExecuteNonQuery();
				result = returnValue;
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					this.Adapter.UpdateCommand.Connection.Close();
				}
			}
			return result;
		}
	}
}
