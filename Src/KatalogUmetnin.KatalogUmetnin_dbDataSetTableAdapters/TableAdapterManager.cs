using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Diagnostics;

namespace KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters
{
	[HelpKeyword("vs.data.TableAdapterManager"), Designer("Microsoft.VSDesigner.DataSource.Design.TableAdapterManagerDesigner, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), DesignerCategory("code"), ToolboxItem(true)]
	public class TableAdapterManager : Component
	{
		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
		public enum UpdateOrderOption
		{
			InsertUpdateDelete,
			UpdateInsertDelete
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
		private class SelfReferenceComparer : IComparer<DataRow>
		{
			private DataRelation _relation;

			private int _childFirst;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal SelfReferenceComparer(DataRelation relation, bool childFirst)
			{
				this._relation = relation;
				if (childFirst)
				{
					this._childFirst = -1;
				}
				else
				{
					this._childFirst = 1;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			private DataRow GetRoot(DataRow row, out int distance)
			{
				Debug.Assert(row != null);
				DataRow root = row;
				distance = 0;
				IDictionary<DataRow, DataRow> traversedRows = new Dictionary<DataRow, DataRow>();
				traversedRows[row] = row;
				DataRow parent = row.GetParentRow(this._relation, DataRowVersion.Default);
				while (parent != null && !traversedRows.ContainsKey(parent))
				{
					distance++;
					root = parent;
					traversedRows[parent] = parent;
					parent = parent.GetParentRow(this._relation, DataRowVersion.Default);
				}
				if (distance == 0)
				{
					traversedRows.Clear();
					traversedRows[row] = row;
					parent = row.GetParentRow(this._relation, DataRowVersion.Original);
					while (parent != null && !traversedRows.ContainsKey(parent))
					{
						distance++;
						root = parent;
						traversedRows[parent] = parent;
						parent = parent.GetParentRow(this._relation, DataRowVersion.Original);
					}
				}
				return root;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public int Compare(DataRow row1, DataRow row2)
			{
				int result;
				if (object.ReferenceEquals(row1, row2))
				{
					result = 0;
				}
				else if (row1 == null)
				{
					result = -1;
				}
				else if (row2 == null)
				{
					result = 1;
				}
				else
				{
					int distance = 0;
					DataRow root = this.GetRoot(row1, out distance);
					int distance2 = 0;
					DataRow root2 = this.GetRoot(row2, out distance2);
					if (object.ReferenceEquals(root, root2))
					{
						result = this._childFirst * distance.CompareTo(distance2);
					}
					else
					{
						Debug.Assert(root.Table != null && root2.Table != null);
						if (root.Table.Rows.IndexOf(root) < root2.Table.Rows.IndexOf(root2))
						{
							result = -1;
						}
						else
						{
							result = 1;
						}
					}
				}
				return result;
			}
		}

		private TableAdapterManager.UpdateOrderOption _updateOrder;

		private UmetnineTableAdapter _umetnineTableAdapter;

		private SlikeTableAdapter _slikeTableAdapter;

		private NapisiTableAdapter _napisiTableAdapter;

		private UmetnineOpisTableAdapter _umetnineOpisTableAdapter;

		private bool _backupDataSetBeforeUpdate;

		private IDbConnection _connection;

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public TableAdapterManager.UpdateOrderOption UpdateOrder
		{
			get
			{
				return this._updateOrder;
			}
			set
			{
				this._updateOrder = value;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Editor("Microsoft.VSDesigner.DataSource.Design.TableAdapterManagerPropertyEditor, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor"), DebuggerNonUserCode]
		public UmetnineTableAdapter UmetnineTableAdapter
		{
			get
			{
				return this._umetnineTableAdapter;
			}
			set
			{
				this._umetnineTableAdapter = value;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Editor("Microsoft.VSDesigner.DataSource.Design.TableAdapterManagerPropertyEditor, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor"), DebuggerNonUserCode]
		public SlikeTableAdapter SlikeTableAdapter
		{
			get
			{
				return this._slikeTableAdapter;
			}
			set
			{
				this._slikeTableAdapter = value;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Editor("Microsoft.VSDesigner.DataSource.Design.TableAdapterManagerPropertyEditor, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor"), DebuggerNonUserCode]
		public NapisiTableAdapter NapisiTableAdapter
		{
			get
			{
				return this._napisiTableAdapter;
			}
			set
			{
				this._napisiTableAdapter = value;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Editor("Microsoft.VSDesigner.DataSource.Design.TableAdapterManagerPropertyEditor, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor"), DebuggerNonUserCode]
		public UmetnineOpisTableAdapter UmetnineOpisTableAdapter
		{
			get
			{
				return this._umetnineOpisTableAdapter;
			}
			set
			{
				this._umetnineOpisTableAdapter = value;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public bool BackupDataSetBeforeUpdate
		{
			get
			{
				return this._backupDataSetBeforeUpdate;
			}
			set
			{
				this._backupDataSetBeforeUpdate = value;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Browsable(false), DebuggerNonUserCode]
		public IDbConnection Connection
		{
			get
			{
				IDbConnection result;
				if (this._connection != null)
				{
					result = this._connection;
				}
				else if (this._umetnineTableAdapter != null && this._umetnineTableAdapter.Connection != null)
				{
					result = this._umetnineTableAdapter.Connection;
				}
				else if (this._slikeTableAdapter != null && this._slikeTableAdapter.Connection != null)
				{
					result = this._slikeTableAdapter.Connection;
				}
				else if (this._napisiTableAdapter != null && this._napisiTableAdapter.Connection != null)
				{
					result = this._napisiTableAdapter.Connection;
				}
				else if (this._umetnineOpisTableAdapter != null && this._umetnineOpisTableAdapter.Connection != null)
				{
					result = this._umetnineOpisTableAdapter.Connection;
				}
				else
				{
					result = null;
				}
				return result;
			}
			set
			{
				this._connection = value;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Browsable(false), DebuggerNonUserCode]
		public int TableAdapterInstanceCount
		{
			get
			{
				int count = 0;
				if (this._umetnineTableAdapter != null)
				{
					count++;
				}
				if (this._slikeTableAdapter != null)
				{
					count++;
				}
				if (this._napisiTableAdapter != null)
				{
					count++;
				}
				if (this._umetnineOpisTableAdapter != null)
				{
					count++;
				}
				return count;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private int UpdateUpdatedRows(KatalogUmetnin_dbDataSet dataSet, List<DataRow> allChangedRows, List<DataRow> allAddedRows)
		{
			int result = 0;
			if (this._umetnineTableAdapter != null)
			{
				DataRow[] updatedRows = dataSet.Umetnine.Select(null, null, DataViewRowState.ModifiedCurrent);
				updatedRows = this.GetRealUpdatedRows(updatedRows, allAddedRows);
				if (updatedRows != null && 0 < updatedRows.Length)
				{
					result += this._umetnineTableAdapter.Update(updatedRows);
					allChangedRows.AddRange(updatedRows);
				}
			}
			if (this._slikeTableAdapter != null)
			{
				DataRow[] updatedRows = dataSet.Slike.Select(null, null, DataViewRowState.ModifiedCurrent);
				updatedRows = this.GetRealUpdatedRows(updatedRows, allAddedRows);
				if (updatedRows != null && 0 < updatedRows.Length)
				{
					result += this._slikeTableAdapter.Update(updatedRows);
					allChangedRows.AddRange(updatedRows);
				}
			}
			if (this._napisiTableAdapter != null)
			{
				DataRow[] updatedRows = dataSet.Napisi.Select(null, null, DataViewRowState.ModifiedCurrent);
				updatedRows = this.GetRealUpdatedRows(updatedRows, allAddedRows);
				if (updatedRows != null && 0 < updatedRows.Length)
				{
					result += this._napisiTableAdapter.Update(updatedRows);
					allChangedRows.AddRange(updatedRows);
				}
			}
			if (this._umetnineOpisTableAdapter != null)
			{
				DataRow[] updatedRows = dataSet.UmetnineOpis.Select(null, null, DataViewRowState.ModifiedCurrent);
				updatedRows = this.GetRealUpdatedRows(updatedRows, allAddedRows);
				if (updatedRows != null && 0 < updatedRows.Length)
				{
					result += this._umetnineOpisTableAdapter.Update(updatedRows);
					allChangedRows.AddRange(updatedRows);
				}
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private int UpdateInsertedRows(KatalogUmetnin_dbDataSet dataSet, List<DataRow> allAddedRows)
		{
			int result = 0;
			if (this._umetnineTableAdapter != null)
			{
				DataRow[] addedRows = dataSet.Umetnine.Select(null, null, DataViewRowState.Added);
				if (addedRows != null && 0 < addedRows.Length)
				{
					result += this._umetnineTableAdapter.Update(addedRows);
					allAddedRows.AddRange(addedRows);
				}
			}
			if (this._slikeTableAdapter != null)
			{
				DataRow[] addedRows = dataSet.Slike.Select(null, null, DataViewRowState.Added);
				if (addedRows != null && 0 < addedRows.Length)
				{
					result += this._slikeTableAdapter.Update(addedRows);
					allAddedRows.AddRange(addedRows);
				}
			}
			if (this._napisiTableAdapter != null)
			{
				DataRow[] addedRows = dataSet.Napisi.Select(null, null, DataViewRowState.Added);
				if (addedRows != null && 0 < addedRows.Length)
				{
					result += this._napisiTableAdapter.Update(addedRows);
					allAddedRows.AddRange(addedRows);
				}
			}
			if (this._umetnineOpisTableAdapter != null)
			{
				DataRow[] addedRows = dataSet.UmetnineOpis.Select(null, null, DataViewRowState.Added);
				if (addedRows != null && 0 < addedRows.Length)
				{
					result += this._umetnineOpisTableAdapter.Update(addedRows);
					allAddedRows.AddRange(addedRows);
				}
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private int UpdateDeletedRows(KatalogUmetnin_dbDataSet dataSet, List<DataRow> allChangedRows)
		{
			int result = 0;
			if (this._umetnineOpisTableAdapter != null)
			{
				DataRow[] deletedRows = dataSet.UmetnineOpis.Select(null, null, DataViewRowState.Deleted);
				if (deletedRows != null && 0 < deletedRows.Length)
				{
					result += this._umetnineOpisTableAdapter.Update(deletedRows);
					allChangedRows.AddRange(deletedRows);
				}
			}
			if (this._napisiTableAdapter != null)
			{
				DataRow[] deletedRows = dataSet.Napisi.Select(null, null, DataViewRowState.Deleted);
				if (deletedRows != null && 0 < deletedRows.Length)
				{
					result += this._napisiTableAdapter.Update(deletedRows);
					allChangedRows.AddRange(deletedRows);
				}
			}
			if (this._slikeTableAdapter != null)
			{
				DataRow[] deletedRows = dataSet.Slike.Select(null, null, DataViewRowState.Deleted);
				if (deletedRows != null && 0 < deletedRows.Length)
				{
					result += this._slikeTableAdapter.Update(deletedRows);
					allChangedRows.AddRange(deletedRows);
				}
			}
			if (this._umetnineTableAdapter != null)
			{
				DataRow[] deletedRows = dataSet.Umetnine.Select(null, null, DataViewRowState.Deleted);
				if (deletedRows != null && 0 < deletedRows.Length)
				{
					result += this._umetnineTableAdapter.Update(deletedRows);
					allChangedRows.AddRange(deletedRows);
				}
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private DataRow[] GetRealUpdatedRows(DataRow[] updatedRows, List<DataRow> allAddedRows)
		{
			DataRow[] result;
			if (updatedRows == null || updatedRows.Length < 1)
			{
				result = updatedRows;
			}
			else if (allAddedRows == null || allAddedRows.Count < 1)
			{
				result = updatedRows;
			}
			else
			{
				List<DataRow> realUpdatedRows = new List<DataRow>();
				for (int i = 0; i < updatedRows.Length; i++)
				{
					DataRow row = updatedRows[i];
					if (!allAddedRows.Contains(row))
					{
						realUpdatedRows.Add(row);
					}
				}
				result = realUpdatedRows.ToArray();
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public virtual int UpdateAll(KatalogUmetnin_dbDataSet dataSet)
		{
			if (dataSet == null)
			{
				throw new ArgumentNullException("dataSet");
			}
			int result2;
			if (!dataSet.HasChanges())
			{
				result2 = 0;
			}
			else
			{
				if (this._umetnineTableAdapter != null && !this.MatchTableAdapterConnection(this._umetnineTableAdapter.Connection))
				{
					throw new ArgumentException("All TableAdapters managed by a TableAdapterManager must use the same connection string.");
				}
				if (this._slikeTableAdapter != null && !this.MatchTableAdapterConnection(this._slikeTableAdapter.Connection))
				{
					throw new ArgumentException("All TableAdapters managed by a TableAdapterManager must use the same connection string.");
				}
				if (this._napisiTableAdapter != null && !this.MatchTableAdapterConnection(this._napisiTableAdapter.Connection))
				{
					throw new ArgumentException("All TableAdapters managed by a TableAdapterManager must use the same connection string.");
				}
				if (this._umetnineOpisTableAdapter != null && !this.MatchTableAdapterConnection(this._umetnineOpisTableAdapter.Connection))
				{
					throw new ArgumentException("All TableAdapters managed by a TableAdapterManager must use the same connection string.");
				}
				IDbConnection workConnection = this.Connection;
				if (workConnection == null)
				{
					throw new ApplicationException("TableAdapterManager contains no connection information. Set each TableAdapterManager TableAdapter property to a valid TableAdapter instance.");
				}
				bool workConnOpened = false;
				if ((workConnection.State & ConnectionState.Broken) == ConnectionState.Broken)
				{
					workConnection.Close();
				}
				if (workConnection.State == ConnectionState.Closed)
				{
					workConnection.Open();
					workConnOpened = true;
				}
				IDbTransaction workTransaction = workConnection.BeginTransaction();
				if (workTransaction == null)
				{
					throw new ApplicationException("The transaction cannot begin. The current data connection does not support transactions or the current state is not allowing the transaction to begin.");
				}
				List<DataRow> allChangedRows = new List<DataRow>();
				List<DataRow> allAddedRows = new List<DataRow>();
				List<DataAdapter> adaptersWithAcceptChangesDuringUpdate = new List<DataAdapter>();
				Dictionary<object, IDbConnection> revertConnections = new Dictionary<object, IDbConnection>();
				int result = 0;
				DataSet backupDataSet = null;
				if (this.BackupDataSetBeforeUpdate)
				{
					backupDataSet = new DataSet();
					backupDataSet.Merge(dataSet);
				}
				try
				{
					if (this._umetnineTableAdapter != null)
					{
						revertConnections.Add(this._umetnineTableAdapter, this._umetnineTableAdapter.Connection);
						this._umetnineTableAdapter.Connection = (OleDbConnection)workConnection;
						this._umetnineTableAdapter.Transaction = (OleDbTransaction)workTransaction;
						if (this._umetnineTableAdapter.Adapter.AcceptChangesDuringUpdate)
						{
							this._umetnineTableAdapter.Adapter.AcceptChangesDuringUpdate = false;
							adaptersWithAcceptChangesDuringUpdate.Add(this._umetnineTableAdapter.Adapter);
						}
					}
					if (this._slikeTableAdapter != null)
					{
						revertConnections.Add(this._slikeTableAdapter, this._slikeTableAdapter.Connection);
						this._slikeTableAdapter.Connection = (OleDbConnection)workConnection;
						this._slikeTableAdapter.Transaction = (OleDbTransaction)workTransaction;
						if (this._slikeTableAdapter.Adapter.AcceptChangesDuringUpdate)
						{
							this._slikeTableAdapter.Adapter.AcceptChangesDuringUpdate = false;
							adaptersWithAcceptChangesDuringUpdate.Add(this._slikeTableAdapter.Adapter);
						}
					}
					if (this._napisiTableAdapter != null)
					{
						revertConnections.Add(this._napisiTableAdapter, this._napisiTableAdapter.Connection);
						this._napisiTableAdapter.Connection = (OleDbConnection)workConnection;
						this._napisiTableAdapter.Transaction = (OleDbTransaction)workTransaction;
						if (this._napisiTableAdapter.Adapter.AcceptChangesDuringUpdate)
						{
							this._napisiTableAdapter.Adapter.AcceptChangesDuringUpdate = false;
							adaptersWithAcceptChangesDuringUpdate.Add(this._napisiTableAdapter.Adapter);
						}
					}
					if (this._umetnineOpisTableAdapter != null)
					{
						revertConnections.Add(this._umetnineOpisTableAdapter, this._umetnineOpisTableAdapter.Connection);
						this._umetnineOpisTableAdapter.Connection = (OleDbConnection)workConnection;
						this._umetnineOpisTableAdapter.Transaction = (OleDbTransaction)workTransaction;
						if (this._umetnineOpisTableAdapter.Adapter.AcceptChangesDuringUpdate)
						{
							this._umetnineOpisTableAdapter.Adapter.AcceptChangesDuringUpdate = false;
							adaptersWithAcceptChangesDuringUpdate.Add(this._umetnineOpisTableAdapter.Adapter);
						}
					}
					if (this.UpdateOrder == TableAdapterManager.UpdateOrderOption.UpdateInsertDelete)
					{
						result += this.UpdateUpdatedRows(dataSet, allChangedRows, allAddedRows);
						result += this.UpdateInsertedRows(dataSet, allAddedRows);
					}
					else
					{
						result += this.UpdateInsertedRows(dataSet, allAddedRows);
						result += this.UpdateUpdatedRows(dataSet, allChangedRows, allAddedRows);
					}
					result += this.UpdateDeletedRows(dataSet, allChangedRows);
					workTransaction.Commit();
					if (0 < allAddedRows.Count)
					{
						DataRow[] rows = new DataRow[allAddedRows.Count];
						allAddedRows.CopyTo(rows);
						for (int i = 0; i < rows.Length; i++)
						{
							DataRow row = rows[i];
							row.AcceptChanges();
						}
					}
					if (0 < allChangedRows.Count)
					{
						DataRow[] rows = new DataRow[allChangedRows.Count];
						allChangedRows.CopyTo(rows);
						for (int i = 0; i < rows.Length; i++)
						{
							DataRow row = rows[i];
							row.AcceptChanges();
						}
					}
				}
				catch (Exception ex)
				{
					workTransaction.Rollback();
					if (this.BackupDataSetBeforeUpdate)
					{
						Debug.Assert(backupDataSet != null);
						dataSet.Clear();
						dataSet.Merge(backupDataSet);
					}
					else if (0 < allAddedRows.Count)
					{
						DataRow[] rows = new DataRow[allAddedRows.Count];
						allAddedRows.CopyTo(rows);
						for (int i = 0; i < rows.Length; i++)
						{
							DataRow row = rows[i];
							row.AcceptChanges();
							row.SetAdded();
						}
					}
					throw ex;
				}
				finally
				{
					if (workConnOpened)
					{
						workConnection.Close();
					}
					if (this._umetnineTableAdapter != null)
					{
						this._umetnineTableAdapter.Connection = (OleDbConnection)revertConnections[this._umetnineTableAdapter];
						this._umetnineTableAdapter.Transaction = null;
					}
					if (this._slikeTableAdapter != null)
					{
						this._slikeTableAdapter.Connection = (OleDbConnection)revertConnections[this._slikeTableAdapter];
						this._slikeTableAdapter.Transaction = null;
					}
					if (this._napisiTableAdapter != null)
					{
						this._napisiTableAdapter.Connection = (OleDbConnection)revertConnections[this._napisiTableAdapter];
						this._napisiTableAdapter.Transaction = null;
					}
					if (this._umetnineOpisTableAdapter != null)
					{
						this._umetnineOpisTableAdapter.Connection = (OleDbConnection)revertConnections[this._umetnineOpisTableAdapter];
						this._umetnineOpisTableAdapter.Transaction = null;
					}
					if (0 < adaptersWithAcceptChangesDuringUpdate.Count)
					{
						DataAdapter[] adapters = new DataAdapter[adaptersWithAcceptChangesDuringUpdate.Count];
						adaptersWithAcceptChangesDuringUpdate.CopyTo(adapters);
						for (int i = 0; i < adapters.Length; i++)
						{
							DataAdapter adapter = adapters[i];
							adapter.AcceptChangesDuringUpdate = true;
						}
					}
				}
				result2 = result;
			}
			return result2;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected virtual void SortSelfReferenceRows(DataRow[] rows, DataRelation relation, bool childFirst)
		{
			Array.Sort<DataRow>(rows, new TableAdapterManager.SelfReferenceComparer(relation, childFirst));
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected virtual bool MatchTableAdapterConnection(IDbConnection inputConnection)
		{
			return this._connection != null || (this.Connection == null || inputConnection == null) || string.Equals(this.Connection.ConnectionString, inputConnection.ConnectionString, StringComparison.Ordinal);
		}
	}
}
