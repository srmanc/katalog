using KatalogUmetnin.Properties;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Diagnostics;

namespace KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters
{
	[DataObject(true), HelpKeyword("vs.data.TableAdapter"), Designer("Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), DesignerCategory("code"), ToolboxItem(true)]
	public class UmetnineTableAdapter : Component
	{
		private OleDbDataAdapter _adapter;

		private OleDbConnection _connection;

		private OleDbTransaction _transaction;

		private OleDbCommand[] _commandCollection;

		private bool _clearBeforeFill;

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected internal OleDbDataAdapter Adapter
		{
			get
			{
				if (this._adapter == null)
				{
					this.InitAdapter();
				}
				return this._adapter;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		internal OleDbConnection Connection
		{
			get
			{
				if (this._connection == null)
				{
					this.InitConnection();
				}
				return this._connection;
			}
			set
			{
				this._connection = value;
				if (this.Adapter.InsertCommand != null)
				{
					this.Adapter.InsertCommand.Connection = value;
				}
				if (this.Adapter.DeleteCommand != null)
				{
					this.Adapter.DeleteCommand.Connection = value;
				}
				if (this.Adapter.UpdateCommand != null)
				{
					this.Adapter.UpdateCommand.Connection = value;
				}
				for (int i = 0; i < this.CommandCollection.Length; i++)
				{
					if (this.CommandCollection[i] != null)
					{
						this.CommandCollection[i].Connection = value;
					}
				}
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		internal OleDbTransaction Transaction
		{
			get
			{
				return this._transaction;
			}
			set
			{
				this._transaction = value;
				for (int i = 0; i < this.CommandCollection.Length; i++)
				{
					this.CommandCollection[i].Transaction = this._transaction;
				}
				if (this.Adapter != null && this.Adapter.DeleteCommand != null)
				{
					this.Adapter.DeleteCommand.Transaction = this._transaction;
				}
				if (this.Adapter != null && this.Adapter.InsertCommand != null)
				{
					this.Adapter.InsertCommand.Transaction = this._transaction;
				}
				if (this.Adapter != null && this.Adapter.UpdateCommand != null)
				{
					this.Adapter.UpdateCommand.Transaction = this._transaction;
				}
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected OleDbCommand[] CommandCollection
		{
			get
			{
				if (this._commandCollection == null)
				{
					this.InitCommandCollection();
				}
				return this._commandCollection;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public bool ClearBeforeFill
		{
			get
			{
				return this._clearBeforeFill;
			}
			set
			{
				this._clearBeforeFill = value;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public UmetnineTableAdapter()
		{
			this.ClearBeforeFill = true;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void InitAdapter()
		{
			this._adapter = new OleDbDataAdapter();
			DataTableMapping tableMapping = new DataTableMapping();
			tableMapping.SourceTable = "Table";
			tableMapping.DataSetTable = "Umetnine";
			tableMapping.ColumnMappings.Add("Nr", "Nr");
			tableMapping.ColumnMappings.Add("st_um_dela", "st_um_dela");
			tableMapping.ColumnMappings.Add("naslov", "naslov");
			tableMapping.ColumnMappings.Add("leto_izdelave", "leto_izdelave");
			tableMapping.ColumnMappings.Add("material", "material");
			tableMapping.ColumnMappings.Add("velikost", "velikost");
			tableMapping.ColumnMappings.Add("zaznamek", "zaznamek");
			tableMapping.ColumnMappings.Add("st_negativ", "st_negativ");
			tableMapping.ColumnMappings.Add("cena", "cena");
			tableMapping.ColumnMappings.Add("cena_dem", "cena_dem");
			tableMapping.ColumnMappings.Add("st_slik", "st_slik");
			tableMapping.ColumnMappings.Add("opis", "opis");
			tableMapping.ColumnMappings.Add("ID", "ID");
			tableMapping.ColumnMappings.Add("datum", "datum");
			tableMapping.ColumnMappings.Add("izkupicek", "izkupicek");
			this._adapter.TableMappings.Add(tableMapping);
			this._adapter.DeleteCommand = new OleDbCommand();
			this._adapter.DeleteCommand.Connection = this.Connection;
			this._adapter.DeleteCommand.CommandText = "DELETE FROM `Umetnine` WHERE ((`Nr` = ?) AND ((? = 1 AND `st_um_dela` IS NULL) OR (`st_um_dela` = ?)) AND ((? = 1 AND `naslov` IS NULL) OR (`naslov` = ?)) AND ((? = 1 AND `leto_izdelave` IS NULL) OR (`leto_izdelave` = ?)) AND ((? = 1 AND `material` IS NULL) OR (`material` = ?)) AND ((? = 1 AND `velikost` IS NULL) OR (`velikost` = ?)) AND ((? = 1 AND `zaznamek` IS NULL) OR (`zaznamek` = ?)) AND ((? = 1 AND `st_negativ` IS NULL) OR (`st_negativ` = ?)) AND ((? = 1 AND `cena` IS NULL) OR (`cena` = ?)) AND ((? = 1 AND `cena_dem` IS NULL) OR (`cena_dem` = ?)) AND ((? = 1 AND `st_slik` IS NULL) OR (`st_slik` = ?)) AND ((? = 1 AND `opis` IS NULL) OR (`opis` = ?)) AND ((? = 1 AND `ID` IS NULL) OR (`ID` = ?)) AND ((? = 1 AND `datum` IS NULL) OR (`datum` = ?)) AND ((? = 1 AND `izkupicek` IS NULL) OR (`izkupicek` = ?)))";
			this._adapter.DeleteCommand.CommandType = CommandType.Text;
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_st_um_dela", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "st_um_dela", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_st_um_dela", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "st_um_dela", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_naslov", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "naslov", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_naslov", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "naslov", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_leto_izdelave", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "leto_izdelave", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_leto_izdelave", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "leto_izdelave", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_material", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "material", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_material", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "material", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_velikost", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "velikost", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_velikost", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "velikost", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_zaznamek", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "zaznamek", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_zaznamek", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "zaznamek", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_st_negativ", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "st_negativ", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_st_negativ", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "st_negativ", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_cena", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "cena", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_cena", OleDbType.Currency, 0, ParameterDirection.Input, 0, 0, "cena", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_cena_dem", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "cena_dem", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_cena_dem", OleDbType.Currency, 0, ParameterDirection.Input, 0, 0, "cena_dem", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_st_slik", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "st_slik", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_st_slik", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "st_slik", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_opis", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_opis", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_ID", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "ID", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_ID", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "ID", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_datum", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "datum", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_datum", OleDbType.Date, 0, ParameterDirection.Input, 0, 0, "datum", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_izkupicek", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "izkupicek", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_izkupicek", OleDbType.Currency, 0, ParameterDirection.Input, 0, 0, "izkupicek", DataRowVersion.Original, false, null));
			this._adapter.InsertCommand = new OleDbCommand();
			this._adapter.InsertCommand.Connection = this.Connection;
			this._adapter.InsertCommand.CommandText = "INSERT INTO `Umetnine` (`st_um_dela`, `naslov`, `leto_izdelave`, `material`, `velikost`, `zaznamek`, `st_negativ`, `cena`, `cena_dem`, `st_slik`, `opis`, `ID`, `datum`, `izkupicek`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			this._adapter.InsertCommand.CommandType = CommandType.Text;
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("st_um_dela", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "st_um_dela", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("naslov", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "naslov", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("leto_izdelave", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "leto_izdelave", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("material", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "material", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("velikost", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "velikost", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("zaznamek", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "zaznamek", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("st_negativ", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "st_negativ", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("cena", OleDbType.Currency, 0, ParameterDirection.Input, 0, 0, "cena", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("cena_dem", OleDbType.Currency, 0, ParameterDirection.Input, 0, 0, "cena_dem", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("st_slik", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "st_slik", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("opis", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("ID", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "ID", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("datum", OleDbType.Date, 0, ParameterDirection.Input, 0, 0, "datum", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("izkupicek", OleDbType.Currency, 0, ParameterDirection.Input, 0, 0, "izkupicek", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand = new OleDbCommand();
			this._adapter.UpdateCommand.Connection = this.Connection;
			this._adapter.UpdateCommand.CommandText = "UPDATE `Umetnine` SET `st_um_dela` = ?, `naslov` = ?, `leto_izdelave` = ?, `material` = ?, `velikost` = ?, `zaznamek` = ?, `st_negativ` = ?, `cena` = ?, `cena_dem` = ?, `st_slik` = ?, `opis` = ?, `ID` = ?, `datum` = ?, `izkupicek` = ? WHERE ((`Nr` = ?) AND ((? = 1 AND `st_um_dela` IS NULL) OR (`st_um_dela` = ?)) AND ((? = 1 AND `naslov` IS NULL) OR (`naslov` = ?)) AND ((? = 1 AND `leto_izdelave` IS NULL) OR (`leto_izdelave` = ?)) AND ((? = 1 AND `material` IS NULL) OR (`material` = ?)) AND ((? = 1 AND `velikost` IS NULL) OR (`velikost` = ?)) AND ((? = 1 AND `zaznamek` IS NULL) OR (`zaznamek` = ?)) AND ((? = 1 AND `st_negativ` IS NULL) OR (`st_negativ` = ?)) AND ((? = 1 AND `cena` IS NULL) OR (`cena` = ?)) AND ((? = 1 AND `cena_dem` IS NULL) OR (`cena_dem` = ?)) AND ((? = 1 AND `st_slik` IS NULL) OR (`st_slik` = ?)) AND ((? = 1 AND `opis` IS NULL) OR (`opis` = ?)) AND ((? = 1 AND `ID` IS NULL) OR (`ID` = ?)) AND ((? = 1 AND `datum` IS NULL) OR (`datum` = ?)) AND ((? = 1 AND `izkupicek` IS NULL) OR (`izkupicek` = ?)))";
			this._adapter.UpdateCommand.CommandType = CommandType.Text;
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("st_um_dela", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "st_um_dela", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("naslov", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "naslov", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("leto_izdelave", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "leto_izdelave", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("material", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "material", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("velikost", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "velikost", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("zaznamek", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "zaznamek", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("st_negativ", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "st_negativ", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("cena", OleDbType.Currency, 0, ParameterDirection.Input, 0, 0, "cena", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("cena_dem", OleDbType.Currency, 0, ParameterDirection.Input, 0, 0, "cena_dem", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("st_slik", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "st_slik", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("opis", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("ID", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "ID", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("datum", OleDbType.Date, 0, ParameterDirection.Input, 0, 0, "datum", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("izkupicek", OleDbType.Currency, 0, ParameterDirection.Input, 0, 0, "izkupicek", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_st_um_dela", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "st_um_dela", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_st_um_dela", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "st_um_dela", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_naslov", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "naslov", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_naslov", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "naslov", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_leto_izdelave", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "leto_izdelave", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_leto_izdelave", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "leto_izdelave", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_material", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "material", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_material", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "material", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_velikost", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "velikost", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_velikost", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "velikost", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_zaznamek", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "zaznamek", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_zaznamek", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "zaznamek", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_st_negativ", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "st_negativ", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_st_negativ", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "st_negativ", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_cena", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "cena", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_cena", OleDbType.Currency, 0, ParameterDirection.Input, 0, 0, "cena", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_cena_dem", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "cena_dem", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_cena_dem", OleDbType.Currency, 0, ParameterDirection.Input, 0, 0, "cena_dem", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_st_slik", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "st_slik", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_st_slik", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "st_slik", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_opis", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_opis", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_ID", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "ID", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_ID", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "ID", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_datum", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "datum", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_datum", OleDbType.Date, 0, ParameterDirection.Input, 0, 0, "datum", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_izkupicek", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "izkupicek", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_izkupicek", OleDbType.Currency, 0, ParameterDirection.Input, 0, 0, "izkupicek", DataRowVersion.Original, false, null));
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void InitConnection()
		{
			this._connection = new OleDbConnection();
			this._connection.ConnectionString = Settings.Get.DbConnectionString;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void InitCommandCollection()
		{
			this._commandCollection = new OleDbCommand[7];
			this._commandCollection[0] = new OleDbCommand();
			this._commandCollection[0].Connection = this.Connection;
			this._commandCollection[0].CommandText = "SELECT        Nr, st_um_dela, naslov, leto_izdelave, material, velikost, zaznamek, st_negativ, cena, cena_dem, st_slik, opis, ID, datum, izkupicek\r\nFROM            Umetnine\r\nORDER BY st_um_dela";
			this._commandCollection[0].CommandType = CommandType.Text;
			this._commandCollection[1] = new OleDbCommand();
			this._commandCollection[1].Connection = this.Connection;
			this._commandCollection[1].CommandText = "DELETE FROM Umetnine\r\nWHERE        (Nr = ?)";
			this._commandCollection[1].CommandType = CommandType.Text;
			this._commandCollection[1].Parameters.Add(new OleDbParameter("Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Original, false, null));
			this._commandCollection[2] = new OleDbCommand();
			this._commandCollection[2].Connection = this.Connection;
			this._commandCollection[2].CommandText = "SELECT ID, Nr, cena, cena_dem, datum, izkupicek, leto_izdelave, material, naslov, opis, st_negativ, st_slik, st_um_dela, velikost, zaznamek FROM Umetnine WHERE (Nr = ?)";
			this._commandCollection[2].CommandType = CommandType.Text;
			this._commandCollection[2].Parameters.Add(new OleDbParameter("Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Current, false, null));
			this._commandCollection[3] = new OleDbCommand();
			this._commandCollection[3].Connection = this.Connection;
			this._commandCollection[3].CommandText = "SELECT ID, Nr, cena, cena_dem, datum, izkupicek, leto_izdelave, material, naslov, opis, st_negativ, st_slik, st_um_dela, velikost, zaznamek FROM Umetnine WHERE (st_um_dela = ?)";
			this._commandCollection[3].CommandType = CommandType.Text;
			this._commandCollection[3].Parameters.Add(new OleDbParameter("st_um_dela", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "st_um_dela", DataRowVersion.Current, false, null));
			this._commandCollection[4] = new OleDbCommand();
			this._commandCollection[4].Connection = this.Connection;
			this._commandCollection[4].CommandText = "SELECT        MAX(Nr) AS NewID\r\nFROM            Umetnine";
			this._commandCollection[4].CommandType = CommandType.Text;
			this._commandCollection[5] = new OleDbCommand();
			this._commandCollection[5].Connection = this.Connection;
			this._commandCollection[5].CommandText = "INSERT INTO `Umetnine` (`st_um_dela`, `naslov`, `leto_izdelave`, `material`, `velikost`, `zaznamek`, `st_negativ`, `cena`, `cena_dem`, `st_slik`, `opis`, `ID`, `datum`, `izkupicek`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			this._commandCollection[5].CommandType = CommandType.Text;
			this._commandCollection[5].Parameters.Add(new OleDbParameter("st_um_dela", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "st_um_dela", DataRowVersion.Current, false, null));
			this._commandCollection[5].Parameters.Add(new OleDbParameter("naslov", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "naslov", DataRowVersion.Current, false, null));
			this._commandCollection[5].Parameters.Add(new OleDbParameter("leto_izdelave", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "leto_izdelave", DataRowVersion.Current, false, null));
			this._commandCollection[5].Parameters.Add(new OleDbParameter("material", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "material", DataRowVersion.Current, false, null));
			this._commandCollection[5].Parameters.Add(new OleDbParameter("velikost", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "velikost", DataRowVersion.Current, false, null));
			this._commandCollection[5].Parameters.Add(new OleDbParameter("zaznamek", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "zaznamek", DataRowVersion.Current, false, null));
			this._commandCollection[5].Parameters.Add(new OleDbParameter("st_negativ", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "st_negativ", DataRowVersion.Current, false, null));
			this._commandCollection[5].Parameters.Add(new OleDbParameter("cena", OleDbType.Currency, 0, ParameterDirection.Input, 19, 0, "cena", DataRowVersion.Current, false, null));
			this._commandCollection[5].Parameters.Add(new OleDbParameter("cena_dem", OleDbType.Currency, 0, ParameterDirection.Input, 19, 0, "cena_dem", DataRowVersion.Current, false, null));
			this._commandCollection[5].Parameters.Add(new OleDbParameter("st_slik", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "st_slik", DataRowVersion.Current, false, null));
			this._commandCollection[5].Parameters.Add(new OleDbParameter("opis", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Current, false, null));
			this._commandCollection[5].Parameters.Add(new OleDbParameter("ID", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "ID", DataRowVersion.Current, false, null));
			this._commandCollection[5].Parameters.Add(new OleDbParameter("datum", OleDbType.Date, 0, ParameterDirection.Input, 0, 0, "datum", DataRowVersion.Current, false, null));
			this._commandCollection[5].Parameters.Add(new OleDbParameter("izkupicek", OleDbType.Currency, 0, ParameterDirection.Input, 19, 0, "izkupicek", DataRowVersion.Current, false, null));
			this._commandCollection[6] = new OleDbCommand();
			this._commandCollection[6].Connection = this.Connection;
			this._commandCollection[6].CommandText = "UPDATE       Umetnine\r\nSET                st_um_dela = ?, naslov = ?, leto_izdelave = ?, material = ?, velikost = ?, zaznamek = ?, st_negativ = ?, cena = ?, cena_dem = ?, st_slik = ?, opis = ?, ID = ?, datum = ?, izkupicek = ?\r\nWHERE        (Nr = ?)";
			this._commandCollection[6].CommandType = CommandType.Text;
			this._commandCollection[6].Parameters.Add(new OleDbParameter("st_um_dela", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "st_um_dela", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("naslov", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "naslov", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("leto_izdelave", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "leto_izdelave", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("material", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "material", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("velikost", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "velikost", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("zaznamek", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "zaznamek", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("st_negativ", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "st_negativ", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("cena", OleDbType.Currency, 0, ParameterDirection.Input, 19, 0, "cena", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("cena_dem", OleDbType.Currency, 0, ParameterDirection.Input, 19, 0, "cena_dem", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("st_slik", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "st_slik", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("opis", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "opis", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("ID", OleDbType.WChar, 255, ParameterDirection.Input, 0, 0, "ID", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("datum", OleDbType.Date, 0, ParameterDirection.Input, 0, 0, "datum", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("izkupicek", OleDbType.Currency, 0, ParameterDirection.Input, 19, 0, "izkupicek", DataRowVersion.Current, false, null));
			this._commandCollection[6].Parameters.Add(new OleDbParameter("Original_Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Original, false, null));
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Fill, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Fill(KatalogUmetnin_dbDataSet.UmetnineDataTable dataTable)
		{
			this.Adapter.SelectCommand = this.CommandCollection[0];
			if (this.ClearBeforeFill)
			{
				dataTable.Clear();
			}
			return this.Adapter.Fill(dataTable);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Select, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual KatalogUmetnin_dbDataSet.UmetnineDataTable GetData()
		{
			this.Adapter.SelectCommand = this.CommandCollection[0];
			KatalogUmetnin_dbDataSet.UmetnineDataTable dataTable = new KatalogUmetnin_dbDataSet.UmetnineDataTable();
			this.Adapter.Fill(dataTable);
			return dataTable;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Fill, false), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int FillByNr(KatalogUmetnin_dbDataSet.UmetnineDataTable dataTable, int Nr)
		{
			this.Adapter.SelectCommand = this.CommandCollection[2];
			this.Adapter.SelectCommand.Parameters[0].Value = Nr;
			if (this.ClearBeforeFill)
			{
				dataTable.Clear();
			}
			return this.Adapter.Fill(dataTable);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Select, false), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual KatalogUmetnin_dbDataSet.UmetnineDataTable GetDataByNr(int Nr)
		{
			this.Adapter.SelectCommand = this.CommandCollection[2];
			this.Adapter.SelectCommand.Parameters[0].Value = Nr;
			KatalogUmetnin_dbDataSet.UmetnineDataTable dataTable = new KatalogUmetnin_dbDataSet.UmetnineDataTable();
			this.Adapter.Fill(dataTable);
			return dataTable;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Fill, false), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int FillByStUmDela(KatalogUmetnin_dbDataSet.UmetnineDataTable dataTable, string st_um_dela)
		{
			this.Adapter.SelectCommand = this.CommandCollection[3];
			if (st_um_dela == null)
			{
				this.Adapter.SelectCommand.Parameters[0].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.SelectCommand.Parameters[0].Value = st_um_dela;
			}
			if (this.ClearBeforeFill)
			{
				dataTable.Clear();
			}
			return this.Adapter.Fill(dataTable);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Select, false), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual KatalogUmetnin_dbDataSet.UmetnineDataTable GetDataByStUmDela(string st_um_dela)
		{
			this.Adapter.SelectCommand = this.CommandCollection[3];
			if (st_um_dela == null)
			{
				this.Adapter.SelectCommand.Parameters[0].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.SelectCommand.Parameters[0].Value = st_um_dela;
			}
			KatalogUmetnin_dbDataSet.UmetnineDataTable dataTable = new KatalogUmetnin_dbDataSet.UmetnineDataTable();
			this.Adapter.Fill(dataTable);
			return dataTable;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(KatalogUmetnin_dbDataSet.UmetnineDataTable dataTable)
		{
			return this.Adapter.Update(dataTable);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(KatalogUmetnin_dbDataSet dataSet)
		{
			return this.Adapter.Update(dataSet, "Umetnine");
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(DataRow dataRow)
		{
			return this.Adapter.Update(new DataRow[]
			{
				dataRow
			});
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(DataRow[] dataRows)
		{
			return this.Adapter.Update(dataRows);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Delete, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Delete(int Original_Nr, string Original_st_um_dela, string Original_naslov, string Original_leto_izdelave, string Original_material, string Original_velikost, string Original_zaznamek, string Original_st_negativ, decimal? Original_cena, decimal? Original_cena_dem, string Original_st_slik, string Original_opis, string Original_ID, DateTime? Original_datum, decimal? Original_izkupicek)
		{
			this.Adapter.DeleteCommand.Parameters[0].Value = Original_Nr;
			if (Original_st_um_dela == null)
			{
				this.Adapter.DeleteCommand.Parameters[1].Value = 1;
				this.Adapter.DeleteCommand.Parameters[2].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[1].Value = 0;
				this.Adapter.DeleteCommand.Parameters[2].Value = Original_st_um_dela;
			}
			if (Original_naslov == null)
			{
				this.Adapter.DeleteCommand.Parameters[3].Value = 1;
				this.Adapter.DeleteCommand.Parameters[4].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[3].Value = 0;
				this.Adapter.DeleteCommand.Parameters[4].Value = Original_naslov;
			}
			if (Original_leto_izdelave == null)
			{
				this.Adapter.DeleteCommand.Parameters[5].Value = 1;
				this.Adapter.DeleteCommand.Parameters[6].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[5].Value = 0;
				this.Adapter.DeleteCommand.Parameters[6].Value = Original_leto_izdelave;
			}
			if (Original_material == null)
			{
				this.Adapter.DeleteCommand.Parameters[7].Value = 1;
				this.Adapter.DeleteCommand.Parameters[8].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[7].Value = 0;
				this.Adapter.DeleteCommand.Parameters[8].Value = Original_material;
			}
			if (Original_velikost == null)
			{
				this.Adapter.DeleteCommand.Parameters[9].Value = 1;
				this.Adapter.DeleteCommand.Parameters[10].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[9].Value = 0;
				this.Adapter.DeleteCommand.Parameters[10].Value = Original_velikost;
			}
			if (Original_zaznamek == null)
			{
				this.Adapter.DeleteCommand.Parameters[11].Value = 1;
				this.Adapter.DeleteCommand.Parameters[12].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[11].Value = 0;
				this.Adapter.DeleteCommand.Parameters[12].Value = Original_zaznamek;
			}
			if (Original_st_negativ == null)
			{
				this.Adapter.DeleteCommand.Parameters[13].Value = 1;
				this.Adapter.DeleteCommand.Parameters[14].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[13].Value = 0;
				this.Adapter.DeleteCommand.Parameters[14].Value = Original_st_negativ;
			}
			if (Original_cena.HasValue)
			{
				this.Adapter.DeleteCommand.Parameters[15].Value = 0;
				this.Adapter.DeleteCommand.Parameters[16].Value = Original_cena.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[15].Value = 1;
				this.Adapter.DeleteCommand.Parameters[16].Value = DBNull.Value;
			}
			if (Original_cena_dem.HasValue)
			{
				this.Adapter.DeleteCommand.Parameters[17].Value = 0;
				this.Adapter.DeleteCommand.Parameters[18].Value = Original_cena_dem.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[17].Value = 1;
				this.Adapter.DeleteCommand.Parameters[18].Value = DBNull.Value;
			}
			if (Original_st_slik == null)
			{
				this.Adapter.DeleteCommand.Parameters[19].Value = 1;
				this.Adapter.DeleteCommand.Parameters[20].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[19].Value = 0;
				this.Adapter.DeleteCommand.Parameters[20].Value = Original_st_slik;
			}
			if (Original_opis == null)
			{
				this.Adapter.DeleteCommand.Parameters[21].Value = 1;
				this.Adapter.DeleteCommand.Parameters[22].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[21].Value = 0;
				this.Adapter.DeleteCommand.Parameters[22].Value = Original_opis;
			}
			if (Original_ID == null)
			{
				this.Adapter.DeleteCommand.Parameters[23].Value = 1;
				this.Adapter.DeleteCommand.Parameters[24].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[23].Value = 0;
				this.Adapter.DeleteCommand.Parameters[24].Value = Original_ID;
			}
			if (Original_datum.HasValue)
			{
				this.Adapter.DeleteCommand.Parameters[25].Value = 0;
				this.Adapter.DeleteCommand.Parameters[26].Value = Original_datum.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[25].Value = 1;
				this.Adapter.DeleteCommand.Parameters[26].Value = DBNull.Value;
			}
			if (Original_izkupicek.HasValue)
			{
				this.Adapter.DeleteCommand.Parameters[27].Value = 0;
				this.Adapter.DeleteCommand.Parameters[28].Value = Original_izkupicek.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[27].Value = 1;
				this.Adapter.DeleteCommand.Parameters[28].Value = DBNull.Value;
			}
			ConnectionState previousConnectionState = this.Adapter.DeleteCommand.Connection.State;
			if ((this.Adapter.DeleteCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				this.Adapter.DeleteCommand.Connection.Open();
			}
			int result;
			try
			{
				int returnValue = this.Adapter.DeleteCommand.ExecuteNonQuery();
				result = returnValue;
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					this.Adapter.DeleteCommand.Connection.Close();
				}
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Insert, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Insert(string st_um_dela, string naslov, string leto_izdelave, string material, string velikost, string zaznamek, string st_negativ, decimal? cena, decimal? cena_dem, string st_slik, string opis, string ID, DateTime? datum, decimal? izkupicek)
		{
			if (st_um_dela == null)
			{
				this.Adapter.InsertCommand.Parameters[0].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[0].Value = st_um_dela;
			}
			if (naslov == null)
			{
				this.Adapter.InsertCommand.Parameters[1].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[1].Value = naslov;
			}
			if (leto_izdelave == null)
			{
				this.Adapter.InsertCommand.Parameters[2].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[2].Value = leto_izdelave;
			}
			if (material == null)
			{
				this.Adapter.InsertCommand.Parameters[3].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[3].Value = material;
			}
			if (velikost == null)
			{
				this.Adapter.InsertCommand.Parameters[4].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[4].Value = velikost;
			}
			if (zaznamek == null)
			{
				this.Adapter.InsertCommand.Parameters[5].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[5].Value = zaznamek;
			}
			if (st_negativ == null)
			{
				this.Adapter.InsertCommand.Parameters[6].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[6].Value = st_negativ;
			}
			if (cena.HasValue)
			{
				this.Adapter.InsertCommand.Parameters[7].Value = cena.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[7].Value = DBNull.Value;
			}
			if (cena_dem.HasValue)
			{
				this.Adapter.InsertCommand.Parameters[8].Value = cena_dem.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[8].Value = DBNull.Value;
			}
			if (st_slik == null)
			{
				this.Adapter.InsertCommand.Parameters[9].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[9].Value = st_slik;
			}
			if (opis == null)
			{
				this.Adapter.InsertCommand.Parameters[10].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[10].Value = opis;
			}
			if (ID == null)
			{
				this.Adapter.InsertCommand.Parameters[11].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[11].Value = ID;
			}
			if (datum.HasValue)
			{
				this.Adapter.InsertCommand.Parameters[12].Value = datum.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[12].Value = DBNull.Value;
			}
			if (izkupicek.HasValue)
			{
				this.Adapter.InsertCommand.Parameters[13].Value = izkupicek.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[13].Value = DBNull.Value;
			}
			ConnectionState previousConnectionState = this.Adapter.InsertCommand.Connection.State;
			if ((this.Adapter.InsertCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				this.Adapter.InsertCommand.Connection.Open();
			}
			int result;
			try
			{
				int returnValue = this.Adapter.InsertCommand.ExecuteNonQuery();
				result = returnValue;
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					this.Adapter.InsertCommand.Connection.Close();
				}
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Update, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(string st_um_dela, string naslov, string leto_izdelave, string material, string velikost, string zaznamek, string st_negativ, decimal? cena, decimal? cena_dem, string st_slik, string opis, string ID, DateTime? datum, decimal? izkupicek, int Original_Nr, string Original_st_um_dela, string Original_naslov, string Original_leto_izdelave, string Original_material, string Original_velikost, string Original_zaznamek, string Original_st_negativ, decimal? Original_cena, decimal? Original_cena_dem, string Original_st_slik, string Original_opis, string Original_ID, DateTime? Original_datum, decimal? Original_izkupicek)
		{
			if (st_um_dela == null)
			{
				this.Adapter.UpdateCommand.Parameters[0].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[0].Value = st_um_dela;
			}
			if (naslov == null)
			{
				this.Adapter.UpdateCommand.Parameters[1].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[1].Value = naslov;
			}
			if (leto_izdelave == null)
			{
				this.Adapter.UpdateCommand.Parameters[2].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[2].Value = leto_izdelave;
			}
			if (material == null)
			{
				this.Adapter.UpdateCommand.Parameters[3].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[3].Value = material;
			}
			if (velikost == null)
			{
				this.Adapter.UpdateCommand.Parameters[4].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[4].Value = velikost;
			}
			if (zaznamek == null)
			{
				this.Adapter.UpdateCommand.Parameters[5].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[5].Value = zaznamek;
			}
			if (st_negativ == null)
			{
				this.Adapter.UpdateCommand.Parameters[6].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[6].Value = st_negativ;
			}
			if (cena.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[7].Value = cena.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[7].Value = DBNull.Value;
			}
			if (cena_dem.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[8].Value = cena_dem.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[8].Value = DBNull.Value;
			}
			if (st_slik == null)
			{
				this.Adapter.UpdateCommand.Parameters[9].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[9].Value = st_slik;
			}
			if (opis == null)
			{
				this.Adapter.UpdateCommand.Parameters[10].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[10].Value = opis;
			}
			if (ID == null)
			{
				this.Adapter.UpdateCommand.Parameters[11].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[11].Value = ID;
			}
			if (datum.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[12].Value = datum.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[12].Value = DBNull.Value;
			}
			if (izkupicek.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[13].Value = izkupicek.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[13].Value = DBNull.Value;
			}
			this.Adapter.UpdateCommand.Parameters[14].Value = Original_Nr;
			if (Original_st_um_dela == null)
			{
				this.Adapter.UpdateCommand.Parameters[15].Value = 1;
				this.Adapter.UpdateCommand.Parameters[16].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[15].Value = 0;
				this.Adapter.UpdateCommand.Parameters[16].Value = Original_st_um_dela;
			}
			if (Original_naslov == null)
			{
				this.Adapter.UpdateCommand.Parameters[17].Value = 1;
				this.Adapter.UpdateCommand.Parameters[18].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[17].Value = 0;
				this.Adapter.UpdateCommand.Parameters[18].Value = Original_naslov;
			}
			if (Original_leto_izdelave == null)
			{
				this.Adapter.UpdateCommand.Parameters[19].Value = 1;
				this.Adapter.UpdateCommand.Parameters[20].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[19].Value = 0;
				this.Adapter.UpdateCommand.Parameters[20].Value = Original_leto_izdelave;
			}
			if (Original_material == null)
			{
				this.Adapter.UpdateCommand.Parameters[21].Value = 1;
				this.Adapter.UpdateCommand.Parameters[22].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[21].Value = 0;
				this.Adapter.UpdateCommand.Parameters[22].Value = Original_material;
			}
			if (Original_velikost == null)
			{
				this.Adapter.UpdateCommand.Parameters[23].Value = 1;
				this.Adapter.UpdateCommand.Parameters[24].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[23].Value = 0;
				this.Adapter.UpdateCommand.Parameters[24].Value = Original_velikost;
			}
			if (Original_zaznamek == null)
			{
				this.Adapter.UpdateCommand.Parameters[25].Value = 1;
				this.Adapter.UpdateCommand.Parameters[26].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[25].Value = 0;
				this.Adapter.UpdateCommand.Parameters[26].Value = Original_zaznamek;
			}
			if (Original_st_negativ == null)
			{
				this.Adapter.UpdateCommand.Parameters[27].Value = 1;
				this.Adapter.UpdateCommand.Parameters[28].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[27].Value = 0;
				this.Adapter.UpdateCommand.Parameters[28].Value = Original_st_negativ;
			}
			if (Original_cena.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[29].Value = 0;
				this.Adapter.UpdateCommand.Parameters[30].Value = Original_cena.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[29].Value = 1;
				this.Adapter.UpdateCommand.Parameters[30].Value = DBNull.Value;
			}
			if (Original_cena_dem.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[31].Value = 0;
				this.Adapter.UpdateCommand.Parameters[32].Value = Original_cena_dem.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[31].Value = 1;
				this.Adapter.UpdateCommand.Parameters[32].Value = DBNull.Value;
			}
			if (Original_st_slik == null)
			{
				this.Adapter.UpdateCommand.Parameters[33].Value = 1;
				this.Adapter.UpdateCommand.Parameters[34].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[33].Value = 0;
				this.Adapter.UpdateCommand.Parameters[34].Value = Original_st_slik;
			}
			if (Original_opis == null)
			{
				this.Adapter.UpdateCommand.Parameters[35].Value = 1;
				this.Adapter.UpdateCommand.Parameters[36].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[35].Value = 0;
				this.Adapter.UpdateCommand.Parameters[36].Value = Original_opis;
			}
			if (Original_ID == null)
			{
				this.Adapter.UpdateCommand.Parameters[37].Value = 1;
				this.Adapter.UpdateCommand.Parameters[38].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[37].Value = 0;
				this.Adapter.UpdateCommand.Parameters[38].Value = Original_ID;
			}
			if (Original_datum.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[39].Value = 0;
				this.Adapter.UpdateCommand.Parameters[40].Value = Original_datum.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[39].Value = 1;
				this.Adapter.UpdateCommand.Parameters[40].Value = DBNull.Value;
			}
			if (Original_izkupicek.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[41].Value = 0;
				this.Adapter.UpdateCommand.Parameters[42].Value = Original_izkupicek.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[41].Value = 1;
				this.Adapter.UpdateCommand.Parameters[42].Value = DBNull.Value;
			}
			ConnectionState previousConnectionState = this.Adapter.UpdateCommand.Connection.State;
			if ((this.Adapter.UpdateCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				this.Adapter.UpdateCommand.Connection.Open();
			}
			int result;
			try
			{
				int returnValue = this.Adapter.UpdateCommand.ExecuteNonQuery();
				result = returnValue;
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					this.Adapter.UpdateCommand.Connection.Close();
				}
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Delete, false), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int DeleteUmetnina(int Nr)
		{
			OleDbCommand command = this.CommandCollection[1];
			command.Parameters[0].Value = Nr;
			ConnectionState previousConnectionState = command.Connection.State;
			if ((command.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				command.Connection.Open();
			}
			int returnValue;
			try
			{
				returnValue = command.ExecuteNonQuery();
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					command.Connection.Close();
				}
			}
			return returnValue;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int? GetNewID()
		{
			OleDbCommand command = this.CommandCollection[4];
			ConnectionState previousConnectionState = command.Connection.State;
			if ((command.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				command.Connection.Open();
			}
			object returnValue;
			try
			{
				returnValue = command.ExecuteScalar();
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					command.Connection.Close();
				}
			}
			int? result;
			if (returnValue == null || returnValue.GetType() == typeof(DBNull))
			{
				result = null;
			}
			else
			{
				result = new int?((int)returnValue);
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Insert, false), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int InsertUmetnina(string st_um_dela, string naslov, string leto_izdelave, string material, string velikost, string zaznamek, string st_negativ, decimal? cena, decimal? cena_dem, string st_slik, string opis, string ID, DateTime? datum, decimal? izkupicek)
		{
			OleDbCommand command = this.CommandCollection[5];
			if (st_um_dela == null)
			{
				command.Parameters[0].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[0].Value = st_um_dela;
			}
			if (naslov == null)
			{
				command.Parameters[1].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[1].Value = naslov;
			}
			if (leto_izdelave == null)
			{
				command.Parameters[2].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[2].Value = leto_izdelave;
			}
			if (material == null)
			{
				command.Parameters[3].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[3].Value = material;
			}
			if (velikost == null)
			{
				command.Parameters[4].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[4].Value = velikost;
			}
			if (zaznamek == null)
			{
				command.Parameters[5].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[5].Value = zaznamek;
			}
			if (st_negativ == null)
			{
				command.Parameters[6].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[6].Value = st_negativ;
			}
			if (cena.HasValue)
			{
				command.Parameters[7].Value = cena.Value;
			}
			else
			{
				command.Parameters[7].Value = DBNull.Value;
			}
			if (cena_dem.HasValue)
			{
				command.Parameters[8].Value = cena_dem.Value;
			}
			else
			{
				command.Parameters[8].Value = DBNull.Value;
			}
			if (st_slik == null)
			{
				command.Parameters[9].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[9].Value = st_slik;
			}
			if (opis == null)
			{
				command.Parameters[10].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[10].Value = opis;
			}
			if (ID == null)
			{
				command.Parameters[11].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[11].Value = ID;
			}
			if (datum.HasValue)
			{
				command.Parameters[12].Value = datum.Value;
			}
			else
			{
				command.Parameters[12].Value = DBNull.Value;
			}
			if (izkupicek.HasValue)
			{
				command.Parameters[13].Value = izkupicek.Value;
			}
			else
			{
				command.Parameters[13].Value = DBNull.Value;
			}
			ConnectionState previousConnectionState = command.Connection.State;
			if ((command.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				command.Connection.Open();
			}
			int returnValue;
			try
			{
				returnValue = command.ExecuteNonQuery();
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					command.Connection.Close();
				}
			}
			return returnValue;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Update, false), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int UpdateUmetnina(string st_um_dela, string naslov, string leto_izdelave, string material, string velikost, string zaznamek, string st_negativ, decimal? cena, decimal? cena_dem, string st_slik, string opis, string ID, DateTime? datum, decimal? izkupicek, int Original_Nr)
		{
			OleDbCommand command = this.CommandCollection[6];
			if (st_um_dela == null)
			{
				command.Parameters[0].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[0].Value = st_um_dela;
			}
			if (naslov == null)
			{
				command.Parameters[1].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[1].Value = naslov;
			}
			if (leto_izdelave == null)
			{
				command.Parameters[2].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[2].Value = leto_izdelave;
			}
			if (material == null)
			{
				command.Parameters[3].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[3].Value = material;
			}
			if (velikost == null)
			{
				command.Parameters[4].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[4].Value = velikost;
			}
			if (zaznamek == null)
			{
				command.Parameters[5].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[5].Value = zaznamek;
			}
			if (st_negativ == null)
			{
				command.Parameters[6].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[6].Value = st_negativ;
			}
			if (cena.HasValue)
			{
				command.Parameters[7].Value = cena.Value;
			}
			else
			{
				command.Parameters[7].Value = DBNull.Value;
			}
			if (cena_dem.HasValue)
			{
				command.Parameters[8].Value = cena_dem.Value;
			}
			else
			{
				command.Parameters[8].Value = DBNull.Value;
			}
			if (st_slik == null)
			{
				command.Parameters[9].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[9].Value = st_slik;
			}
			if (opis == null)
			{
				command.Parameters[10].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[10].Value = opis;
			}
			if (ID == null)
			{
				command.Parameters[11].Value = DBNull.Value;
			}
			else
			{
				command.Parameters[11].Value = ID;
			}
			if (datum.HasValue)
			{
				command.Parameters[12].Value = datum.Value;
			}
			else
			{
				command.Parameters[12].Value = DBNull.Value;
			}
			if (izkupicek.HasValue)
			{
				command.Parameters[13].Value = izkupicek.Value;
			}
			else
			{
				command.Parameters[13].Value = DBNull.Value;
			}
			command.Parameters[14].Value = Original_Nr;
			ConnectionState previousConnectionState = command.Connection.State;
			if ((command.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				command.Connection.Open();
			}
			int returnValue;
			try
			{
				returnValue = command.ExecuteNonQuery();
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					command.Connection.Close();
				}
			}
			return returnValue;
		}
	}
}
