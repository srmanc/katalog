using KatalogUmetnin.Properties;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Diagnostics;

namespace KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters
{
	[DataObject(true), HelpKeyword("vs.data.TableAdapter"), Designer("Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), DesignerCategory("code"), ToolboxItem(true)]
	public class NapisiTableAdapter : Component
	{
		private OleDbDataAdapter _adapter;

		private OleDbConnection _connection;

		private OleDbTransaction _transaction;

		private OleDbCommand[] _commandCollection;

		private bool _clearBeforeFill;

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected internal OleDbDataAdapter Adapter
		{
			get
			{
				if (this._adapter == null)
				{
					this.InitAdapter();
				}
				return this._adapter;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		internal OleDbConnection Connection
		{
			get
			{
				if (this._connection == null)
				{
					this.InitConnection();
				}
				return this._connection;
			}
			set
			{
				this._connection = value;
				if (this.Adapter.InsertCommand != null)
				{
					this.Adapter.InsertCommand.Connection = value;
				}
				if (this.Adapter.DeleteCommand != null)
				{
					this.Adapter.DeleteCommand.Connection = value;
				}
				if (this.Adapter.UpdateCommand != null)
				{
					this.Adapter.UpdateCommand.Connection = value;
				}
				for (int i = 0; i < this.CommandCollection.Length; i++)
				{
					if (this.CommandCollection[i] != null)
					{
						this.CommandCollection[i].Connection = value;
					}
				}
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		internal OleDbTransaction Transaction
		{
			get
			{
				return this._transaction;
			}
			set
			{
				this._transaction = value;
				for (int i = 0; i < this.CommandCollection.Length; i++)
				{
					this.CommandCollection[i].Transaction = this._transaction;
				}
				if (this.Adapter != null && this.Adapter.DeleteCommand != null)
				{
					this.Adapter.DeleteCommand.Transaction = this._transaction;
				}
				if (this.Adapter != null && this.Adapter.InsertCommand != null)
				{
					this.Adapter.InsertCommand.Transaction = this._transaction;
				}
				if (this.Adapter != null && this.Adapter.UpdateCommand != null)
				{
					this.Adapter.UpdateCommand.Transaction = this._transaction;
				}
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected OleDbCommand[] CommandCollection
		{
			get
			{
				if (this._commandCollection == null)
				{
					this.InitCommandCollection();
				}
				return this._commandCollection;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public bool ClearBeforeFill
		{
			get
			{
				return this._clearBeforeFill;
			}
			set
			{
				this._clearBeforeFill = value;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public NapisiTableAdapter()
		{
			this.ClearBeforeFill = true;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void InitAdapter()
		{
			this._adapter = new OleDbDataAdapter();
			DataTableMapping tableMapping = new DataTableMapping();
			tableMapping.SourceTable = "Table";
			tableMapping.DataSetTable = "Napisi";
			tableMapping.ColumnMappings.Add("Nr", "Nr");
			tableMapping.ColumnMappings.Add("IDNapisa", "IDNapisa");
			tableMapping.ColumnMappings.Add("Tekst", "Tekst");
			tableMapping.ColumnMappings.Add("Font", "Font");
			tableMapping.ColumnMappings.Add("Color", "Color");
			tableMapping.ColumnMappings.Add("Size", "Size");
			tableMapping.ColumnMappings.Add("X", "X");
			tableMapping.ColumnMappings.Add("Y", "Y");
			this._adapter.TableMappings.Add(tableMapping);
			this._adapter.DeleteCommand = new OleDbCommand();
			this._adapter.DeleteCommand.Connection = this.Connection;
			this._adapter.DeleteCommand.CommandText = "DELETE FROM `Napisi` WHERE ((`Nr` = ?) AND (`IDNapisa` = ?) AND ((? = 1 AND `Font` IS NULL) OR (`Font` = ?)) AND ((? = 1 AND `Color` IS NULL) OR (`Color` = ?)) AND ((? = 1 AND `Size` IS NULL) OR (`Size` = ?)) AND ((? = 1 AND `X` IS NULL) OR (`X` = ?)) AND ((? = 1 AND `Y` IS NULL) OR (`Y` = ?)))";
			this._adapter.DeleteCommand.CommandType = CommandType.Text;
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_IDNapisa", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "IDNapisa", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_Font", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Font", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_Font", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "Font", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_Color", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Color", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_Color", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "Color", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_Size", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Size", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_Size", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Size", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_X", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "X", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_X", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "X", DataRowVersion.Original, false, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("IsNull_Y", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Y", DataRowVersion.Original, true, null));
			this._adapter.DeleteCommand.Parameters.Add(new OleDbParameter("Original_Y", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Y", DataRowVersion.Original, false, null));
			this._adapter.InsertCommand = new OleDbCommand();
			this._adapter.InsertCommand.Connection = this.Connection;
			this._adapter.InsertCommand.CommandText = "INSERT INTO `Napisi` (`Nr`, `IDNapisa`, `Tekst`, `Font`, `Color`, `Size`, `X`, `Y`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
			this._adapter.InsertCommand.CommandType = CommandType.Text;
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("IDNapisa", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "IDNapisa", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("Tekst", OleDbType.LongVarWChar, 0, ParameterDirection.Input, 0, 0, "Tekst", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("Font", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "Font", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("Color", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "Color", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("Size", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Size", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("X", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "X", DataRowVersion.Current, false, null));
			this._adapter.InsertCommand.Parameters.Add(new OleDbParameter("Y", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Y", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand = new OleDbCommand();
			this._adapter.UpdateCommand.Connection = this.Connection;
			this._adapter.UpdateCommand.CommandText = "UPDATE `Napisi` SET `Nr` = ?, `IDNapisa` = ?, `Tekst` = ?, `Font` = ?, `Color` = ?, `Size` = ?, `X` = ?, `Y` = ? WHERE ((`Nr` = ?) AND (`IDNapisa` = ?) AND ((? = 1 AND `Font` IS NULL) OR (`Font` = ?)) AND ((? = 1 AND `Color` IS NULL) OR (`Color` = ?)) AND ((? = 1 AND `Size` IS NULL) OR (`Size` = ?)) AND ((? = 1 AND `X` IS NULL) OR (`X` = ?)) AND ((? = 1 AND `Y` IS NULL) OR (`Y` = ?)))";
			this._adapter.UpdateCommand.CommandType = CommandType.Text;
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IDNapisa", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "IDNapisa", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Tekst", OleDbType.LongVarWChar, 0, ParameterDirection.Input, 0, 0, "Tekst", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Font", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "Font", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Color", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "Color", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Size", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Size", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("X", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "X", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Y", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Y", DataRowVersion.Current, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_IDNapisa", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "IDNapisa", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_Font", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Font", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_Font", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "Font", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_Color", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Color", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_Color", OleDbType.VarWChar, 0, ParameterDirection.Input, 0, 0, "Color", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_Size", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Size", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_Size", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Size", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_X", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "X", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_X", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "X", DataRowVersion.Original, false, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("IsNull_Y", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Y", DataRowVersion.Original, true, null));
			this._adapter.UpdateCommand.Parameters.Add(new OleDbParameter("Original_Y", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Y", DataRowVersion.Original, false, null));
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void InitConnection()
		{
			this._connection = new OleDbConnection();
			this._connection.ConnectionString = Settings.Get.DbConnectionString;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void InitCommandCollection()
		{
			this._commandCollection = new OleDbCommand[2];
			this._commandCollection[0] = new OleDbCommand();
			this._commandCollection[0].Connection = this.Connection;
			this._commandCollection[0].CommandText = "SELECT        Nr, IDNapisa, Tekst, Font, Color, [Size], X, Y\r\nFROM            Napisi";
			this._commandCollection[0].CommandType = CommandType.Text;
			this._commandCollection[1] = new OleDbCommand();
			this._commandCollection[1].Connection = this.Connection;
			this._commandCollection[1].CommandText = "SELECT        Nr, IDNapisa, Tekst, Font, Color, [Size], X, Y\r\nFROM            Napisi\r\nWHERE Nr=? AND IDNapisa=?";
			this._commandCollection[1].CommandType = CommandType.Text;
			this._commandCollection[1].Parameters.Add(new OleDbParameter("Nr", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "Nr", DataRowVersion.Current, false, null));
			this._commandCollection[1].Parameters.Add(new OleDbParameter("IDNapisa", OleDbType.Integer, 0, ParameterDirection.Input, 0, 0, "IDNapisa", DataRowVersion.Current, false, null));
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Fill, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Fill(KatalogUmetnin_dbDataSet.NapisiDataTable dataTable)
		{
			this.Adapter.SelectCommand = this.CommandCollection[0];
			if (this.ClearBeforeFill)
			{
				dataTable.Clear();
			}
			return this.Adapter.Fill(dataTable);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Select, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual KatalogUmetnin_dbDataSet.NapisiDataTable GetData()
		{
			this.Adapter.SelectCommand = this.CommandCollection[0];
			KatalogUmetnin_dbDataSet.NapisiDataTable dataTable = new KatalogUmetnin_dbDataSet.NapisiDataTable();
			this.Adapter.Fill(dataTable);
			return dataTable;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Fill, false), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int FillByID(KatalogUmetnin_dbDataSet.NapisiDataTable dataTable, int Nr, int IDNapisa)
		{
			this.Adapter.SelectCommand = this.CommandCollection[1];
			this.Adapter.SelectCommand.Parameters[0].Value = Nr;
			this.Adapter.SelectCommand.Parameters[1].Value = IDNapisa;
			if (this.ClearBeforeFill)
			{
				dataTable.Clear();
			}
			return this.Adapter.Fill(dataTable);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Select, false), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual KatalogUmetnin_dbDataSet.NapisiDataTable GetDataByID(int Nr, int IDNapisa)
		{
			this.Adapter.SelectCommand = this.CommandCollection[1];
			this.Adapter.SelectCommand.Parameters[0].Value = Nr;
			this.Adapter.SelectCommand.Parameters[1].Value = IDNapisa;
			KatalogUmetnin_dbDataSet.NapisiDataTable dataTable = new KatalogUmetnin_dbDataSet.NapisiDataTable();
			this.Adapter.Fill(dataTable);
			return dataTable;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(KatalogUmetnin_dbDataSet.NapisiDataTable dataTable)
		{
			return this.Adapter.Update(dataTable);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(KatalogUmetnin_dbDataSet dataSet)
		{
			return this.Adapter.Update(dataSet, "Napisi");
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(DataRow dataRow)
		{
			return this.Adapter.Update(new DataRow[]
			{
				dataRow
			});
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(DataRow[] dataRows)
		{
			return this.Adapter.Update(dataRows);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Delete, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Delete(int Original_Nr, int Original_IDNapisa, string Original_Font, string Original_Color, int? Original_Size, int? Original_X, int? Original_Y)
		{
			this.Adapter.DeleteCommand.Parameters[0].Value = Original_Nr;
			this.Adapter.DeleteCommand.Parameters[1].Value = Original_IDNapisa;
			if (Original_Font == null)
			{
				this.Adapter.DeleteCommand.Parameters[2].Value = 1;
				this.Adapter.DeleteCommand.Parameters[3].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[2].Value = 0;
				this.Adapter.DeleteCommand.Parameters[3].Value = Original_Font;
			}
			if (Original_Color == null)
			{
				this.Adapter.DeleteCommand.Parameters[4].Value = 1;
				this.Adapter.DeleteCommand.Parameters[5].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[4].Value = 0;
				this.Adapter.DeleteCommand.Parameters[5].Value = Original_Color;
			}
			if (Original_Size.HasValue)
			{
				this.Adapter.DeleteCommand.Parameters[6].Value = 0;
				this.Adapter.DeleteCommand.Parameters[7].Value = Original_Size.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[6].Value = 1;
				this.Adapter.DeleteCommand.Parameters[7].Value = DBNull.Value;
			}
			if (Original_X.HasValue)
			{
				this.Adapter.DeleteCommand.Parameters[8].Value = 0;
				this.Adapter.DeleteCommand.Parameters[9].Value = Original_X.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[8].Value = 1;
				this.Adapter.DeleteCommand.Parameters[9].Value = DBNull.Value;
			}
			if (Original_Y.HasValue)
			{
				this.Adapter.DeleteCommand.Parameters[10].Value = 0;
				this.Adapter.DeleteCommand.Parameters[11].Value = Original_Y.Value;
			}
			else
			{
				this.Adapter.DeleteCommand.Parameters[10].Value = 1;
				this.Adapter.DeleteCommand.Parameters[11].Value = DBNull.Value;
			}
			ConnectionState previousConnectionState = this.Adapter.DeleteCommand.Connection.State;
			if ((this.Adapter.DeleteCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				this.Adapter.DeleteCommand.Connection.Open();
			}
			int result;
			try
			{
				int returnValue = this.Adapter.DeleteCommand.ExecuteNonQuery();
				result = returnValue;
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					this.Adapter.DeleteCommand.Connection.Close();
				}
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Insert, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Insert(int Nr, int IDNapisa, string Tekst, string Font, string Color, int? Size, int? X, int? Y)
		{
			this.Adapter.InsertCommand.Parameters[0].Value = Nr;
			this.Adapter.InsertCommand.Parameters[1].Value = IDNapisa;
			if (Tekst == null)
			{
				this.Adapter.InsertCommand.Parameters[2].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[2].Value = Tekst;
			}
			if (Font == null)
			{
				this.Adapter.InsertCommand.Parameters[3].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[3].Value = Font;
			}
			if (Color == null)
			{
				this.Adapter.InsertCommand.Parameters[4].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[4].Value = Color;
			}
			if (Size.HasValue)
			{
				this.Adapter.InsertCommand.Parameters[5].Value = Size.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[5].Value = DBNull.Value;
			}
			if (X.HasValue)
			{
				this.Adapter.InsertCommand.Parameters[6].Value = X.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[6].Value = DBNull.Value;
			}
			if (Y.HasValue)
			{
				this.Adapter.InsertCommand.Parameters[7].Value = Y.Value;
			}
			else
			{
				this.Adapter.InsertCommand.Parameters[7].Value = DBNull.Value;
			}
			ConnectionState previousConnectionState = this.Adapter.InsertCommand.Connection.State;
			if ((this.Adapter.InsertCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				this.Adapter.InsertCommand.Connection.Open();
			}
			int result;
			try
			{
				int returnValue = this.Adapter.InsertCommand.ExecuteNonQuery();
				result = returnValue;
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					this.Adapter.InsertCommand.Connection.Close();
				}
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Update, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(int Nr, int IDNapisa, string Tekst, string Font, string Color, int? Size, int? X, int? Y, int Original_Nr, int Original_IDNapisa, string Original_Font, string Original_Color, int? Original_Size, int? Original_X, int? Original_Y)
		{
			this.Adapter.UpdateCommand.Parameters[0].Value = Nr;
			this.Adapter.UpdateCommand.Parameters[1].Value = IDNapisa;
			if (Tekst == null)
			{
				this.Adapter.UpdateCommand.Parameters[2].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[2].Value = Tekst;
			}
			if (Font == null)
			{
				this.Adapter.UpdateCommand.Parameters[3].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[3].Value = Font;
			}
			if (Color == null)
			{
				this.Adapter.UpdateCommand.Parameters[4].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[4].Value = Color;
			}
			if (Size.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[5].Value = Size.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[5].Value = DBNull.Value;
			}
			if (X.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[6].Value = X.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[6].Value = DBNull.Value;
			}
			if (Y.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[7].Value = Y.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[7].Value = DBNull.Value;
			}
			this.Adapter.UpdateCommand.Parameters[8].Value = Original_Nr;
			this.Adapter.UpdateCommand.Parameters[9].Value = Original_IDNapisa;
			if (Original_Font == null)
			{
				this.Adapter.UpdateCommand.Parameters[10].Value = 1;
				this.Adapter.UpdateCommand.Parameters[11].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[10].Value = 0;
				this.Adapter.UpdateCommand.Parameters[11].Value = Original_Font;
			}
			if (Original_Color == null)
			{
				this.Adapter.UpdateCommand.Parameters[12].Value = 1;
				this.Adapter.UpdateCommand.Parameters[13].Value = DBNull.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[12].Value = 0;
				this.Adapter.UpdateCommand.Parameters[13].Value = Original_Color;
			}
			if (Original_Size.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[14].Value = 0;
				this.Adapter.UpdateCommand.Parameters[15].Value = Original_Size.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[14].Value = 1;
				this.Adapter.UpdateCommand.Parameters[15].Value = DBNull.Value;
			}
			if (Original_X.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[16].Value = 0;
				this.Adapter.UpdateCommand.Parameters[17].Value = Original_X.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[16].Value = 1;
				this.Adapter.UpdateCommand.Parameters[17].Value = DBNull.Value;
			}
			if (Original_Y.HasValue)
			{
				this.Adapter.UpdateCommand.Parameters[18].Value = 0;
				this.Adapter.UpdateCommand.Parameters[19].Value = Original_Y.Value;
			}
			else
			{
				this.Adapter.UpdateCommand.Parameters[18].Value = 1;
				this.Adapter.UpdateCommand.Parameters[19].Value = DBNull.Value;
			}
			ConnectionState previousConnectionState = this.Adapter.UpdateCommand.Connection.State;
			if ((this.Adapter.UpdateCommand.Connection.State & ConnectionState.Open) != ConnectionState.Open)
			{
				this.Adapter.UpdateCommand.Connection.Open();
			}
			int result;
			try
			{
				int returnValue = this.Adapter.UpdateCommand.ExecuteNonQuery();
				result = returnValue;
			}
			finally
			{
				if (previousConnectionState == ConnectionState.Closed)
				{
					this.Adapter.UpdateCommand.Connection.Close();
				}
			}
			return result;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DataObjectMethod(DataObjectMethodType.Update, true), HelpKeyword("vs.data.TableAdapter"), DebuggerNonUserCode]
		public virtual int Update(string Tekst, string Font, string Color, int? Size, int? X, int? Y, int Original_Nr, int Original_IDNapisa, string Original_Font, string Original_Color, int? Original_Size, int? Original_X, int? Original_Y)
		{
			return this.Update(Original_Nr, Original_IDNapisa, Tekst, Font, Color, Size, X, Y, Original_Nr, Original_IDNapisa, Original_Font, Original_Color, Original_Size, Original_X, Original_Y);
		}
	}
}
