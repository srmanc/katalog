using KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters;
using Microsoft.Reporting.WinForms;
using System;
using System.ComponentModel;
using System.Data;
using System.Threading;
using System.Windows.Forms;

namespace KatalogUmetnin
{
	public class PrintPreviewList : System.Windows.Forms.Form
	{
		private string Filter = "";

        private bool Price;

		private string Sort = "";    

		private IContainer components = null;

		private ReportViewer reportViewer1;

		private System.Windows.Forms.BindingSource UmetnineBindingSource;

		private KatalogUmetnin_dbDataSet KatalogUmetnin_dbDataSet;

		private UmetnineTableAdapter UmetnineTableAdapter;

		public PrintPreviewList(string filter, bool price, string sort = "")
		{
			this.Filter = filter;
            this.Price = price;
			this.Sort = sort;
			this.InitializeComponent();
		}

		private void PrintPreview_Load(object sender, EventArgs e)
		{
            if (this.Price)
			    this.reportViewer1.LocalReport.ReportEmbeddedResource = "KatalogUmetnin." + Settings.GetLanguageExtType("ReportList") + ".rdlc";
            else
                this.reportViewer1.LocalReport.ReportEmbeddedResource = "KatalogUmetnin." + Settings.GetLanguageExtType("ReportListWithoutPrice") + ".rdlc";
            ReportDataSource rds = reportViewer1.LocalReport.DataSources[0];
            DataView u = Query.UmetnineJoined();
            string rowTitle = Settings.GetLanguageExtType("naslov");
            string rowType = Settings.GetLanguageExtType("ImeTipa");
            foreach (DataRow row in u.Table.Rows) {
                string valueTitle = row[rowTitle] as string;
                string valueType = row[rowType] as string;
                row[rowTitle] = valueType + " " + valueTitle;
            }
            rds.Value = u;
            //this.UmetnineTableAdapter.Fill(this.KatalogUmetnin_dbDataSet.Umetnine);
            this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
			this.UmetnineBindingSource.Filter = this.Filter;
			if (!string.IsNullOrWhiteSpace(this.Sort))
			{
				this.UmetnineBindingSource.Sort = this.Sort;
			}
            this.reportViewer1.RefreshReport();
		}

		private void reportViewer1_ReportRefresh(object sender, CancelEventArgs e)
		{
			this.reportViewer1.PrintDialog();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintPreviewList));
            this.UmetnineBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.KatalogUmetnin_dbDataSet = new KatalogUmetnin.KatalogUmetnin_dbDataSet();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.UmetnineTableAdapter = new KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters.UmetnineTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.UmetnineBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KatalogUmetnin_dbDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // UmetnineBindingSource
            // 
            this.UmetnineBindingSource.DataMember = "Umetnine";
            this.UmetnineBindingSource.DataSource = this.KatalogUmetnin_dbDataSet;
            // 
            // KatalogUmetnin_dbDataSet
            // 
            this.KatalogUmetnin_dbDataSet.DataSetName = "KatalogUmetnin_dbDataSet";
            this.KatalogUmetnin_dbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DS1";
            reportDataSource1.Value = this.UmetnineBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "KatalogUmetnin.ReportList.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(284, 261);
            this.reportViewer1.TabIndex = 0;
            // 
            // UmetnineTableAdapter
            // 
            this.UmetnineTableAdapter.ClearBeforeFill = true;
            // 
            // PrintPreviewList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.reportViewer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PrintPreviewList";
            this.Text = "Predogled tiskanja: Spisek umetniških del";
            this.Load += new System.EventHandler(this.PrintPreview_Load);
            ((System.ComponentModel.ISupportInitialize)(this.UmetnineBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KatalogUmetnin_dbDataSet)).EndInit();
            this.ResumeLayout(false);

		}
	}
}
