using KatalogUmetnin.Properties;
using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Linq;

namespace KatalogUmetnin
{
    [Serializable][DataContract]
	public class Settings 
    {
        private static Settings instance;

        public static string LogFile = "Error.log";
        public static string SettingsFile = "settings.json";
        public static string DatabaseFile = "KatalogUmetnin_db.accdb";        
        public static string DatabaseFileV2 = "KatalogUmetnin_db_v2.accdb";

        [DataMember]
        public string storagePath;
        [DataMember]
        private string language;
        [DataMember]
        private bool showPrice;
        [DataMember]
        private bool debugMode;

        private string dbConnectionString;
        private string wvo2003ConnectionString;

        private Settings() {
            Defaults();
        }

        private void Defaults() {
            if (language == null)   
                language = Languages[0];
            if (storagePath == null)    
                storagePath = "";
            if (dbConnectionString == null)
                dbConnectionString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\{0};Persist Security Info=True;Jet OLEDB:Database Password='" + DefaultPassword + "';", DatabaseFileV2);                        
        }

        public static Settings Get {
            get {
                if (instance == null)
                    instance = new Settings();

                return instance;
            }
        }

        public static string GetLogPath() {
            return Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), LogFile);
        }

        private static string GetSettingsPath() {
            return Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), SettingsFile);
        }

        public static bool Load() {
            bool bRet = false;
            string s = GetSettingsPath();            

            if (File.Exists(s)) {
                try
                {
                    MemoryStream ms = new MemoryStream();
                    using (FileStream fs = new FileStream(s, FileMode.Open)) {
                        fs.CopyTo(ms);
                        fs.Close();
                        ms.Seek(0, SeekOrigin.Begin);                        
                    }
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(Get.GetType());
                    instance = (Settings)ser.ReadObject(ms);
                    instance.Defaults();
                    bRet = true;
                }
                catch {                    
                    Save();
                }
            }
            else
                Save();

            return bRet;
        }

        public static void Save() {
            string s = GetSettingsPath();
            if (File.Exists(s))
                File.Delete(s);
            
            MemoryStream ms = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(Get.GetType());
            ser.WriteObject(ms, Get);

            using (FileStream fs = new FileStream(s, FileMode.Create)) {
                ms.Seek(0, SeekOrigin.Begin);
                ms.CopyTo(fs);
                fs.Close();
            }
        }
               
        public string StoragePath {
            get { return storagePath; }
            set {
                storagePath = value;
                Save();                
            }
        }  

        public string Language {
            get { return language; }
            set {
                language = value;
                Save();
            }
        }

        public string LanguageShort {
            get {
                int idx =  Languages.ToList<string>().IndexOf(Settings.Get.Language);
                if (idx >= 0 && idx < LanguagesShort.Length)
                    return LanguagesShort[idx];
                return "";
            }
        }

        public bool ShowPrice {
            get { return showPrice; }
            set {
                showPrice = value;
                Save();
            }
        }

        public bool DebugMode {
            get { return debugMode; }
            set {
                debugMode = value;
                Save();
            }
        }

        public string DbConnectionString
        {
            get { return dbConnectionString; }
            set { dbConnectionString = value; }            
        }

        /*public static string Base64Encode(string plainText) {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }*/

        public static string Base64Decode(string base64EncodedData) {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string DefaultPassword {
            get {                
                return Base64Decode("S2F0YWxvZzIwMTY=");
            }
        }

        public static string[] Languages = new string[] {
			"English",
			"Deutsch",
			"Slovenski"
		};

        public static string[] LanguagesShort = new string[] {
            "en", "de", "si"
        };

        public static void SetCulture() {
            CultureInfo ci = new CultureInfo("en");
            string language = Settings.Get.Language;
            if (language != null)
            {
                if (!(language == "Slovenski"))
                {
                    if (!(language == "Deutsch"))
                    {
                        if (language == "English")
                        {
                            ci = new CultureInfo("en");
                        }
                    }
                    else
                    {
                        ci = new CultureInfo("de");
                    }
                }
                else
                {
                    ci = new CultureInfo("sl-SI");
                }
            }
            Thread.CurrentThread.CurrentUICulture = ci;
        }

        public static void SetTexts(Control f)
        {
            ComponentResourceManager man = new ComponentResourceManager(f.GetType());
            foreach (Control c in f.Controls)
                if (c != null)
                {
                    man.ApplyResources(c, c.Name);                    
                    
                    if (c is ToolStrip)
                    {
                        foreach (ToolStripItem i in (c as ToolStrip).Items)
                            if (i != null)
                            {
                                man.ApplyResources(i, i.Name);
                                if (i is ToolStripSplitButton)
                                    foreach (ToolStripItem mi in (i as ToolStripSplitButton).DropDownItems)
                                        man.ApplyResources(mi, mi.Name);
                            }
                    }
                    else if (c is DataGridView)
                    {
                        foreach (DataGridViewTextBoxColumn column in (c as DataGridView).Columns)
                            man.ApplyResources(column, column.Name);
                    }
                    else if (c is Panel)
                    {
                        foreach (Control cc in c.Controls)
                            if (cc is Label || cc is RadioButton || cc is Button)
                                man.ApplyResources(cc, cc.Name);                            
                    }                        
                }
            man.ApplyResources(f, f.Name);
        }

        public static void FreeGarbage(bool Wait = true)
        {
            GC.Collect();
            if (Wait)
                GC.WaitForPendingFinalizers();
        }

        public static string GetLanguageExtType(string Type)
        {
            int idx = Type.IndexOf('_');

            if (idx >= 0)
            {
                Type = Type.Substring(0, idx);
            }
            Type += "_" + Settings.Get.LanguageShort;

            return Type;
        }

        public static string FormatDimensions(int x, int y, int z) {
            string s = "", sep = " x ";
            if (x != 0)
                s = x.ToString();
            if (y != 0)
                if (!string.IsNullOrEmpty(s))
                    s += sep + y;
                else
                    s += y;
            if (z != 0)
                if (!string.IsNullOrEmpty(s))
                    s += sep + z;
                else
                    s += z;
            s += " cm";

            return s;
        }        
    }

    public class CultureLock
    {
        private CultureInfo oldCulture;

        public CultureLock() {
            CultureInfo dotCulture = Thread.CurrentThread.CurrentCulture.Clone() as CultureInfo;
            oldCulture = Thread.CurrentThread.CurrentCulture;
            dotCulture.NumberFormat.NumberDecimalSeparator = ".";
            Thread.CurrentThread.CurrentCulture = dotCulture;
        }

        public void Unlock() {
            Thread.CurrentThread.CurrentCulture = oldCulture;
        }        
    }
}
