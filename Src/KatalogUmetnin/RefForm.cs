﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KatalogUmetnin
{
    public partial class RefForm : Form
    {
        private RefItems Items;
        private int autoinc;

        public RefForm(string Title, RefItems Items) {
            InitializeComponent();
            Settings.SetTexts(this);

            this.Text = Title;
            this.Items = Items;
            comboBoxRef.Items.AddRange(this.Items.ToArray());
            if (this.Items.Count > 0)
                comboBoxRef.SelectedIndex = 0;
            autoinc = this.Items[Items.Count - 1].ID + 1;
        }

        private void comboBoxRef_SelectedIndexChanged(object sender, EventArgs e) {
            RefItem Item = Items[comboBoxRef.SelectedIndex];

            textBoxEnglish.Text = Item.English;
            textBoxDeutsch.Text = Item.German;
            textBoxSlovensko.Text = Item.Slovene;
            listBoxAppear.BeginUpdate();
            listBoxAppear.Items.Clear();
            listBoxAppear.Items.AddRange(Item.Appearances.ToArray());
            listBoxAppear.EndUpdate();
            buttonSave.Enabled = false;
            buttonDelete.Enabled = Item.Appearances.Count == 0;
            buttonOpen.Enabled = false;
        }

        private void textBoxAnyLang_TextChanged(object sender, EventArgs e) {
            RefItem Item = Items[comboBoxRef.SelectedIndex];

            bool engChanged = !Item.English.Equals(textBoxEnglish.Text);
            bool gerChanged = !Item.German.Equals(textBoxDeutsch.Text);
            bool sloChanged = !Item.Slovene.Equals(textBoxSlovensko.Text);

            buttonSave.Enabled = engChanged || gerChanged || sloChanged;
        }

        private void buttonAddNew_Click(object sender, EventArgs e) {
            RefItem Item = new RefItem(autoinc,
                "New item " + autoinc, 
                "Neues item " + autoinc, 
                "Nov vnos " + autoinc, 
                new List<string>() {
            }, new List<int>() { });
            autoinc++;

            Items.Save(Item);

            comboBoxRef.BeginUpdate();
            comboBoxRef.Items.Clear();
            comboBoxRef.Items.AddRange(Items.ToArray());
            comboBoxRef.EndUpdate();
            comboBoxRef.SelectedIndex = Items.Count - 1;
        }

        private void buttonSave_Click(object sender, EventArgs e) {
            int idx = comboBoxRef.SelectedIndex;
            RefItem Item = Items[idx];

            Item.English = textBoxEnglish.Text;
            Item.German = textBoxDeutsch.Text;
            Item.Slovene = textBoxSlovensko.Text;
            Items.Save(Item);

            comboBoxRef.BeginUpdate();
            comboBoxRef.Items.Clear();
            comboBoxRef.Items.AddRange(Items.ToArray());
            comboBoxRef.EndUpdate();
            comboBoxRef.SelectedIndex = idx;
            textBoxAnyLang_TextChanged(this, e);
        }

        private void buttonDelete_Click(object sender, EventArgs e) {
            int idx = comboBoxRef.SelectedIndex;
            RefItem Item = Items[idx];

            Items.Delete(Item);

            comboBoxRef.BeginUpdate();
            comboBoxRef.Items.Clear();
            comboBoxRef.Items.AddRange(Items.ToArray());
            comboBoxRef.EndUpdate();
            comboBoxRef.SelectedIndex = Math.Min(idx, Items.Count - 1);
        }

        private void listBoxAppear_SelectedIndexChanged(object sender, EventArgs e) {
            buttonOpen.Enabled = listBoxAppear.SelectedIndex >= 0;
        }

        private void buttonOpen_Click(object sender, EventArgs e) {
            int idx = comboBoxRef.SelectedIndex;
            RefItem item = Items[idx];

            idx = listBoxAppear.SelectedIndex;
            if (idx >= 0) {
                int id = item.AppearanceIDs[idx];

                Umetnisko_delo ud = new Umetnisko_delo(id);
                if (ud.ShowDialog(this) == System.Windows.Forms.DialogResult.OK) {
                    //UNCOM refrešaj refform in form1
                    Items.Fill();
                    comboBoxRef_SelectedIndexChanged(this, null);
                }
            }
        }
    }

    public class RefItem
    {
        public int ID;
        public string English;
        public string German;
        public string Slovene;
        public List<string> Appearances;
        public List<int> AppearanceIDs;

        public RefItem(int ID, string English, string German, string Slovene,
            List<string> Appearances, List<int> IDs) {
            this.ID = ID;
            this.English = English;
            this.German = German;
            this.Slovene = Slovene;
            this.Appearances = new List<string>(Appearances);
            this.AppearanceIDs = new List<int>(IDs);
        }

        public override string ToString() {
            return English + " / " + German + " / " + Slovene;
        }
    }

    public class RefItems : List<RefItem>
    {
        private string TableName;
        private string IDField;
        private string Field;
        private string EnglishField;
        private string GermanField;
        private string SloveneField;
        private string JoinField;

        public RefItems(string TableName, string IDField, string Field, string JoinField) {
            this.IDField = IDField;
            this.TableName = TableName;
            this.Field = Field;
            this.EnglishField = Field + "_en";
            this.GermanField = Field + "_de";
            this.SloveneField = Field + "_si";
            this.JoinField = JoinField;
        }

        public void Fill() {
            Clear();

            DataView data = Query.Refs(TableName);

            for (int i = 0; i < data.Count; i++) {
                DataView view = Query.UmetnineAppear(JoinField, String.Format("{0}", (int)data[i][IDField]));

                List<string> appearances = new List<string>();

                List<int> ids = new List<int>();
                for (int j = 0; j < view.Count; j++) {
                    appearances.Add((string)view[j][Settings.GetLanguageExtType("naslov")]);
                    ids.Add((int)view[j]["Nr"]);
                }

                Add(new RefItem((int)data[i][IDField],
                    (string)data[i][EnglishField],
                    (string)data[i][GermanField],
                    (string)data[i][SloveneField], appearances, ids));
            }
            
            /*//debug fill
            Add(new RefItem("Zlato / Gold / Gold", "Gold", "Gold", "Zlato", new List<string>() {
                "Broska", "Ogrlica", "Ura", "Prstan"
            }));
            Add(new RefItem("Srebro / Silver / Silber", "Sliver", "Silber", "Srebro", new List<string>() {
                "Nakit"
            }));
            Add(new RefItem("Platina / Platin / Platinum", "Platin", "Platinum", "Platina", new List<string>() {
            }));*/
        }

        public bool Save(RefItem Item) {
            bool bRet = true;

            if (!Contains(Item)) {
                DataRow row = Query.InsertRef(TableName, Field, Item.ID, Item.English, Item.German, Item.Slovene);
                Add(Item);
            }
            else {
                DataRow row = Query.UpdateRef(TableName, Field, Item.ID, Item.English, Item.German, Item.Slovene);
            }

            return bRet;
        }

        public bool Delete(RefItem Item) {
            bool bRet = true;

            Query.DeleteRef(TableName, Item.ID);
            Remove(Item);

            return bRet;
        }
    }    
}
