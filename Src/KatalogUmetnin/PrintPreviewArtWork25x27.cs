using KatalogUmetnin.ImagePresentation;
using KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters;
using Microsoft.Reporting.WinForms;
using System;
using System.ComponentModel;
using System.Data;
using System.Threading;
using System.Windows.Forms;

namespace KatalogUmetnin
{
	public class PrintPreviewArtWork25x27 : System.Windows.Forms.Form
	{
		private string StUmDela = "";

		private bool PriceVisible = true;

		private IContainer components = null;

		private System.Windows.Forms.BindingSource UmetnineBindingSource;

		private KatalogUmetnin_dbDataSet KatalogUmetnin_dbDataSet;

		private UmetnineTableAdapter UmetnineTableAdapter;

		private ReportViewer reportViewer1;

		public PrintPreviewArtWork25x27(string st_umdela, bool priceHidden = false)
		{
			this.StUmDela = st_umdela;
			this.PriceVisible = !priceHidden;
			this.InitializeComponent();
		}

		private void PrintPreviewArtWork25x27_Load(object sender, EventArgs e)
		{
			PrintRecordSelection prs = new PrintRecordSelection(this.StUmDela);
			if (prs.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
                ReportDataSource rds = reportViewer1.LocalReport.DataSources[0];
                DataView u = Query.UmetnineLangJoined("st_um_dela", Settings.Get.LanguageShort, prs.SelectedArts.ToArray());
                u.Table.Columns.Add("velikost");
                foreach (DataRow row in u.Table.Rows)
                    row["velikost"] = Settings.FormatDimensions((int)row["dolzina"], (int)row["visina"], (int)row["sirina"]);
                rds.Value = u;

                this.reportViewer1.LocalReport.ReportEmbeddedResource = "KatalogUmetnin." + Settings.GetLanguageExtType("Report25x27")+".rdlc";
				this.UmetnineTableAdapter.ClearBeforeFill = false;
				foreach (string a in prs.SelectedArts)
				{
					//this.UmetnineTableAdapter.FillByStUmDela(this.KatalogUmetnin_dbDataSet.Umetnine, a);
				}
				string comPath = Img.CompositePrintLocation.TrimEnd(new char[]
				{
					'\\'
				}) + "\\";
				ReportParameter rp = new ReportParameter("CompositionPath", comPath);
				this.reportViewer1.LocalReport.SetParameters(rp);
				this.reportViewer1.LocalReport.SetParameters(new ReportParameter("PriceVisible", this.PriceVisible.ToString()));
				this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
				this.reportViewer1.RefreshReport();
			}
			else
			{
				base.Close();
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.UmetnineBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.KatalogUmetnin_dbDataSet = new KatalogUmetnin.KatalogUmetnin_dbDataSet();
            this.UmetnineTableAdapter = new KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters.UmetnineTableAdapter();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.UmetnineBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KatalogUmetnin_dbDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // UmetnineBindingSource
            // 
            this.UmetnineBindingSource.DataMember = "Umetnine";
            this.UmetnineBindingSource.DataSource = this.KatalogUmetnin_dbDataSet;
            // 
            // KatalogUmetnin_dbDataSet
            // 
            this.KatalogUmetnin_dbDataSet.DataSetName = "KatalogUmetnin_dbDataSet";
            this.KatalogUmetnin_dbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // UmetnineTableAdapter
            // 
            this.UmetnineTableAdapter.ClearBeforeFill = true;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DS2";
            reportDataSource1.Value = this.UmetnineBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.EnableExternalImages = true;
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "KatalogUmetnin.Report25x27DE.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(284, 261);
            this.reportViewer1.TabIndex = 1;
            // 
            // PrintPreviewArtWork25x27
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.reportViewer1);
            this.Icon = this.Icon;
            this.Name = "PrintPreviewArtWork25x27";
            this.Text = "Predogled tiskanja (25cm x 27cm)";
            this.Load += new System.EventHandler(this.PrintPreviewArtWork25x27_Load);
            ((System.ComponentModel.ISupportInitialize)(this.UmetnineBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KatalogUmetnin_dbDataSet)).EndInit();
            this.ResumeLayout(false);

		}
	}
}
