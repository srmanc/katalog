using KatalogUmetnin.ImagePresentation;
using KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters;
using KatalogUmetnin.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Resources;
using System.Data.OleDb;

namespace KatalogUmetnin
{
    public class Form1 : System.Windows.Forms.Form
    {
        public bool ExitOnStartup = false;

        private IContainer components = null;

        private System.Windows.Forms.DataGridView dataGridView1;

        private System.Windows.Forms.DataGridViewTextBoxColumn izkopicekDataGridViewTextBoxColumn;

        private System.Windows.Forms.PictureBox pbComposition;

        private System.Windows.Forms.Label label12;

        private System.Windows.Forms.Label label10;

        private System.Windows.Forms.TextBox textBoxOpis;

        private System.Windows.Forms.Label label7;

        private System.Windows.Forms.TextBox textBoxNegativ;

        private System.Windows.Forms.Label label6;

        private System.Windows.Forms.TextBox textBoxZaznamek;

        private System.Windows.Forms.Label label4;

        private System.Windows.Forms.TextBox textBoxMaterial;

        private System.Windows.Forms.Label labelDatumVnosa;

        private System.Windows.Forms.Label label1;

        private System.Windows.Forms.Label labelNr;

        private System.Windows.Forms.ToolStrip toolStrip1;

        private System.Windows.Forms.ToolStripButton toolStripButton1;

        private System.Windows.Forms.ToolStripButton toolStripButton2;

        private System.Windows.Forms.ToolStripButton toolStripButton3;

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;

        private System.Windows.Forms.ToolStripButton toolStripButton4;

        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;

        private System.Windows.Forms.ToolStripMenuItem natisniVsaDelaToolStripMenuItem;

        private System.Windows.Forms.ToolStripButton toolStripButton5;

        private System.Windows.Forms.ToolStripTextBox tbFilter;

        private System.Windows.Forms.ToolStripLabel toolStripLabel1;

        private System.Windows.Forms.ToolStripSplitButton splitDebug;

        private System.Windows.Forms.ToolStripMenuItem regenerateCompositsToolStripMenuItem;

        private System.Windows.Forms.ToolStripButton toolStripButton6;

        private System.Windows.Forms.ToolStripMenuItem natisniIzbranoDeloBrezCeneToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem x27ToolStripMenuItem;

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;

        private System.Windows.Forms.ToolStripMenuItem ausgewähltesKunstwerkOhnePreisDrucken25cmX27cmToolStripMenuItem;

        private System.Windows.Forms.ToolStripButton toolStripButton7;

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;

        private System.Windows.Forms.ToolStripMenuItem ausgewähltesKunstwerkDruckenToolStripMenuItem;

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;

        private System.Windows.Forms.ToolStripMenuItem bildDruckenToolStripMenuItem;

        private System.Windows.Forms.ToolStripButton toolStripButton8;

        private System.Windows.Forms.ToolStripMenuItem testWhiteToolStripMenuItem;

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private DataGridViewTextBoxColumn cenaDataGridViewTextBoxColumn;
        private ToolStripSplitButton toolStripSplitButton2;
        private ToolStripMenuItem urediTipToolStripMenuItem;
        private ToolStripMenuItem urediMaterialToolStripMenuItem;
        private DataGridViewTextBoxColumn Nr;
        private DataGridViewTextBoxColumn stumdelaDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn tipDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn naslovDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn letoizdelaveDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn dimenzijeDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn dolzinaDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn visinaDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn sirinaDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn zaznamekDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn _cenaDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn stnegativDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn materialDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn stslikDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn opisDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn datumDataGridViewTextBoxColumn;
        private ToolStripSplitButton toolStripSplitButton3;
        private ToolStripMenuItem natisniSpisekSCenoToolStripMenuItem;
        private ToolStripMenuItem natisniSpisekBrezCeneToolStripMenuItem;
        private ToolStripMenuItem transformDBToolStripMenuItem;

        private System.Windows.Forms.DataGridViewRow SelectedRow {
            get {
                System.Windows.Forms.DataGridViewRow result;
                if (this.dataGridView1.SelectedRows.Count > 0) {
                    result = this.dataGridView1.SelectedRows[0];
                }
                else {
                    result = null;
                }
                return result;
            }
        }

        private void TransformResourceToCode(string ResourcePath) {
            List<string> sl = new List<string>();

            if (!File.Exists(ResourcePath)) return;

            ResourceSet set = new ResourceSet(ResourcePath);
            IEnumerator e = set.GetEnumerator();
            while (e.MoveNext()) {
                DictionaryEntry entry = (DictionaryEntry)e.Current;

                string sKey = (string)entry.Key;

                if (sKey.Equals("Type") ||
                    sKey.Contains(".Type") ||
                    sKey.Contains(".ImeMode") ||
                    sKey.Contains("BrowserDialog"))
                    continue;
                if (sKey.StartsWith(">>"))
                    sKey = sKey.Substring(">>".Length);
                if (sKey.StartsWith("$"))
                    sKey = sKey.Substring("$".Length);
                if (sKey.Contains("ZOrder"))
                    continue;

                string s = sKey + " = ";
                if (entry.Value.GetType().Equals(typeof(string))) {
                    if (sKey.Contains(".Error")) {
                        s = "this.errorProvider1.SetError(" + sKey.Replace(".Error", "") + ", \"" + entry.Value + "\")";
                    }
                    else {
                        string sVal = entry.Value.ToString();
                        if (sVal.StartsWith("$")) {
                            sVal = sVal.Substring(1);
                            s += sVal;
                        }
                        else if (sKey.Contains(".Parent"))
                            s += sVal;
                        else
                            s += "\"" + sVal + "\"";
                    }
                }
                else if (entry.Value.GetType().Equals(typeof(Point))) {
                    s += "new Point(" + ((Point)(entry.Value)).X + ", " + ((Point)(entry.Value)).Y + ")";
                }
                else if (entry.Value.GetType().Equals(typeof(PointF))) {
                    s += "new PointF(" + ((PointF)(entry.Value)).X + ", " + ((PointF)(entry.Value)).Y + ")";
                }
                else if (entry.Value.GetType().Equals(typeof(SizeF))) {
                    s += "new SizeF(" + ((SizeF)(entry.Value)).Width + "," + ((SizeF)(entry.Value)).Height + ")";
                }
                else if (entry.Value.GetType().Equals(typeof(Size))) {
                    s += "new Size(" + ((Size)(entry.Value)).Width + "," + ((Size)(entry.Value)).Height + ")";
                }
                else if (entry.Value.GetType().Equals(typeof(Boolean))) {
                    s += entry.Value.ToString().ToLower();
                }
                else if (entry.Value.GetType().Equals(typeof(AnchorStyles))) {
                    s += entry.Value.ToString().Replace(", ", "|");
                    s = s.Replace("Bottom", "AnchorStyles.Bottom");
                    s = s.Replace("Right", "AnchorStyles.Right");
                    s = s.Replace("Left", "AnchorStyles.Left");
                    s = s.Replace("Top", "AnchorStyles.Top");
                }
                else if (entry.Value.GetType().Equals(typeof(FlatStyle))) {
                    s += "FlatStyle." + entry.Value.ToString();
                }
                else if (entry.Value.GetType().Equals(typeof(Int32))) {
                    if (sKey.Contains(".IconPadding")) {
                        s = "this.errorProvider1.SetIconPadding(" + sKey.Replace(".IconPadding", "") + ", " + entry.Value + ")";
                    }
                    else
                        s += entry.Value.ToString();
                }
                else if (entry.Value.GetType().Equals(typeof(Font))) {
                    Font f = (Font)entry.Value;
                    s += "new Font(\"" + f.Name + "\"," + (int)f.Size + ")";
                }
                else if (entry.Value.GetType().Equals(typeof(TextImageRelation))) {
                    s += "TextImageRelation." + entry.Value.ToString();
                }
                else if (entry.Value.GetType().Equals(typeof(ContentAlignment))) {
                    s += "ContentAlignment." + entry.Value.ToString();
                }
                else if (entry.Value.GetType().Equals(typeof(PictureBoxSizeMode))) {
                    s += "PictureBoxSizeMode." + entry.Value.ToString();
                }
                else if (entry.Value.GetType().Equals(typeof(ScrollBars))) {
                    s += "ScrollBars." + entry.Value.ToString();
                }
                else if (entry.Value.GetType().Equals(typeof(FormStartPosition))) {
                    s += "FormStartPosition." + entry.Value.ToString();
                }
                else if (entry.Value.GetType().Equals(typeof(ErrorIconAlignment))) {
                    s = "this.errorProvider1.SetIconAlignment(" + sKey.Replace(".IconAlignment", "") + ", ErrorIconAlignment." + entry.Value + ")";
                }
                else if (entry.Value.GetType().Equals(typeof(Padding))) {
                    s += "new Padding(" + ((Padding)(entry.Value)).Left + ", " + ((Padding)(entry.Value)).Top + ", " +
                        ((Padding)(entry.Value)).Right + ", " + ((Padding)(entry.Value)).Bottom + ")";
                }
                else if (entry.Value.GetType().Equals(typeof(Appearance))) {
                    s += "Appearance." + entry.Value.ToString();
                }
                else
                    s += entry.Value.ToString();

                s += ";";
                sl.Add(s);
            }

            string OutputPath = Path.ChangeExtension(ResourcePath, ".Designer.cs");
            StreamWriter writer = new StreamWriter(OutputPath);
            foreach (string s in sl)
                writer.WriteLine(s);
            writer.Flush();
        }

        private void TransformResources() {
            //    TransformResourceToCode(@"Y:\Work\Freelance\Katalog\Src\KatalogUmetnin.ImagePresentation\ImagePresentationUC.resources");
        }

        private string _T(object parent, string key) {
            string res = parent.GetType().ToString();
            System.Reflection.Assembly ass = parent.GetType().Assembly;
            ResourceManager man = new ResourceManager(res, ass);
            return man.GetString(key, CultureInfo.CurrentUICulture);
        }

        public Form1() {
            TransformResources();
            Settings.SetCulture();
            string storagePath = Settings.Get.StoragePath;
            if (string.IsNullOrEmpty(storagePath)) {
                SettingsForm sf = new SettingsForm();
                if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    storagePath = Settings.Get.StoragePath;
                }
                else {
                    this.ExitOnStartup = true;
                }
            }
            else {
                try {
                    if (!Directory.Exists(storagePath)) {
                        Directory.CreateDirectory(storagePath);
                    }
                    Img.Initialize(storagePath);
                }
                catch (Exception ex) {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
            }
            this.InitializeComponent();
            this.splitDebug.Visible = Debugger.IsAttached;
            Settings.SetTexts(this);
            _cenaDataGridViewTextBoxColumn.Visible = Settings.Get.ShowPrice;
        }

        private void Form1_Load(object sender, EventArgs e) {
            if (this.ExitOnStartup) {
                base.Close();
            }
            else {
                this.SetDatabase();
                Img.Initialize(Settings.Get.StoragePath.TrimEnd(new char[]
				{
					'\\'
				}) + "\\");
                this.LoadData();
                this.LoadDetails();
                this.dataGridView1.SelectionChanged += new EventHandler(this.dataGridView1_SelectionChanged);
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e) {
            this.LoadDetails();
        }

        public void LoadDetails() {
            if (this.SelectedRow != null) {
                this.LoadDetails(this.SelectedRow);
            }
            else {
                this.textBoxMaterial.Text = (this.textBoxNegativ.Text = (this.textBoxOpis.Text = (this.textBoxZaznamek.Text = (this.labelDatumVnosa.Text = (this.labelNr.Text = "")))));
            }
        }

        public void LoadDetails(System.Windows.Forms.DataGridViewRow r) {
            string idString = r.Cells["Nr"].Value.ToString();
            string compFile = Path.Combine(Img.CompositePreviewLocation, "Kompozicija_" + idString + ".png");
            if (File.Exists(compFile)) {
                this.pbComposition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                MemoryStream ms = new MemoryStream(File.ReadAllBytes(compFile));
                this.pbComposition.BackColor = System.Drawing.Color.White;
                this.pbComposition.Image = System.Drawing.Image.FromStream(ms);
            }
            else {
                this.pbComposition.BorderStyle = System.Windows.Forms.BorderStyle.None;
                this.pbComposition.Image = null;
                this.pbComposition.BackColor = System.Drawing.SystemColors.Control;
            }
            this.textBoxMaterial.Text = r.Cells["materialDataGridViewTextBoxColumn"].Value.ToString();
            this.textBoxNegativ.Text = r.Cells["stnegativDataGridViewTextBoxColumn"].Value.ToString();
            this.textBoxOpis.Text = r.Cells["opisDataGridViewTextBoxColumn"].Value.ToString();
            this.textBoxZaznamek.Text = r.Cells["zaznamekDataGridViewTextBoxColumn"].Value.ToString();
            this.labelDatumVnosa.Text = r.Cells["datumDataGridViewTextBoxColumn"].Value.ToString();
            this.labelNr.Text = r.Cells["Nr"].Value.ToString();
        }        

        public void LoadData() {
            naslovDataGridViewTextBoxColumn.DataPropertyName = Settings.GetLanguageExtType(naslovDataGridViewTextBoxColumn.DataPropertyName);
            tipDataGridViewTextBoxColumn.DataPropertyName = Settings.GetLanguageExtType(tipDataGridViewTextBoxColumn.DataPropertyName);
            materialDataGridViewTextBoxColumn.DataPropertyName = Settings.GetLanguageExtType(materialDataGridViewTextBoxColumn.DataPropertyName);
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = Query.UmetnineJoined().Table;
        }

        private void SetDatabase() {
            string sPath = Path.Combine(System.Windows.Forms.Application.StartupPath, Settings.DatabaseFileV2);

            if (!File.Exists(sPath)) {
                string sPathOld = Path.Combine(System.Windows.Forms.Application.StartupPath, Settings.DatabaseFile);
                
                if (!File.Exists(sPathOld)) {
                    System.Windows.Forms.MessageBox.Show("Pot do baze podatkov je neveljavna, baza podatkov\n [" + sPath + "]\n ne obstaja!", "Napaka DB", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
                    System.Windows.Forms.Application.Exit();
                }

                TransformDB.ToVer2(sPathOld, sPath);
                if (!File.Exists(sPath)) {
                    System.Windows.Forms.MessageBox.Show("Pri predelavi baze podatkov je prišlo do napake!", "Napaka DB", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
                    System.Windows.Forms.Application.Exit();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e) {
        }

        private void button2_Click(object sender, EventArgs e) {
        }

        private void dataGridView1_CellDoubleClick(object sender, System.Windows.Forms.DataGridViewCellEventArgs e) {
            if (e.RowIndex < 0)
                return;
            int id = Convert.ToInt32(this.dataGridView1.Rows[e.RowIndex].Cells["Nr"].Value);            
            Umetnisko_delo ud = new Umetnisko_delo(id);
            if (ud.ShowDialog(this) == System.Windows.Forms.DialogResult.OK) {
                int idx = this.dataGridView1.FirstDisplayedScrollingRowIndex;
                dataGridView1.DataSource = Query.UmetnineJoined();
                this.LoadDetails();
                this.SelectDGVRowByID(id.ToString(), idx);                
            }
        }

        private void SelectDGVRowByID(string id, int idx) {
            int r = -1;
            for (int i = 0; i < this.dataGridView1.Rows.Count; i++) {
                if (this.dataGridView1.Rows[i].Cells["Nr"].Value.ToString() == id) {
                    r = i;
                    break;
                }
            }
            if (r >= 0) {
                this.dataGridView1.Rows[0].Selected = false;
                this.dataGridView1.Rows[r].Selected = true;
                if (idx >= 0)
                    this.dataGridView1.FirstDisplayedScrollingRowIndex = idx;
                else
                    this.dataGridView1.FirstDisplayedScrollingRowIndex = r;
            }
        }

        private void button3_Click(object sender, EventArgs e) {
        }

        private void button4_Click(object sender, EventArgs e) {
        }

        private void tbFilter_TextChanged(object sender, EventArgs e) {
        }

        private string ApplyFilter() {
            string f = "";
            string t = this.tbFilter.Text.Trim();
            if (!string.IsNullOrEmpty(t)) { 
                f = string.Concat(new string[] 
				{
					"(" + Settings.GetLanguageExtType("naslov") + " LIKE '%",
					t,
					"%') OR (st_um_dela LIKE '%",
					t,
					"%')"
				});
            }            
            //this.umetnineBindingSource.Filter = f;
            DataView view = this.dataGridView1.DataSource as DataView;
            if (view == null)
                view = new DataView(dataGridView1.DataSource as DataTable);
            view.RowFilter = f;
            dataGridView1.DataSource = view;

            return f;
        }

        private void button5_Click(object sender, EventArgs e) {
        }

        private void button6_Click(object sender, EventArgs e) {
        }

        private void toolStripButton1_Click(object sender, EventArgs e) {
            Umetnisko_delo ud = new Umetnisko_delo();
            System.Windows.Forms.DialogResult dr = ud.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK) {
                int id = ud.Nmr;
                int idx = this.dataGridView1.FirstDisplayedScrollingRowIndex;
                dataGridView1.DataSource = Query.UmetnineJoined();
                this.LoadDetails();
                this.SelectDGVRowByID(id.ToString(), idx); 
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e) {
            if (this.SelectedRow != null) {
                int id = Convert.ToInt32(this.SelectedRow.Cells["Nr"].Value);
                int idx = this.dataGridView1.FirstDisplayedScrollingRowIndex;
                Umetnisko_delo ud = new Umetnisko_delo(id);
                if (ud.ShowDialog(this) == System.Windows.Forms.DialogResult.OK) {
                    dataGridView1.DataSource = Query.UmetnineJoined();
                    this.LoadDetails();
                    this.SelectDGVRowByID(id.ToString(), idx);
                }
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e) {
            if (this.SelectedRow != null) {
                if (System.Windows.Forms.MessageBox.Show(MessageStrings.ArtDeleteText, MessageStrings.ArtDeleteTitle, System.Windows.Forms.MessageBoxButtons.YesNoCancel, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes) {
                    //int id = Convert.ToInt32(this.SelectedRow.Cells["Nr"].Value);
                    int prev_id = -1;
                        
                    int idx = this.dataGridView1.FirstDisplayedScrollingRowIndex;
                    //this.umetnineTableAdapter.DeleteUmetnina(Convert.ToInt32(idString));
                    for (int i = dataGridView1.RowCount - 1; i >= 0; i--) 
                        if (this.dataGridView1.Rows[i].Selected) {
                            if (prev_id == -1)
                                if (i == dataGridView1.RowCount - 1)
                                    prev_id = Convert.ToInt32(this.dataGridView1.Rows[i - 1].Cells["Nr"].Value);
                                else
                                    prev_id = Convert.ToInt32(this.dataGridView1.Rows[i + 1].Cells["Nr"].Value);
                            int id = Convert.ToInt32(this.dataGridView1.Rows[i].Cells["Nr"].Value);
                            Query.DeleteUmetnina(id);
                        }
                    dataGridView1.DataSource = Query.UmetnineJoined();
                    //this.LoadData();
                    this.LoadDetails();
                    this.SelectDGVRowByID(prev_id.ToString(), idx);
                }
            }
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e) {
            this.toolStripSplitButton1.ShowDropDown();
        }

        private void natisniVsaDelaToolStripMenuItem_Click(object sender, EventArgs e) {
        }

        private void toolStrip1_ItemClicked(object sender, System.Windows.Forms.ToolStripItemClickedEventArgs e) {
        }

        private void toolStripButton4_Click(object sender, EventArgs e) {
            /*string s = "";
            if (this.dataGridView1.SortedColumn != null) {
                s = this.dataGridView1.SortedColumn.DataPropertyName;
                if (this.dataGridView1.SortOrder == System.Windows.Forms.SortOrder.Ascending) {
                    s += " asc";
                }
                else if (this.dataGridView1.SortOrder == System.Windows.Forms.SortOrder.Descending) {
                    s += " desc";
                }
            }
            string f = this.ApplyFilter();
            PrintPreviewList ppl = new PrintPreviewList(f, s);
            ppl.ShowDialog(this);*/
        }

        private void toolStripButton5_Click(object sender, EventArgs e) {
            this.tbFilter.Clear();
        }

        private void toolStripTextBox1_Click(object sender, EventArgs e) {
        }

        private void toolStripTextBox1_TextChanged(object sender, EventArgs e) {
            this.ApplyFilter();
        }

        private void toolStripButton6_Click(object sender, EventArgs e) {
        }

        private void regenerateCompositsToolStripMenuItem_Click(object sender, EventArgs e) {
            /*System.Drawing.Size s = new ImagePresentationUC().CanvasSize;
            List<CanvasImg> ci = new List<CanvasImg>();
            ImagePresentationUC ip = new ImagePresentationUC();
            Dictionary<int, string> opisCache = new UmetnineOpisTableAdapter().GetData().ToDictionary((KatalogUmetnin_dbDataSet.UmetnineOpisRow r) => r.Nr, (KatalogUmetnin_dbDataSet.UmetnineOpisRow r) => r.opis);
            SlikeTableAdapter slikeTA = new SlikeTableAdapter();
            foreach (System.Windows.Forms.DataGridViewRow r2 in ((IEnumerable)this.dataGridView1.Rows)) {
                try {
                    ci = new List<CanvasImg>();
                    int id = Convert.ToInt32(r2.Cells["Nr"].Value.ToString());
                    string o = opisCache.ContainsKey(id) ? opisCache[id] : "";
                    slikeTA.FillByNr(this.katalogUmetnin_dbDataSet.Slike, new int?(id));
                    ip.LoadData(id, this.katalogUmetnin_dbDataSet.Slike, o);
                    ExportCanvas ec = new ExportCanvas(id, s, ip.CanvasImages, o);
                    ec.Export();
                }
                catch (Exception ex_126) {
                }
            }
            System.Windows.Forms.MessageBox.Show("Done");*/
        }

        private void splitDebug_ButtonClick(object sender, EventArgs e) {
            this.splitDebug.ShowDropDown();
        }

        private void toolStripComboBox1_Click(object sender, EventArgs e) {
        }

        private void toolStripComboBox1_TextChanged(object sender, EventArgs e) {
            System.Windows.Forms.ToolStripComboBox t = (System.Windows.Forms.ToolStripComboBox)sender;
            this.ChangeLanguage(new CultureInfo(t.Text));
        }

        private void ChangeLanguage(CultureInfo culture) {
            Thread.CurrentThread.CurrentUICulture = culture;
            foreach (System.Windows.Forms.Form form in System.Windows.Forms.Application.OpenForms) {
                this.ApplyControlResources(new ComponentResourceManager(form.GetType()), form);
            }
        }

        private void ApplyControlResources(ComponentResourceManager resourceManager, System.Windows.Forms.Control control) {
            if (control is System.Windows.Forms.Form) {
                resourceManager.ApplyResources(control, "$this");
            }
            else {
                resourceManager.ApplyResources(control, control.Name);
            }
            if (control is System.Windows.Forms.IContainerControl) {
                foreach (System.Windows.Forms.Control childControl in control.Controls) {
                    this.ApplyControlResources(resourceManager, childControl);
                }
            }
            else if (control is System.Windows.Forms.ToolStrip) {
                System.Windows.Forms.ToolStrip ts = (System.Windows.Forms.ToolStrip)control;
                foreach (System.Windows.Forms.ToolStripItem childControl2 in ts.Items) {
                    if (childControl2 is System.Windows.Forms.ToolStripSplitButton) {
                        System.Windows.Forms.ToolStripSplitButton tsb = (System.Windows.Forms.ToolStripSplitButton)childControl2;
                        foreach (System.Windows.Forms.ToolStripMenuItem it in tsb.DropDownItems) {
                            resourceManager.ApplyResources(it, it.Name);
                        }
                    }
                    else {
                        resourceManager.ApplyResources(childControl2, childControl2.Name);
                    }
                }
            }
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e) {
            System.Windows.Forms.ToolStripComboBox t = (System.Windows.Forms.ToolStripComboBox)sender;
            this.ChangeLanguage(new CultureInfo(t.Text));
        }

        private void toolStripButton6_Click_1(object sender, EventArgs e) {
            SettingsForm sf = new SettingsForm();
            if (sf.ShowDialog(this) == System.Windows.Forms.DialogResult.OK) {
                string storagePath = Settings.Get.StoragePath;
                if (!Directory.Exists(storagePath)) {
                    Directory.CreateDirectory(storagePath);
                }
                _cenaDataGridViewTextBoxColumn.Visible = Settings.Get.ShowPrice;
                Img.Initialize(storagePath);
                LoadData();
            }
        }

        private void natisniIzbranoDeloBrezCeneToolStripMenuItem_Click(object sender, EventArgs e) {
            if (this.SelectedRow != null) {
                string st_umdela = this.SelectedRow.Cells["stumdelaDataGridViewTextBoxColumn"].Value.ToString();
                PrintPreviewArtWork ppaw = new PrintPreviewArtWork(st_umdela, false);
                ppaw.ShowDialog(this);
            }
        }

        private void x27ToolStripMenuItem_Click(object sender, EventArgs e) {
            if (this.SelectedRow != null) {
                string st_umdela = this.SelectedRow.Cells["stumdelaDataGridViewTextBoxColumn"].Value.ToString();
                PrintPreviewArtWork25x27 ppaw = new PrintPreviewArtWork25x27(st_umdela, false);
                ppaw.ShowDialog(this);
            }
        }

        private void ausgewähltesKunstwerkOhnePreisDrucken25cmX27cmToolStripMenuItem_Click(object sender, EventArgs e) {
            if (this.SelectedRow != null) {
                string st_umdela = this.SelectedRow.Cells["stumdelaDataGridViewTextBoxColumn"].Value.ToString();
                PrintPreviewArtWork25x27 ppaw = new PrintPreviewArtWork25x27(st_umdela, true);
                ppaw.ShowDialog(this);
            }
        }

        private void toolStripButton7_Click(object sender, EventArgs e) {
            if (this.SelectedRow != null) {
                int id = Convert.ToInt32(this.SelectedRow.Cells["Nr"].Value);
                string compFile = Path.Combine(Img.CompositePrintLocation, "Kompozicija_" + id.ToString() + ".png");
                if (File.Exists(compFile)) {
                    Process.Start(compFile);
                }
            }
        }

        private void ausgewähltesKunstwerkDruckenToolStripMenuItem_Click(object sender, EventArgs e) {
            if (this.SelectedRow != null) {                
                string st_umdela = this.SelectedRow.Cells["stumdelaDataGridViewTextBoxColumn"].Value.ToString();
                PrintPreviewArtWork ppaw = new PrintPreviewArtWork(st_umdela, true);                
                ppaw.ShowDialog(this);                
            }
        }

        private void bildDruckenToolStripMenuItem_Click(object sender, EventArgs e) {
            if (this.SelectedRow != null) {
                string st_umdela = this.SelectedRow.Cells["stumdelaDataGridViewTextBoxColumn"].Value.ToString();
                PrintPreviewArtWork25x27ImageOnly ppaw = new PrintPreviewArtWork25x27ImageOnly(st_umdela);
                ppaw.ShowDialog(this);
            }
        }

        private void toolStripButton8_Click(object sender, EventArgs e) {
            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                Dictionary<string, string> filesToExport = new Dictionary<string, string>();
                char[] inv = Path.GetInvalidFileNameChars();
                DataTable umetnine = Query.UmetnineJoined().Table; 
                foreach (DataRow umetnina in umetnine.Rows) {
                    string compFile = Path.Combine(Img.CompositePrintLocation, "Kompozicija_" + umetnina["Nr"].ToString() + ".png");                    
                    if (File.Exists(compFile)) {
                        string newFileName = Path.Combine(fbd.SelectedPath, umetnina[Settings.GetLanguageExtType("ImeTipa")] + " " + umetnina["st_um_dela"].ToString().PadLeft(4, '0')); // + " - ");
                        /*string text = umetnina[Settings.GetLanguageExtType("naslov")].ToString().Trim();
                        for (int i = 0; i < text.Length; i++) {
                            char c = text[i];
                            if (!inv.Contains(c)) {
                                newFileName += c.ToString();
                            }
                        }*/
                        newFileName += ".png";                        
                        filesToExport.Add(compFile, newFileName);
                    }
                }
                foreach (KeyValuePair<string, string> d in filesToExport) {
                    try {
                        File.Copy(d.Key, d.Value, true);
                    }
                    catch (Exception ex_177) {
                    }
                }
                System.Windows.Forms.MessageBox.Show("EXPORT: OK!");
            }
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e) {
        }

        private void testWhiteToolStripMenuItem_Click(object sender, EventArgs e) {
            /*System.Drawing.Size s = new ImagePresentationUC().CanvasSize;
            List<CanvasImg> ci = new List<CanvasImg>();
            ImagePresentationUC ip = new ImagePresentationUC();
            Dictionary<int, string> opisCache = new UmetnineOpisTableAdapter().GetData().ToDictionary((KatalogUmetnin_dbDataSet.UmetnineOpisRow r) => r.Nr, (KatalogUmetnin_dbDataSet.UmetnineOpisRow r) => r.opis);
            SlikeTableAdapter slikeTA = new SlikeTableAdapter();
            IEnumerator enumerator = ((IEnumerable)this.dataGridView1.Rows).GetEnumerator();
            try {
                if (enumerator.MoveNext()) {
                    System.Windows.Forms.DataGridViewRow r2 = (System.Windows.Forms.DataGridViewRow)enumerator.Current;
                    ci = new List<CanvasImg>();
                    int id = Convert.ToInt32(r2.Cells["Nr"].Value.ToString());
                    string o = opisCache.ContainsKey(id) ? opisCache[id] : "";
                    slikeTA.FillByNr(this.katalogUmetnin_dbDataSet.Slike, new int?(id));
                    //ip.LoadData(id, this.katalogUmetnin_dbDataSet.Slike, o);
                    ExportCanvas ec = new ExportCanvas(id, s, ip.CanvasImages, o);
                    ec.Export();
                    Process.Start(Path.Combine(Img.CompositePreviewLocation, "Kompozicija_" + id.ToString() + ".png"));
                }
            }
            finally {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable != null) {
                    disposable.Dispose();
                }
            }*/
        }

        private void transformDBToolStripMenuItem_Click(object sender, EventArgs e) {
            TransformDB.ToVer2(Settings.DatabaseFile, Settings.DatabaseFileV2);
        }

        internal void EnableDebug() {
            this.splitDebug.Visible = true;
        }

        protected override void Dispose(bool disposing) {
            if (disposing && this.components != null) {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Nr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stumdelaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.naslovDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.letoizdelaveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dimenzijeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dolzinaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.visinaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sirinaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zaznamekDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._cenaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stnegativDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stslikDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.opisDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxOpis = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxNegativ = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxZaznamek = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxMaterial = new System.Windows.Forms.TextBox();
            this.labelDatumVnosa = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelNr = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.natisniVsaDelaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ausgewähltesKunstwerkDruckenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.natisniIzbranoDeloBrezCeneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ausgewähltesKunstwerkOhnePreisDrucken25cmX27cmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x27ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bildDruckenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSplitButton3 = new System.Windows.Forms.ToolStripSplitButton();
            this.natisniSpisekSCenoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.natisniSpisekBrezCeneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.tbFilter = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.splitDebug = new System.Windows.Forms.ToolStripSplitButton();
            this.regenerateCompositsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testWhiteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.transformDBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSplitButton2 = new System.Windows.Forms.ToolStripSplitButton();
            this.urediTipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.urediMaterialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbComposition = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbComposition)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nr,
            this.stumdelaDataGridViewTextBoxColumn,
            this.tipDataGridViewTextBoxColumn,
            this.naslovDataGridViewTextBoxColumn,
            this.letoizdelaveDataGridViewTextBoxColumn,
            this.dimenzijeDataGridViewTextBoxColumn,
            this.dolzinaDataGridViewTextBoxColumn,
            this.visinaDataGridViewTextBoxColumn,
            this.sirinaDataGridViewTextBoxColumn,
            this.zaznamekDataGridViewTextBoxColumn,
            this._cenaDataGridViewTextBoxColumn,
            this.stnegativDataGridViewTextBoxColumn,
            this.materialDataGridViewTextBoxColumn,
            this.stslikDataGridViewTextBoxColumn,
            this.opisDataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn,
            this.datumDataGridViewTextBoxColumn});
            this.dataGridView1.Location = new System.Drawing.Point(12, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1122, 389);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            // 
            // Nr
            // 
            this.Nr.DataPropertyName = "Nr";
            this.Nr.HeaderText = "Nr";
            this.Nr.Name = "Nr";
            this.Nr.ReadOnly = true;
            this.Nr.Visible = false;
            this.Nr.Width = 120;
            // 
            // stumdelaDataGridViewTextBoxColumn
            // 
            this.stumdelaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.stumdelaDataGridViewTextBoxColumn.DataPropertyName = "st_um_dela";
            this.stumdelaDataGridViewTextBoxColumn.FillWeight = 140F;
            this.stumdelaDataGridViewTextBoxColumn.HeaderText = "Št. umetniškega dela";
            this.stumdelaDataGridViewTextBoxColumn.MinimumWidth = 140;
            this.stumdelaDataGridViewTextBoxColumn.Name = "stumdelaDataGridViewTextBoxColumn";
            this.stumdelaDataGridViewTextBoxColumn.ReadOnly = true;
            this.stumdelaDataGridViewTextBoxColumn.Width = 140;
            // 
            // tipDataGridViewTextBoxColumn
            // 
            this.tipDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.tipDataGridViewTextBoxColumn.DataPropertyName = "ImeTipa_si";
            this.tipDataGridViewTextBoxColumn.HeaderText = "Tip";
            this.tipDataGridViewTextBoxColumn.Name = "tipDataGridViewTextBoxColumn";
            this.tipDataGridViewTextBoxColumn.ReadOnly = true;
            this.tipDataGridViewTextBoxColumn.Visible = false;
            // 
            // naslovDataGridViewTextBoxColumn
            // 
            this.naslovDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.naslovDataGridViewTextBoxColumn.DataPropertyName = "naslov";
            this.naslovDataGridViewTextBoxColumn.HeaderText = "Naslov dela";
            this.naslovDataGridViewTextBoxColumn.Name = "naslovDataGridViewTextBoxColumn";
            this.naslovDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // letoizdelaveDataGridViewTextBoxColumn
            // 
            this.letoizdelaveDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.letoizdelaveDataGridViewTextBoxColumn.DataPropertyName = "leto_izdelave";
            this.letoizdelaveDataGridViewTextBoxColumn.HeaderText = "Leto izdelave";
            this.letoizdelaveDataGridViewTextBoxColumn.Name = "letoizdelaveDataGridViewTextBoxColumn";
            this.letoizdelaveDataGridViewTextBoxColumn.ReadOnly = true;
            this.letoizdelaveDataGridViewTextBoxColumn.Width = 95;
            // 
            // dimenzijeDataGridViewTextBoxColumn
            // 
            this.dimenzijeDataGridViewTextBoxColumn.HeaderText = "Dimenzije";
            this.dimenzijeDataGridViewTextBoxColumn.Name = "dimenzijeDataGridViewTextBoxColumn";
            this.dimenzijeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dolzinaDataGridViewTextBoxColumn
            // 
            this.dolzinaDataGridViewTextBoxColumn.DataPropertyName = "dolzina";
            this.dolzinaDataGridViewTextBoxColumn.HeaderText = "Dolzina";
            this.dolzinaDataGridViewTextBoxColumn.Name = "dolzinaDataGridViewTextBoxColumn";
            this.dolzinaDataGridViewTextBoxColumn.ReadOnly = true;
            this.dolzinaDataGridViewTextBoxColumn.Visible = false;
            // 
            // visinaDataGridViewTextBoxColumn
            // 
            this.visinaDataGridViewTextBoxColumn.DataPropertyName = "visina";
            this.visinaDataGridViewTextBoxColumn.HeaderText = "Višina";
            this.visinaDataGridViewTextBoxColumn.Name = "visinaDataGridViewTextBoxColumn";
            this.visinaDataGridViewTextBoxColumn.ReadOnly = true;
            this.visinaDataGridViewTextBoxColumn.Visible = false;
            // 
            // sirinaDataGridViewTextBoxColumn
            // 
            this.sirinaDataGridViewTextBoxColumn.DataPropertyName = "sirina";
            this.sirinaDataGridViewTextBoxColumn.HeaderText = "Širina";
            this.sirinaDataGridViewTextBoxColumn.Name = "sirinaDataGridViewTextBoxColumn";
            this.sirinaDataGridViewTextBoxColumn.ReadOnly = true;
            this.sirinaDataGridViewTextBoxColumn.Visible = false;
            // 
            // zaznamekDataGridViewTextBoxColumn
            // 
            this.zaznamekDataGridViewTextBoxColumn.DataPropertyName = "zaznamek";
            this.zaznamekDataGridViewTextBoxColumn.HeaderText = "Zaznamek";
            this.zaznamekDataGridViewTextBoxColumn.Name = "zaznamekDataGridViewTextBoxColumn";
            this.zaznamekDataGridViewTextBoxColumn.ReadOnly = true;
            this.zaznamekDataGridViewTextBoxColumn.Visible = false;
            // 
            // _cenaDataGridViewTextBoxColumn
            // 
            this._cenaDataGridViewTextBoxColumn.DataPropertyName = "cena";
            this._cenaDataGridViewTextBoxColumn.HeaderText = "Cena";
            this._cenaDataGridViewTextBoxColumn.Name = "_cenaDataGridViewTextBoxColumn";
            this._cenaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // stnegativDataGridViewTextBoxColumn
            // 
            this.stnegativDataGridViewTextBoxColumn.DataPropertyName = "st_negativ";
            this.stnegativDataGridViewTextBoxColumn.HeaderText = "Št. negativa";
            this.stnegativDataGridViewTextBoxColumn.Name = "stnegativDataGridViewTextBoxColumn";
            this.stnegativDataGridViewTextBoxColumn.ReadOnly = true;
            this.stnegativDataGridViewTextBoxColumn.Visible = false;
            // 
            // materialDataGridViewTextBoxColumn
            // 
            this.materialDataGridViewTextBoxColumn.DataPropertyName = "ImeMateriala_si";
            this.materialDataGridViewTextBoxColumn.HeaderText = "Material";
            this.materialDataGridViewTextBoxColumn.Name = "materialDataGridViewTextBoxColumn";
            this.materialDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // stslikDataGridViewTextBoxColumn
            // 
            this.stslikDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.stslikDataGridViewTextBoxColumn.DataPropertyName = "st_slik";
            this.stslikDataGridViewTextBoxColumn.HeaderText = "Št. slik";
            this.stslikDataGridViewTextBoxColumn.Name = "stslikDataGridViewTextBoxColumn";
            this.stslikDataGridViewTextBoxColumn.ReadOnly = true;
            this.stslikDataGridViewTextBoxColumn.Visible = false;
            // 
            // opisDataGridViewTextBoxColumn
            // 
            this.opisDataGridViewTextBoxColumn.DataPropertyName = "opis";
            this.opisDataGridViewTextBoxColumn.HeaderText = "opis";
            this.opisDataGridViewTextBoxColumn.Name = "opisDataGridViewTextBoxColumn";
            this.opisDataGridViewTextBoxColumn.ReadOnly = true;
            this.opisDataGridViewTextBoxColumn.Visible = false;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // datumDataGridViewTextBoxColumn
            // 
            this.datumDataGridViewTextBoxColumn.DataPropertyName = "datum";
            this.datumDataGridViewTextBoxColumn.HeaderText = "Datum";
            this.datumDataGridViewTextBoxColumn.Name = "datumDataGridViewTextBoxColumn";
            this.datumDataGridViewTextBoxColumn.ReadOnly = true;
            this.datumDataGridViewTextBoxColumn.Visible = false;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(12, 619);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(106, 13);
            this.label12.TabIndex = 34;
            this.label12.Text = "Datum vnosa";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(12, 501);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(106, 15);
            this.label10.TabIndex = 33;
            this.label10.Text = "Opis";
            // 
            // textBoxOpis
            // 
            this.textBoxOpis.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxOpis.Location = new System.Drawing.Point(124, 501);
            this.textBoxOpis.Multiline = true;
            this.textBoxOpis.Name = "textBoxOpis";
            this.textBoxOpis.ReadOnly = true;
            this.textBoxOpis.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxOpis.Size = new System.Drawing.Size(244, 115);
            this.textBoxOpis.TabIndex = 32;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(0, 452);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(118, 17);
            this.label7.TabIndex = 31;
            this.label7.Text = "Številka negativa";
            // 
            // textBoxNegativ
            // 
            this.textBoxNegativ.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxNegativ.Location = new System.Drawing.Point(124, 449);
            this.textBoxNegativ.Name = "textBoxNegativ";
            this.textBoxNegativ.ReadOnly = true;
            this.textBoxNegativ.Size = new System.Drawing.Size(107, 20);
            this.textBoxNegativ.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(12, 478);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(106, 17);
            this.label6.TabIndex = 29;
            this.label6.Text = "Zaznamek";
            // 
            // textBoxZaznamek
            // 
            this.textBoxZaznamek.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxZaznamek.Location = new System.Drawing.Point(124, 475);
            this.textBoxZaznamek.Name = "textBoxZaznamek";
            this.textBoxZaznamek.ReadOnly = true;
            this.textBoxZaznamek.Size = new System.Drawing.Size(244, 20);
            this.textBoxZaznamek.TabIndex = 28;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(15, 426);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 17);
            this.label4.TabIndex = 27;
            this.label4.Text = "Material";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxMaterial
            // 
            this.textBoxMaterial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxMaterial.Location = new System.Drawing.Point(124, 423);
            this.textBoxMaterial.Name = "textBoxMaterial";
            this.textBoxMaterial.ReadOnly = true;
            this.textBoxMaterial.Size = new System.Drawing.Size(244, 20);
            this.textBoxMaterial.TabIndex = 26;
            // 
            // labelDatumVnosa
            // 
            this.labelDatumVnosa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDatumVnosa.AutoSize = true;
            this.labelDatumVnosa.Location = new System.Drawing.Point(121, 619);
            this.labelDatumVnosa.Name = "labelDatumVnosa";
            this.labelDatumVnosa.Size = new System.Drawing.Size(90, 13);
            this.labelDatumVnosa.TabIndex = 35;
            this.labelDatumVnosa.Text = "labelDatumVnosa";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(12, 640);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Šifra v bazi";
            // 
            // labelNr
            // 
            this.labelNr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelNr.AutoSize = true;
            this.labelNr.Location = new System.Drawing.Point(121, 640);
            this.labelNr.Name = "labelNr";
            this.labelNr.Size = new System.Drawing.Size(40, 13);
            this.labelNr.TabIndex = 37;
            this.labelNr.Text = "labelNr";
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripSeparator1,
            this.toolStripSplitButton1,
            this.toolStripSplitButton3,
            this.toolStripButton4,
            this.toolStripButton5,
            this.tbFilter,
            this.toolStripLabel1,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripSeparator3,
            this.toolStripButton6,
            this.splitDebug,
            this.toolStripSplitButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1146, 39);
            this.toolStrip1.TabIndex = 43;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(74, 36);
            this.toolStripButton1.Text = "Dodaj";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(71, 36);
            this.toolStripButton2.Text = "Uredi";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(73, 36);
            this.toolStripButton3.Text = "Izbriši";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.natisniVsaDelaToolStripMenuItem,
            this.ausgewähltesKunstwerkDruckenToolStripMenuItem,
            this.natisniIzbranoDeloBrezCeneToolStripMenuItem,
            this.toolStripSeparator2,
            this.ausgewähltesKunstwerkOhnePreisDrucken25cmX27cmToolStripMenuItem,
            this.x27ToolStripMenuItem,
            this.toolStripSeparator4,
            this.bildDruckenToolStripMenuItem});
            this.toolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton1.Image")));
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(160, 36);
            this.toolStripSplitButton1.Text = "Natisni izbrano delo";
            this.toolStripSplitButton1.ButtonClick += new System.EventHandler(this.toolStripSplitButton1_ButtonClick);
            // 
            // natisniVsaDelaToolStripMenuItem
            // 
            this.natisniVsaDelaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("natisniVsaDelaToolStripMenuItem.Image")));
            this.natisniVsaDelaToolStripMenuItem.Name = "natisniVsaDelaToolStripMenuItem";
            this.natisniVsaDelaToolStripMenuItem.Size = new System.Drawing.Size(312, 22);
            this.natisniVsaDelaToolStripMenuItem.Text = "Natisni vsa dela";
            this.natisniVsaDelaToolStripMenuItem.Visible = false;
            this.natisniVsaDelaToolStripMenuItem.Click += new System.EventHandler(this.natisniVsaDelaToolStripMenuItem_Click);
            // 
            // ausgewähltesKunstwerkDruckenToolStripMenuItem
            // 
            this.ausgewähltesKunstwerkDruckenToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ausgewähltesKunstwerkDruckenToolStripMenuItem.Image")));
            this.ausgewähltesKunstwerkDruckenToolStripMenuItem.Name = "ausgewähltesKunstwerkDruckenToolStripMenuItem";
            this.ausgewähltesKunstwerkDruckenToolStripMenuItem.Size = new System.Drawing.Size(312, 22);
            this.ausgewähltesKunstwerkDruckenToolStripMenuItem.Click += new System.EventHandler(this.ausgewähltesKunstwerkDruckenToolStripMenuItem_Click);
            // 
            // natisniIzbranoDeloBrezCeneToolStripMenuItem
            // 
            this.natisniIzbranoDeloBrezCeneToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("natisniIzbranoDeloBrezCeneToolStripMenuItem.Image")));
            this.natisniIzbranoDeloBrezCeneToolStripMenuItem.Name = "natisniIzbranoDeloBrezCeneToolStripMenuItem";
            this.natisniIzbranoDeloBrezCeneToolStripMenuItem.Size = new System.Drawing.Size(312, 22);
            this.natisniIzbranoDeloBrezCeneToolStripMenuItem.Text = "Natisni izbrano delo brez cene";
            this.natisniIzbranoDeloBrezCeneToolStripMenuItem.Click += new System.EventHandler(this.natisniIzbranoDeloBrezCeneToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(309, 6);
            // 
            // ausgewähltesKunstwerkOhnePreisDrucken25cmX27cmToolStripMenuItem
            // 
            this.ausgewähltesKunstwerkOhnePreisDrucken25cmX27cmToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ausgewähltesKunstwerkOhnePreisDrucken25cmX27cmToolStripMenuItem.Image")));
            this.ausgewähltesKunstwerkOhnePreisDrucken25cmX27cmToolStripMenuItem.Name = "ausgewähltesKunstwerkOhnePreisDrucken25cmX27cmToolStripMenuItem";
            this.ausgewähltesKunstwerkOhnePreisDrucken25cmX27cmToolStripMenuItem.Size = new System.Drawing.Size(312, 22);
            this.ausgewähltesKunstwerkOhnePreisDrucken25cmX27cmToolStripMenuItem.Text = "Natisni izbrano delo (25cm x 27cm)";
            this.ausgewähltesKunstwerkOhnePreisDrucken25cmX27cmToolStripMenuItem.Click += new System.EventHandler(this.ausgewähltesKunstwerkOhnePreisDrucken25cmX27cmToolStripMenuItem_Click);
            // 
            // x27ToolStripMenuItem
            // 
            this.x27ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("x27ToolStripMenuItem.Image")));
            this.x27ToolStripMenuItem.Name = "x27ToolStripMenuItem";
            this.x27ToolStripMenuItem.Size = new System.Drawing.Size(312, 22);
            this.x27ToolStripMenuItem.Text = "Natisni izbrano delo brez cene (25cm x 27cm)";
            this.x27ToolStripMenuItem.Click += new System.EventHandler(this.x27ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(309, 6);
            // 
            // bildDruckenToolStripMenuItem
            // 
            this.bildDruckenToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("bildDruckenToolStripMenuItem.Image")));
            this.bildDruckenToolStripMenuItem.Name = "bildDruckenToolStripMenuItem";
            this.bildDruckenToolStripMenuItem.Size = new System.Drawing.Size(312, 22);
            this.bildDruckenToolStripMenuItem.Click += new System.EventHandler(this.bildDruckenToolStripMenuItem_Click);
            // 
            // toolStripSplitButton3
            // 
            this.toolStripSplitButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.natisniSpisekSCenoToolStripMenuItem,
            this.natisniSpisekBrezCeneToolStripMenuItem});
            this.toolStripSplitButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton3.Image")));
            this.toolStripSplitButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton3.Name = "toolStripSplitButton3";
            this.toolStripSplitButton3.Size = new System.Drawing.Size(127, 36);
            this.toolStripSplitButton3.Text = "Natisni spisek";
            this.toolStripSplitButton3.ButtonClick += new System.EventHandler(this.toolStripSplitButton3_ButtonClick);
            // 
            // natisniSpisekSCenoToolStripMenuItem
            // 
            this.natisniSpisekSCenoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("natisniSpisekSCenoToolStripMenuItem.Image")));
            this.natisniSpisekSCenoToolStripMenuItem.Name = "natisniSpisekSCenoToolStripMenuItem";
            this.natisniSpisekSCenoToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.natisniSpisekSCenoToolStripMenuItem.Text = "Natisni spisek s ceno";
            this.natisniSpisekSCenoToolStripMenuItem.Click += new System.EventHandler(this.natisniSpisekSCenoToolStripMenuItem_Click);
            // 
            // natisniSpisekBrezCeneToolStripMenuItem
            // 
            this.natisniSpisekBrezCeneToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("natisniSpisekBrezCeneToolStripMenuItem.Image")));
            this.natisniSpisekBrezCeneToolStripMenuItem.Name = "natisniSpisekBrezCeneToolStripMenuItem";
            this.natisniSpisekBrezCeneToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.natisniSpisekBrezCeneToolStripMenuItem.Text = "Natisni spisek brez cene";
            this.natisniSpisekBrezCeneToolStripMenuItem.Click += new System.EventHandler(this.natisniSpisekBrezCeneToolStripMenuItem_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(115, 36);
            this.toolStripButton4.Text = "Natisni spisek";
            this.toolStripButton4.Visible = false;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton5.Text = "toolStripButton5";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // tbFilter
            // 
            this.tbFilter.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbFilter.BackColor = System.Drawing.SystemColors.Window;
            this.tbFilter.Name = "tbFilter";
            this.tbFilter.Size = new System.Drawing.Size(52, 39);
            this.tbFilter.Click += new System.EventHandler(this.toolStripTextBox1_Click);
            this.tbFilter.TextChanged += new System.EventHandler(this.toolStripTextBox1_TextChanged);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(36, 36);
            this.toolStripLabel1.Text = "Filter:";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(100, 36);
            this.toolStripButton7.Text = "Odpri sliko";
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(98, 36);
            this.toolStripButton6.Text = "Nastavitve";
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click_1);
            // 
            // splitDebug
            // 
            this.splitDebug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regenerateCompositsToolStripMenuItem,
            this.testWhiteToolStripMenuItem,
            this.toolStripSeparator5,
            this.transformDBToolStripMenuItem});
            this.splitDebug.Image = ((System.Drawing.Image)(resources.GetObject("splitDebug.Image")));
            this.splitDebug.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.splitDebug.Name = "splitDebug";
            this.splitDebug.Size = new System.Drawing.Size(90, 36);
            this.splitDebug.Text = "Debug";
            this.splitDebug.ButtonClick += new System.EventHandler(this.splitDebug_ButtonClick);
            // 
            // regenerateCompositsToolStripMenuItem
            // 
            this.regenerateCompositsToolStripMenuItem.Name = "regenerateCompositsToolStripMenuItem";
            this.regenerateCompositsToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.regenerateCompositsToolStripMenuItem.Text = "Regenerate Composits";
            this.regenerateCompositsToolStripMenuItem.Click += new System.EventHandler(this.regenerateCompositsToolStripMenuItem_Click);
            // 
            // testWhiteToolStripMenuItem
            // 
            this.testWhiteToolStripMenuItem.Name = "testWhiteToolStripMenuItem";
            this.testWhiteToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.testWhiteToolStripMenuItem.Text = "TestWhite";
            this.testWhiteToolStripMenuItem.Click += new System.EventHandler(this.testWhiteToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(190, 6);
            // 
            // transformDBToolStripMenuItem
            // 
            this.transformDBToolStripMenuItem.Name = "transformDBToolStripMenuItem";
            this.transformDBToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.transformDBToolStripMenuItem.Text = "Transform DB";
            this.transformDBToolStripMenuItem.Click += new System.EventHandler(this.transformDBToolStripMenuItem_Click);
            // 
            // toolStripSplitButton2
            // 
            this.toolStripSplitButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.urediTipToolStripMenuItem,
            this.urediMaterialToolStripMenuItem});
            this.toolStripSplitButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton2.Image")));
            this.toolStripSplitButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton2.Name = "toolStripSplitButton2";
            this.toolStripSplitButton2.Size = new System.Drawing.Size(125, 36);
            this.toolStripSplitButton2.Text = "Uredi šifrante";
            this.toolStripSplitButton2.ButtonClick += new System.EventHandler(this.toolStripSplitButton2_ButtonClick);
            // 
            // urediTipToolStripMenuItem
            // 
            this.urediTipToolStripMenuItem.Name = "urediTipToolStripMenuItem";
            this.urediTipToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.urediTipToolStripMenuItem.Text = "Uredi tip";
            this.urediTipToolStripMenuItem.Click += new System.EventHandler(this.urediTipToolStripMenuItem_Click);
            // 
            // urediMaterialToolStripMenuItem
            // 
            this.urediMaterialToolStripMenuItem.Name = "urediMaterialToolStripMenuItem";
            this.urediMaterialToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.urediMaterialToolStripMenuItem.Text = "Uredi material";
            this.urediMaterialToolStripMenuItem.Click += new System.EventHandler(this.urediMaterialToolStripMenuItem_Click);
            // 
            // pbComposition
            // 
            this.pbComposition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbComposition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbComposition.Location = new System.Drawing.Point(921, 423);
            this.pbComposition.Name = "pbComposition";
            this.pbComposition.Size = new System.Drawing.Size(213, 230);
            this.pbComposition.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbComposition.TabIndex = 4;
            this.pbComposition.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1146, 665);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.labelNr);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelDatumVnosa);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxOpis);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxNegativ);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxZaznamek);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxMaterial);
            this.Controls.Add(this.pbComposition);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Werkverzeichnis Slavko Oblak";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbComposition)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e) {
            if (dataGridView1.Columns[e.ColumnIndex] == dimenzijeDataGridViewTextBoxColumn) {
                // Ensure that the value is a string.
                Int32 x = (Int32)dataGridView1.Rows[e.RowIndex].Cells[dolzinaDataGridViewTextBoxColumn.Index].Value;
                Int32 y = (Int32)dataGridView1.Rows[e.RowIndex].Cells[visinaDataGridViewTextBoxColumn.Index].Value;
                Int32 z = (Int32)dataGridView1.Rows[e.RowIndex].Cells[sirinaDataGridViewTextBoxColumn.Index].Value;

                string s = Settings.FormatDimensions(x, y, z);

                e.Value = s;
            }
            else if (dataGridView1.Columns[e.ColumnIndex] == _cenaDataGridViewTextBoxColumn) {
                e.Value += " €";
            }
            else if (dataGridView1.Columns[e.ColumnIndex] == stumdelaDataGridViewTextBoxColumn) {
                e.Value += " - " + dataGridView1.Rows[e.RowIndex].Cells[tipDataGridViewTextBoxColumn.Index].Value;
            }
        }

        private void urediTipToolStripMenuItem_Click(object sender, EventArgs e) {
            RefItems items = new RefItems("Tipi", "ID", "ImeTipa", "tip");
            items.Fill();
            RefForm form = new RefForm(tipDataGridViewTextBoxColumn.HeaderText, items); 
            form.ShowDialog();

            dataGridView1.DataSource = Query.UmetnineJoined();
            this.LoadDetails();
        }

        private void urediMaterialToolStripMenuItem_Click(object sender, EventArgs e) {
            RefItems items = new RefItems("Materiali", "ID", "ImeMateriala", "material");
            items.Fill();
            RefForm form = new RefForm(materialDataGridViewTextBoxColumn.HeaderText, items);
            form.ShowDialog();

            dataGridView1.DataSource = Query.UmetnineJoined();
            this.LoadDetails();
        }

        private void toolStripSplitButton2_ButtonClick(object sender, EventArgs e)
        {
            this.toolStripSplitButton2.ShowDropDown();
        }

        private void natisniSpisekSCenoToolStripMenuItem_Click(object sender, EventArgs e) {
            string s = "";
            if (this.dataGridView1.SortedColumn != null) {
                s = this.dataGridView1.SortedColumn.DataPropertyName;
                if (this.dataGridView1.SortOrder == System.Windows.Forms.SortOrder.Ascending) {
                    s += " asc";
                }
                else if (this.dataGridView1.SortOrder == System.Windows.Forms.SortOrder.Descending) {
                    s += " desc";
                }
            }
            string f = this.ApplyFilter();
            PrintPreviewList ppl = new PrintPreviewList(f, true, s);
            ppl.ShowDialog(this);
        }

        private void natisniSpisekBrezCeneToolStripMenuItem_Click(object sender, EventArgs e) {
            string s = "";
            if (this.dataGridView1.SortedColumn != null) {
                s = this.dataGridView1.SortedColumn.DataPropertyName;
                if (this.dataGridView1.SortOrder == System.Windows.Forms.SortOrder.Ascending) {
                    s += " asc";
                }
                else if (this.dataGridView1.SortOrder == System.Windows.Forms.SortOrder.Descending) {
                    s += " desc";
                }
            }
            string f = this.ApplyFilter();
            PrintPreviewList ppl = new PrintPreviewList(f, false, s);
            ppl.ShowDialog(this);
        }

        private void toolStripSplitButton3_ButtonClick(object sender, EventArgs e) {
            this.toolStripSplitButton3.ShowDropDown();
        }
    }
}
