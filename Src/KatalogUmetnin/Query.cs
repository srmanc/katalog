﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;

namespace KatalogUmetnin
{
    public static class Query
    {
        public static DataRow ExecuteFirst(string sql, params object[] values) {
            DataView View = Execute(sql, values);
            return View.Table.Rows[0];
        }

        public static DataView Execute(string sql, params object[] values) {
            string connetionString = Settings.Get.DbConnectionString;
            /*
            for (int i = 0; i < values.Length; i++)
                if (values[i] is string)
                    values[i] = "'" + values[i] + "'";
            */
            {
                CultureLock cl = new CultureLock();
                sql = String.Format(sql, values);
                if (Settings.Get.DebugMode)
                    MessageBox.Show(sql);
                cl.Unlock();
            }            
            OleDbConnection connection = new OleDbConnection(connetionString);
            OleDbDataAdapter dataadapter = new OleDbDataAdapter(sql, connection);
            DataTable dt = new DataTable();
            connection.Open();
            dataadapter.Fill(dt);
            connection.Close();
            return new DataView(dt);
        }

        // --- Prepared queries --- //
        private const string sqlSelectIdentity = "SELECT @@IDENTITY";
        private const string sqlSelectUmetnina = "SELECT * FROM Umetnine WHERE {0} = {1}";
        private const string sqlSelectUmetnineJoined = @"SELECT Umetnine.*,Tipi.ImeTipa_{0},Materiali.ImeMateriala_{0} FROM (Umetnine 
            INNER JOIN Tipi ON Umetnine.tip = Tipi.ID) 
            INNER JOIN Materiali ON Umetnine.material = Materiali.ID 
            ORDER BY Umetnine.st_um_dela ASC";
        private const string sqlSelectUmetnineAppear = @"SELECT * FROM Umetnine WHERE {0} = {1} ORDER BY st_um_dela ASC";
        private const string sqlSelectUmetninaJoinedUnfinished = @"SELECT Umetnine.*,Tipi.ImeTipa_{0},Materiali.ImeMateriala_{0} FROM (Umetnine 
            INNER JOIN Tipi ON Umetnine.tip = Tipi.ID) 
            INNER JOIN Materiali ON Umetnine.material = Materiali.ID 
            WHERE";
        private const string sqlInsertUmetnina = @"INSERT INTO Umetnine 
            (Nr, st_um_dela, tip, naslov_en, naslov_de, naslov_si, leto_izdelave, material, dolzina, visina, sirina, zaznamek, st_negativ, cena, cena_dem, st_slik, opis, ID, datum, izkupicek) VALUES 
            ({0}, '{1}', {2}, '{3}', '{4}', '{5}', '{6}', {7}, {8}, {9}, {10}, '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}')";
        private const string sqlUpdateUmetnina = @"UPDATE Umetnine SET
            st_um_dela = '{1}', tip = {2}, naslov_en = '{3}', naslov_de = '{4}', naslov_si = '{5}', leto_izdelave = '{6}', material = {7}, dolzina = {8},
            visina = {9}, sirina = {10}, zaznamek = '{11}', st_negativ = '{12}', cena = '{13}', cena_dem = '{14}', st_slik = '{15}',
            opis = '{16}', ID = '{17}', datum = '{18}', izkupicek = '{19}' WHERE Nr = {0}";
        private const string sqlDeleteUmetnina = "DELETE FROM Umetnine WHERE Nr = {0}";
        private const string sqlSelectSlike = "SELECT * FROM Slike WHERE Nr = {0}";
        private const string sqlInsertSlika = @"INSERT INTO Slike 
            (ID, Nr, Ime, VrstniRed, Zoom, X, Y, Visible, Rotation) VALUES
            ({0}, {1}, '{2}', {3}, {4}, {5}, {6}, {7}, {8})";
        private const string sqlUpdateSlika = @"UPDATE Slike SET 
            Nr = {1}, Ime = '{2}', VrstniRed = {3}, Zoom = {4}, X = {5}, Y = {6}, Visible = {7}, Rotation = {8} WHERE ID = {0}";
        private const string sqlDeleteSlika = @"DELETE FROM Slike WHERE ID = {0}";
        private const string sqlSelectMateriali = "SELECT * FROM Materiali ORDER BY ID ASC";
        private const string sqlSelectTipi = "SELECT * FROM Tipi ORDER BY ID ASC";
        private const string sqlSelectRef = "SELECT * FROM {0} ORDER BY ID ASC";
        private const string sqlInsertRef = "INSERT INTO {0} (ID, {1}_en, {1}_de, {1}_si) VALUES ({2}, '{3}', '{4}', '{5}')";
        private const string sqlUpdateRef = "UPDATE {0} SET {1}_en = '{3}', {1}_de = '{4}', {1}_si = '{5}' WHERE ID = {2}";
        private const string sqlDeleteRef = "DELETE FROM {0} WHERE ID = {1}";
        private const string sqlGetMaxIDFromTable = "SELECT MAX({1}) FROM {0}";

        // --- Prepared methods --- //

        public static DataRow Umetnina(string key, object value) {
            return ExecuteFirst(sqlSelectUmetnina, key, value);
        }

        public static DataView UmetnineJoined() {
            string sql = String.Format(sqlSelectUmetnineJoined, Settings.Get.LanguageShort);
            return Execute(sql);
        }

        public static DataView UmetnineAppear(string JoinField, string JoinValue) {
            string sql = String.Format(sqlSelectUmetnineAppear, JoinField, JoinValue);
            return Execute(sql);
        }        

        public static DataView UmetnineLangJoined(string key, string lang, params object[] values) {
            string sql = sqlSelectUmetninaJoinedUnfinished;
            for (int i = 0; i < values.Length; i++) {
                if (i > 0)
                    sql += " OR";
                sql += " " + key + " = '" + values[i] + "'";
            }
            return Execute(sql, lang);
        }

        public static DataRow InsertUmetnina(params object[] values) {
            Execute(sqlInsertUmetnina, values);
            return ExecuteFirst(sqlSelectIdentity);
        }

        public static void UpdateUmetnina(params object[] values) {
            Execute(sqlUpdateUmetnina, values);
        }

        public static void DeleteUmetnina(int id) {
            Execute(sqlDeleteUmetnina, id);
        }

        public static DataView Slike(int id) {
            return Execute(sqlSelectSlike, id);
        }

        public static DataRow InsertSlika(params object[] values) {
            Execute(sqlInsertSlika, values);
            return ExecuteFirst(sqlSelectIdentity);
        }

        public static void UpdateSlika(params object[] values) {
            Execute(sqlUpdateSlika, values);
        }

        public static void DeleteSlika(int id) {
            Execute(sqlDeleteSlika, id);
        }

        public static DataView Tipi() {
            return Execute(sqlSelectTipi);
        }

        public static DataView Materiali() {
            return Execute(sqlSelectMateriali);
        }

        public static DataView Refs(string TableName) {
            return Execute(sqlSelectRef, TableName);
        }

        public static DataRow InsertRef(params object[] values) {
            Execute(sqlInsertRef, values);
            return ExecuteFirst(sqlSelectIdentity);
        }

        public static DataRow UpdateRef(params object[] values) {
            Execute(sqlUpdateRef, values);
            return ExecuteFirst(sqlSelectIdentity);
        }

        public static void DeleteRef(params object[] values) {
            Execute(sqlDeleteRef, values);
        }

        public static int AutoIncID(string TableName, string AutoIncColumn) {
            DataRow r = ExecuteFirst(sqlGetMaxIDFromTable, TableName, AutoIncColumn);
            return (int)r[0] + 1;
        }

        public static object SelectedItem(DataView t, string n, object o) {
            for (int i = 0; i < t.Table.Rows.Count; i++)
                if (t.Table.Rows[i][n].ToString() == o.ToString())
                    return i;
            return -1;
        }

        public static ComboBox.ObjectCollection FillItems(ComboBox.ObjectCollection items, DataView t, string n, string id) {
            for (int i = 0; i < t.Table.Rows.Count; i++)
                items.Add(new QueryItem(t.Table.Rows[i][n], t.Table.Rows[i][id]));
            return items;
        }
    }

    internal class QueryItem
    {
        private object name;
        private object id;

        public QueryItem(object name, object id) {
            this.name = name;
            this.id = id;
        }

        public override string ToString() {
            return name.ToString();
        }

        public object ID() {
            return id;
        }
    }
}
