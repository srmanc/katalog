﻿using KatalogUmetnin.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace KatalogUmetnin
{
	public class SettingsForm : System.Windows.Forms.Form
	{
		private string initLang = "";

		private IContainer components = null;

		private System.Windows.Forms.TextBox textBox1;

		private System.Windows.Forms.Label label1;

		private System.Windows.Forms.Label label2;

		private System.Windows.Forms.ComboBox comboBox1;

		private System.Windows.Forms.Button button3;

		private System.Windows.Forms.Button button2;

		private System.Windows.Forms.LinkLabel linkLabel1;

		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;

		private System.Windows.Forms.Label label3;
        private Label label4;
        private CheckBox checkPrice;
        private System.Windows.Forms.LinkLabel linkLabel2;

		public SettingsForm()
		{
			this.InitializeComponent();
			this.label3.Text = "";
            Settings.SetTexts(this);
		}

		private void SettingsForm_Load(object sender, EventArgs e)
		{
			this.comboBox1.Items.Clear();
			string[] languages = Settings.Languages;
			for (int k = 0; k < languages.Length; k++)
			{
				string i = languages[k];
				this.comboBox1.Items.Add(i);
			}
			this.comboBox1.SelectedIndex = 0;
			for (int j = 0; j < this.comboBox1.Items.Count; j++)
			{
				if (this.comboBox1.Items[j].ToString() == Settings.Get.Language)
				{
					this.comboBox1.SelectedIndex = j;
					break;
				}
			}
			this.initLang = Settings.Get.Language;
			this.textBox1.Text = Settings.Get.StoragePath;
            this.checkPrice.Checked = Settings.Get.ShowPrice;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			if (this.ValidateBeforeSave())
			{
                Settings.Get.StoragePath = textBox1.Text;
                Settings.Get.Language = comboBox1.Text;
                Settings.Get.ShowPrice = checkPrice.Checked;
				if (this.initLang != Settings.Get.Language)
				{
					//System.Windows.Forms.MessageBox.Show(MessageStrings.SettingsLanguageChangeText);
                    Form1 f = (Form1)base.Owner;
                    Settings.SetCulture();
                    if (f != null) {
                        Settings.SetTexts(f);
                        f.Text += " " + Program.Version;
                    }
                }
				base.DialogResult = System.Windows.Forms.DialogResult.OK;
			}
		}

		public void SetWarningText(string txt)
		{
			this.label3.Text = txt;
		}

		private bool ValidateBeforeSave()
		{
			bool result;
			if (this.textBox1.Text.Trim() == "")
			{
				System.Windows.Forms.MessageBox.Show(MessageStrings.SettingsStorageNotSetText);
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		private void linkLabel1_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			this.folderBrowserDialog1.SelectedPath = this.textBox1.Text.Trim();
			if (this.folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				this.textBox1.Text = this.folderBrowserDialog1.SelectedPath;
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			base.DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		private void linkLabel2_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			Form1 f = (Form1)base.Owner;
			f.EnableDebug();
			base.DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.checkPrice = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(318, 12);
            this.textBox1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(464, 31);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(24, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(282, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Pot do imenika shrambe slik:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(30, 67);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(276, 35);
            this.label2.TabIndex = 3;
            this.label2.Text = "Jezik:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(318, 62);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(270, 33);
            this.comboBox1.TabIndex = 4;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(774, 140);
            this.button3.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(204, 62);
            this.button3.TabIndex = 6;
            this.button3.Text = "Prekliči";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(562, 140);
            this.button2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(200, 62);
            this.button2.TabIndex = 5;
            this.button2.Text = "Potrdi";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.Location = new System.Drawing.Point(798, 17);
            this.linkLabel1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(180, 25);
            this.linkLabel1.TabIndex = 7;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Prebrskaj";
            this.linkLabel1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(24, 140);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(526, 62);
            this.label3.TabIndex = 8;
            this.label3.Text = "label3";
            // 
            // linkLabel2
            // 
            this.linkLabel2.Location = new System.Drawing.Point(666, 77);
            this.linkLabel2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(312, 25);
            this.linkLabel2.TabIndex = 9;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "ENABLE DEBUG";
            this.linkLabel2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.linkLabel2.Visible = false;
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(30, 117);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(276, 35);
            this.label4.TabIndex = 10;
            this.label4.Text = "Pokaži ceno:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // checkPrice
            // 
            this.checkPrice.AutoSize = true;
            this.checkPrice.Location = new System.Drawing.Point(318, 123);
            this.checkPrice.Name = "checkPrice";
            this.checkPrice.Size = new System.Drawing.Size(15, 14);
            this.checkPrice.TabIndex = 11;
            this.checkPrice.UseVisualStyleBackColor = true;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 225);
            this.Controls.Add(this.checkPrice);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "SettingsForm";
            this.Text = "Nastavitve";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
	}
}
