using KatalogUmetnin.ImagePresentation;
using KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters;
using KatalogUmetnin.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.OleDb;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Data;
using System.Globalization;
using System.Threading;

namespace KatalogUmetnin
{
	public class Umetnisko_delo : System.Windows.Forms.Form
	{
		private int Nr = -1;

		private IContainer components = null;

		private System.Windows.Forms.PictureBox pictureBox1;

		private System.Windows.Forms.TextBox textBoxstDela;

		private System.Windows.Forms.Label label1;

		private System.Windows.Forms.Label labelNaslov;

		private System.Windows.Forms.TextBox textBoxNaslov;

		private System.Windows.Forms.Label label3;

		private System.Windows.Forms.TextBox textBoxLeto;

        private System.Windows.Forms.Label label4;

		private System.Windows.Forms.Label label5;

		private System.Windows.Forms.TextBox textBoxDolzina;

		private System.Windows.Forms.Label label6;

		private System.Windows.Forms.TextBox textBoxZaznamek;

		private System.Windows.Forms.Label label7;

		private System.Windows.Forms.TextBox textBoxNegativ;

		private System.Windows.Forms.Label label8;

		private System.Windows.Forms.TextBox textBoxCenaEUR;

		private System.Windows.Forms.Label label9;

		private System.Windows.Forms.TextBox textBoxStSlik;

		private System.Windows.Forms.Label label10;

		private System.Windows.Forms.TextBox textBoxOpis;

		private System.Windows.Forms.Label label11;

		private System.Windows.Forms.TextBox textBoxID;

		private System.Windows.Forms.Label label12;

		private System.Windows.Forms.DateTimePicker dateTimePicker1;

		private System.Windows.Forms.Label label13;

		private System.Windows.Forms.TextBox textBoxIzkupicek;

		private System.Windows.Forms.Button button1;

		private System.Windows.Forms.Button button2;

		private System.Windows.Forms.Label label14;

		private System.Windows.Forms.TextBox textBoxCenaDM;

		private UmetnineTableAdapter umetnineTableAdapter1;

		private System.Windows.Forms.ErrorProvider errorProvider1;

		private ImagePresentationUC imagePresentationUC1;

		private System.Windows.Forms.PictureBox pictureBox2;

		private System.Windows.Forms.Panel panel1;
        private TextBox textBoxSirina;
        private TextBox textBoxVisina;
        private ComboBox comboBoxMaterial;
        private ComboBox comboBoxTip;
        private Label label15;
        private Label label16;
        private Label labelTitel;
        private TextBox textBoxTitel;
        private Label labelTitle;
        private TextBox textBoxTitle;
        private CheckBox checkPrice;
        private Button buttonFix;
        private SlikeTableAdapter slikeTableAdapter1;

        public int Nmr {
            get {
                return this.Nr;
            }
        }

		public bool IsNew
		{
			get
			{
				return this.Nr == -1;
			}
		}

		public Umetnisko_delo()
		{
			this.InitializeComponent();
            Settings.SetTexts(this);

			this.textBoxCenaEUR.Text = (this.textBoxIzkupicek.Text = "0,00 €");
			this.textBoxCenaDM.Text = "0,00 DM";
            this.textBoxSirina.Text = "0";
            this.textBoxDolzina.Text = "0";
            this.textBoxVisina.Text = "0";

            FillReferences(null);

            this.imagePresentationUC1.Anchor = this.imagePresentationUC1.Anchor | AnchorStyles.Bottom;
            this.pictureBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            this.panel1.Visible = Settings.Get.ShowPrice;
        }        

		public Umetnisko_delo(int Nr)
		{
            this.Nr = Nr;
			this.InitializeComponent();
            Settings.SetTexts(this);

            DataView s = Query.Slike(Nr);
            DataRow u = Query.Umetnina("Nr", Nr);
            FillReferences(u);
            this.textBoxstDela.Text = u["st_um_dela"].ToString();
			this.textBoxNaslov.Text = u["naslov_si"].ToString();
            this.textBoxTitel.Text = u["naslov_de"].ToString();
            this.textBoxTitle.Text = u["naslov_en"].ToString();
            this.textBoxLeto.Text = u["leto_izdelave"].ToString();
			this.textBoxDolzina.Text = u["dolzina"].ToString();
            this.textBoxVisina.Text = u["visina"].ToString();
            this.textBoxSirina.Text = u["sirina"].ToString();
			this.textBoxZaznamek.Text = u["zaznamek"].ToString();
			this.textBoxNegativ.Text = u["st_negativ"].ToString();
			this.textBoxCenaEUR.Text = u["cena"].ToString();
			ValidateMoneyTextBox(this.textBoxCenaEUR, "€");
			this.textBoxCenaDM.Text = u["cena_dem"].ToString();
			ValidateMoneyTextBox(this.textBoxCenaDM, "DM");
			this.textBoxStSlik.Text = u["st_slik"].ToString();
			this.textBoxOpis.Text = u["opis"].ToString();
			this.textBoxID.Text = u["ID"].ToString();
			this.dateTimePicker1.Value = TransformDB.StringToDateTime(u["datum"].ToString());
			this.textBoxIzkupicek.Text = u["izkupicek"].ToString();
			ValidateMoneyTextBox(this.textBoxIzkupicek, "€");
            
			this.imagePresentationUC1.LoadData(Nr, s, u["opis"].ToString());
			this.textBoxOpis.TextChanged += new EventHandler(this.textBoxOpis_TextChanged);

            this.imagePresentationUC1.Anchor = this.imagePresentationUC1.Anchor | AnchorStyles.Bottom;
            this.pictureBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left;            
            this.checkPrice.Checked = Settings.Get.ShowPrice;            
            this.panel1.Visible = Settings.Get.ShowPrice;
        }

        private void FillReferences(DataRow umetnina)
        {
            DataView m = Query.Materiali();
            DataView t = Query.Tipi();

            Query.FillItems(this.comboBoxMaterial.Items, m, Settings.GetLanguageExtType("ImeMateriala"), "ID");
            Query.FillItems(this.comboBoxTip.Items, t, Settings.GetLanguageExtType("ImeTipa"), "ID");
            if (umetnina != null)
            {
                this.comboBoxMaterial.SelectedIndex = (int)Query.SelectedItem(m, "ID", umetnina["material"]);
                this.comboBoxTip.SelectedIndex = (int)Query.SelectedItem(t, "ID", umetnina["tip"]);
            }
            else {
                this.comboBoxMaterial.SelectedIndex = 0;
                this.comboBoxTip.SelectedIndex = 0;
            }
        }

        private void textBoxOpis_TextChanged(object sender, EventArgs e)
		{
			this.imagePresentationUC1.UpdateOpis(this.textBoxOpis.Text);
		}

		private void button1_Click(object sender, EventArgs e)
		{
			this.Save(true);
		}

		public void Save(bool closeAfterSave = true)
		{
			if (this.ValidateFields())
			{
				this.SetMoneyTB(this.textBoxCenaEUR);
				this.SetMoneyTB(this.textBoxCenaDM);
				this.SetMoneyTB(this.textBoxIzkupicek);
                int tip = (int)(comboBoxTip.SelectedItem as QueryItem).ID();
                int material = (int)(comboBoxMaterial.SelectedItem as QueryItem).ID();
				if (this.IsNew)
				{
                    int lNr = Query.AutoIncID("Umetnine", "Nr");
                    CultureLock cl = new CultureLock();
                    DataRow NewRow = Query.InsertUmetnina(lNr, this.textBoxstDela.Text, tip, this.textBoxTitle.Text, this.textBoxTitel.Text, this.textBoxNaslov.Text, this.textBoxLeto.Text, material, Int32.Parse(this.textBoxDolzina.Text), Int32.Parse(this.textBoxVisina.Text), Int32.Parse(this.textBoxSirina.Text), this.textBoxZaznamek.Text, this.textBoxNegativ.Text, new decimal?(Convert.ToDecimal(this.textBoxCenaEUR.Text.Replace(" €", ""))), new decimal?(0), this.textBoxStSlik.Text, this.textBoxOpis.Text, this.textBoxID.Text, new DateTime?(this.dateTimePicker1.Value), new decimal?(0));
                    cl.Unlock();
                    this.Nr = lNr;
                    if (!closeAfterSave)
					{                        
						this.imagePresentationUC1.LoadData(this.Nr);
					}
				}
				else
				{
                    CultureLock cl = new CultureLock();    
                    Query.UpdateUmetnina(this.Nr, this.textBoxstDela.Text, tip, this.textBoxTitle.Text, this.textBoxTitel.Text, this.textBoxNaslov.Text, this.textBoxLeto.Text, material, Int32.Parse(this.textBoxDolzina.Text), Int32.Parse(this.textBoxVisina.Text), Int32.Parse(this.textBoxSirina.Text), this.textBoxZaznamek.Text, this.textBoxNegativ.Text, new decimal?(Convert.ToDecimal(this.textBoxCenaEUR.Text.Replace(" €", ""))), new decimal?(0), this.textBoxStSlik.Text, this.textBoxOpis.Text, this.textBoxID.Text, new DateTime?(this.dateTimePicker1.Value), new decimal?(0));
                    cl.Unlock();
                    this.UpdateSlike();                    
				}
				if (closeAfterSave)
				{                    
					this.imagePresentationUC1.ExportComposition();
					base.DialogResult = System.Windows.Forms.DialogResult.OK;
				}
			}
		}

		private void UpdateSlike()
		{
			DataView slike = Query.Slike(this.Nr);
            List<string> updatedItems = new List<string>();
			for (int idx = 0; idx < slike.Count; idx++)
			{
				//while (enumerator.MoveNext())
				{
                    //KatalogUmetnin_dbDataSet.SlikeRow s = enumerator.Current;
                    DataRow s = slike[idx].Row;
                    int ID = (int)s["ID"];
                    IEnumerable<CanvasImg> matchingItems = from p in this.imagePresentationUC1.CanvasImages
                    //where p.FileName.ToLower() == s.Ime.ToLower()
                    where p.FileName.ToLower() == ((string)s["Ime"]).ToLower()
                    select p;                    
                    if (matchingItems.Count<CanvasImg>() == 0)
					{
                        //s.Delete();                        
                        Query.DeleteSlika(ID);
					}
					else
					{
						CanvasImg c = matchingItems.First<CanvasImg>();
                        /*s.X = c.Location.X;
						s.Y = c.Location.Y;
						s.Visible = c.Visible;
						s.VrstniRed = c.Index;
						s.Zoom = c.Zoom;
                        s.Rotation = c.Rotation;*/                         
                        Query.UpdateSlika(ID, this.Nr, c.FileName, c.Index, c.Zoom, c.Location.X, c.Location.Y, c.Visible, c.Rotation);
                        updatedItems.Add(c.FileName);                        
					}
				}
			}
			IEnumerable<CanvasImg> i = from p in this.imagePresentationUC1.CanvasImages
			where !updatedItems.Contains(p.FileName)
			select p;
			foreach (CanvasImg s2 in i)
			{
                //ds.Slike.AddSlikeRow(this.Nr, s2.FileName, s2.Index, s2.Zoom, s2.Location.X, s2.Location.Y, s2.Visible, s2.Rotation);
                int ID = Query.AutoIncID("Slike", "ID");
                Query.InsertSlika(ID, this.Nr, s2.FileName, s2.Index, s2.Zoom, s2.Location.X, s2.Location.Y, s2.Visible, s2.Rotation);
			}
			//this.slikeTableAdapter1.Update(ds.Slike);                                    
		}
		
		private bool ValidateFields()
		{
            CultureLock cl = new CultureLock();
			decimal d = -1m;
			bool result;
            string sPrice = this.textBoxCenaEUR.Text.Replace(" €", "").Trim();
            if (sPrice.Equals("0,00")) {
                sPrice = "0";
                this.textBoxCenaEUR.Text = sPrice;
            }

            if (!decimal.TryParse(sPrice, out d))
			{
				System.Windows.Forms.MessageBox.Show(MessageStrings.CenaValidationText + " (" + sPrice + ") ",
                    MessageStrings.CenaValidationTitle, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Hand);
				result = false;
			}
			else if (!decimal.TryParse(this.textBoxDolzina.Text, out d) || 
                !decimal.TryParse(this.textBoxVisina.Text, out d) || 
                !decimal.TryParse(this.textBoxSirina.Text, out d)) {
                System.Windows.Forms.MessageBox.Show(MessageStrings.DimenzijeValidationText, MessageStrings.DimenzijeValidationTitle, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Hand);
                result = false;
            }
			else
			{
				result = true;
			}
            cl.Unlock();
			return result;
		}

		private void SetMoneyTB(System.Windows.Forms.TextBox tb)
		{
			if (tb.Text.Trim() == "")
			{
				tb.Text = "0";
			}
		}

		private void textBoxCenaEUR_Validating(object sender, CancelEventArgs e)
		{
			this.ValidateMoneyTextBox(this.textBoxCenaEUR, "€");
		}

		private void ValidateMoneyTextBox(System.Windows.Forms.TextBox t, string currency)
		{
            CultureLock cl = new CultureLock();

            decimal amount = 0m;
			if (t.Text == "")
			{
				this.errorProvider1.SetError(t, MessageStrings.MoneyValidation0);
				t.Text = "0 " + currency;
			}
			else if (!decimal.TryParse(t.Text.Replace(" " + currency, ""), out amount))
			{
				this.errorProvider1.SetError(t, MessageStrings.InvalidValue);
				t.Text = "0,00 " + currency;
			}
			else
			{
				this.errorProvider1.SetError(t, "");
				t.Text = Convert.ToDecimal(t.Text.Replace(" " + currency, "")).ToString("F2") + " " + currency;
			}

            cl.Unlock();
        }

		private void textBoxCenaDM_Validating(object sender, CancelEventArgs e)
		{
			this.ValidateMoneyTextBox(this.textBoxCenaDM, "DM");
		}

		private void textBoxIzkupicek_Validating(object sender, CancelEventArgs e)
		{
			this.ValidateMoneyTextBox(this.textBoxIzkupicek, "€");
		}

		private void Umetnisko_delo_Load(object sender, EventArgs e)
		{            
            imagePresentationUC1.RealignComponents();            
		}

		private void button2_Click(object sender, EventArgs e)
		{
			base.DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}		

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Umetnisko_delo));
            this.textBoxstDela = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelNaslov = new System.Windows.Forms.Label();
            this.textBoxNaslov = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxLeto = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxDolzina = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxZaznamek = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxNegativ = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.textBoxCenaEUR = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxIzkupicek = new System.Windows.Forms.TextBox();
            this.textBoxCenaDM = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxStSlik = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxOpis = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxVisina = new System.Windows.Forms.TextBox();
            this.textBoxSirina = new System.Windows.Forms.TextBox();
            this.comboBoxMaterial = new System.Windows.Forms.ComboBox();
            this.comboBoxTip = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelTitle = new System.Windows.Forms.Label();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.labelTitel = new System.Windows.Forms.Label();
            this.textBoxTitel = new System.Windows.Forms.TextBox();
            this.imagePresentationUC1 = new KatalogUmetnin.ImagePresentation.ImagePresentationUC();
            this.umetnineTableAdapter1 = new KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters.UmetnineTableAdapter();
            this.slikeTableAdapter1 = new KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters.SlikeTableAdapter();
            this.checkPrice = new System.Windows.Forms.CheckBox();
            this.buttonFix = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxstDela
            // 
            this.textBoxstDela.Location = new System.Drawing.Point(211, 9);
            this.textBoxstDela.Name = "textBoxstDela";
            this.textBoxstDela.Size = new System.Drawing.Size(61, 20);
            this.textBoxstDela.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(70, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Št. umetniškega dela";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelNaslov
            // 
            this.labelNaslov.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.labelNaslov.Location = new System.Drawing.Point(286, 12);
            this.labelNaslov.Name = "labelNaslov";
            this.labelNaslov.Size = new System.Drawing.Size(61, 13);
            this.labelNaslov.TabIndex = 5;
            this.labelNaslov.Text = "Naslov (si)";
            this.labelNaslov.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxNaslov
            // 
            this.textBoxNaslov.Location = new System.Drawing.Point(354, 9);
            this.textBoxNaslov.Name = "textBoxNaslov";
            this.textBoxNaslov.Size = new System.Drawing.Size(188, 20);
            this.textBoxNaslov.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(718, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Leto izdelave";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxLeto
            // 
            this.textBoxLeto.Location = new System.Drawing.Point(849, 38);
            this.textBoxLeto.Name = "textBoxLeto";
            this.textBoxLeto.Size = new System.Drawing.Size(45, 20);
            this.textBoxLeto.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(85, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Material";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(267, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Velikost";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxDolzina
            // 
            this.textBoxDolzina.Location = new System.Drawing.Point(353, 61);
            this.textBoxDolzina.Name = "textBoxDolzina";
            this.textBoxDolzina.Size = new System.Drawing.Size(34, 20);
            this.textBoxDolzina.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(559, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Zaznamek";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxZaznamek
            // 
            this.textBoxZaznamek.Location = new System.Drawing.Point(620, 35);
            this.textBoxZaznamek.Name = "textBoxZaznamek";
            this.textBoxZaznamek.Size = new System.Drawing.Size(88, 20);
            this.textBoxZaznamek.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(48, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 17);
            this.label7.TabIndex = 15;
            this.label7.Text = "Številka negativa";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxNegativ
            // 
            this.textBoxNegativ.Location = new System.Drawing.Point(177, 61);
            this.textBoxNegativ.Name = "textBoxNegativ";
            this.textBoxNegativ.Size = new System.Drawing.Size(95, 20);
            this.textBoxNegativ.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(30, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "Cena €";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.textBoxCenaEUR);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.textBoxIzkupicek);
            this.panel1.Controls.Add(this.textBoxCenaDM);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Location = new System.Drawing.Point(14, 87);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(694, 40);
            this.panel1.TabIndex = 33;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(3, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(32, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 34;
            this.pictureBox2.TabStop = false;
            // 
            // textBoxCenaEUR
            // 
            this.textBoxCenaEUR.Location = new System.Drawing.Point(99, 9);
            this.textBoxCenaEUR.Name = "textBoxCenaEUR";
            this.textBoxCenaEUR.Size = new System.Drawing.Size(80, 20);
            this.textBoxCenaEUR.TabIndex = 16;
            this.textBoxCenaEUR.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxCenaEUR_Validating);
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(439, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(150, 17);
            this.label14.TabIndex = 31;
            this.label14.Text = "Cena DM";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label14.Visible = false;
            // 
            // textBoxIzkupicek
            // 
            this.textBoxIzkupicek.Location = new System.Drawing.Point(278, 9);
            this.textBoxIzkupicek.Name = "textBoxIzkupicek";
            this.textBoxIzkupicek.Size = new System.Drawing.Size(80, 20);
            this.textBoxIzkupicek.TabIndex = 26;
            this.textBoxIzkupicek.Visible = false;
            this.textBoxIzkupicek.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxIzkupicek_Validating);
            // 
            // textBoxCenaDM
            // 
            this.textBoxCenaDM.Location = new System.Drawing.Point(595, 9);
            this.textBoxCenaDM.Name = "textBoxCenaDM";
            this.textBoxCenaDM.Size = new System.Drawing.Size(85, 20);
            this.textBoxCenaDM.TabIndex = 30;
            this.textBoxCenaDM.Visible = false;
            this.textBoxCenaDM.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxCenaDM_Validating);
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(185, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 17);
            this.label13.TabIndex = 27;
            this.label13.Text = "Izkupiček";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label13.Visible = false;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(434, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Število slik";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // textBoxStSlik
            // 
            this.textBoxStSlik.Location = new System.Drawing.Point(497, 35);
            this.textBoxStSlik.Name = "textBoxStSlik";
            this.textBoxStSlik.Size = new System.Drawing.Size(45, 20);
            this.textBoxStSlik.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(769, 64);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Opis";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxOpis
            // 
            this.textBoxOpis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOpis.Location = new System.Drawing.Point(849, 64);
            this.textBoxOpis.Multiline = true;
            this.textBoxOpis.Name = "textBoxOpis";
            this.textBoxOpis.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxOpis.Size = new System.Drawing.Size(188, 66);
            this.textBoxOpis.TabIndex = 20;
            this.textBoxOpis.Text = " ";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(906, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "ID";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(977, 38);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(60, 20);
            this.textBoxID.TabIndex = 22;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(529, 64);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(87, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Datum";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(620, 61);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(88, 20);
            this.dateTimePicker1.TabIndex = 25;
            this.dateTimePicker1.Visible = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(944, 797);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(93, 33);
            this.button2.TabIndex = 29;
            this.button2.Text = "Prekliči";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(845, 797);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 33);
            this.button1.TabIndex = 28;
            this.button1.Text = "Shrani";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxVisina
            // 
            this.textBoxVisina.Location = new System.Drawing.Point(393, 61);
            this.textBoxVisina.Name = "textBoxVisina";
            this.textBoxVisina.Size = new System.Drawing.Size(38, 20);
            this.textBoxVisina.TabIndex = 34;
            // 
            // textBoxSirina
            // 
            this.textBoxSirina.Location = new System.Drawing.Point(437, 61);
            this.textBoxSirina.Name = "textBoxSirina";
            this.textBoxSirina.Size = new System.Drawing.Size(36, 20);
            this.textBoxSirina.TabIndex = 35;
            // 
            // comboBoxMaterial
            // 
            this.comboBoxMaterial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMaterial.FormattingEnabled = true;
            this.comboBoxMaterial.Location = new System.Drawing.Point(177, 35);
            this.comboBoxMaterial.Name = "comboBoxMaterial";
            this.comboBoxMaterial.Size = new System.Drawing.Size(95, 21);
            this.comboBoxMaterial.TabIndex = 36;
            // 
            // comboBoxTip
            // 
            this.comboBoxTip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTip.FormattingEnabled = true;
            this.comboBoxTip.Location = new System.Drawing.Point(354, 35);
            this.comboBoxTip.Name = "comboBoxTip";
            this.comboBoxTip.Size = new System.Drawing.Size(65, 21);
            this.comboBoxTip.TabIndex = 38;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(283, 38);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(64, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Tip";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(479, 64);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(21, 17);
            this.label16.TabIndex = 39;
            this.label16.Text = "cm";
            // 
            // labelTitle
            // 
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.labelTitle.Location = new System.Drawing.Point(558, 12);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(56, 17);
            this.labelTitle.TabIndex = 41;
            this.labelTitle.Text = "Title (en)";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(620, 9);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(167, 20);
            this.textBoxTitle.TabIndex = 40;
            // 
            // labelTitel
            // 
            this.labelTitel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.labelTitel.Location = new System.Drawing.Point(793, 12);
            this.labelTitel.Name = "labelTitel";
            this.labelTitel.Size = new System.Drawing.Size(49, 13);
            this.labelTitel.TabIndex = 43;
            this.labelTitel.Text = "Titel (de)";
            this.labelTitel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxTitel
            // 
            this.textBoxTitel.Location = new System.Drawing.Point(849, 9);
            this.textBoxTitel.Name = "textBoxTitel";
            this.textBoxTitel.Size = new System.Drawing.Size(188, 20);
            this.textBoxTitel.TabIndex = 42;
            // 
            // imagePresentationUC1
            // 
            this.imagePresentationUC1.ID = 0;
            this.imagePresentationUC1.Location = new System.Drawing.Point(12, 150);
            this.imagePresentationUC1.Name = "imagePresentationUC1";
            this.imagePresentationUC1.Opis = null;
            this.imagePresentationUC1.Size = new System.Drawing.Size(1029, 634);
            this.imagePresentationUC1.Slike = null;
            this.imagePresentationUC1.TabIndex = 32;
            // 
            // umetnineTableAdapter1
            // 
            this.umetnineTableAdapter1.ClearBeforeFill = true;
            // 
            // slikeTableAdapter1
            // 
            this.slikeTableAdapter1.ClearBeforeFill = true;
            // 
            // checkPrice
            // 
            this.checkPrice.AutoSize = true;
            this.checkPrice.Location = new System.Drawing.Point(620, 65);
            this.checkPrice.Name = "checkPrice";
            this.checkPrice.Size = new System.Drawing.Size(15, 14);
            this.checkPrice.TabIndex = 44;
            this.checkPrice.UseVisualStyleBackColor = true;
            this.checkPrice.CheckedChanged += new System.EventHandler(this.checkPrice_CheckedChanged);
            // 
            // buttonFix
            // 
            this.buttonFix.Location = new System.Drawing.Point(767, 107);
            this.buttonFix.Name = "buttonFix";
            this.buttonFix.Size = new System.Drawing.Size(75, 23);
            this.buttonFix.TabIndex = 45;
            this.buttonFix.Text = "Fix";
            this.buttonFix.UseVisualStyleBackColor = true;
            this.buttonFix.Click += new System.EventHandler(this.buttonFix_Click);
            // 
            // Umetnisko_delo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1049, 835);
            this.Controls.Add(this.buttonFix);
            this.Controls.Add(this.checkPrice);
            this.Controls.Add(this.labelTitel);
            this.Controls.Add(this.textBoxTitel);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.textBoxTitle);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.comboBoxMaterial);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.comboBoxTip);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textBoxSirina);
            this.Controls.Add(this.textBoxVisina);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.imagePresentationUC1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxOpis);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxStSlik);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxNegativ);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxZaznamek);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxDolzina);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxLeto);
            this.Controls.Add(this.labelNaslov);
            this.Controls.Add(this.textBoxNaslov);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxstDela);
            this.Icon = this.Icon;
            this.Name = "Umetnisko_delo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Umetniško delo";
            this.Load += new System.EventHandler(this.Umetnisko_delo_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        private void label9_Click(object sender, EventArgs e) {

        }

        private void checkPrice_CheckedChanged(object sender, EventArgs e) {            
            this.panel1.Visible = checkPrice.Checked;
        }

        private void buttonFix_Click(object sender, EventArgs e) {
            imagePresentationUC1.RealignComponents();
        }
    }
}
