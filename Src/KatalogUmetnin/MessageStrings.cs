using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace KatalogUmetnin
{
    [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode, CompilerGenerated]
    internal class MessageStrings
    {
        private static ResourceManager resourceMan;

        private static CultureInfo resourceCulture;

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(MessageStrings.resourceMan, null)) {
                    ResourceManager temp = new ResourceManager("KatalogUmetnin.MessageStrings", typeof(MessageStrings).Assembly);
                    MessageStrings.resourceMan = temp;
                }
                return MessageStrings.resourceMan;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static CultureInfo Culture {
            get {
                return MessageStrings.resourceCulture;
            }
            set {
                MessageStrings.resourceCulture = value;
            }
        }

        internal static string ArtDeleteText {
            get {
                return MessageStrings.ResourceManager.GetString("ArtDeleteText", MessageStrings.resourceCulture);
            }
        }

        internal static string ArtDeleteTitle {
            get {
                return MessageStrings.ResourceManager.GetString("ArtDeleteTitle", MessageStrings.resourceCulture);
            }
        }

        internal static string CenaValidationText {
            get {
                return MessageStrings.ResourceManager.GetString("CenaValidationText", MessageStrings.resourceCulture);
            }
        }

        internal static string CenaValidationTitle {
            get {
                return MessageStrings.ResourceManager.GetString("CenaValidationTitle", MessageStrings.resourceCulture);
            }
        }

        internal static string Error {
            get {
                return MessageStrings.ResourceManager.GetString("Error", MessageStrings.resourceCulture);
            }
        }

        internal static string ImageItemDeleteText {
            get {
                return MessageStrings.ResourceManager.GetString("ImageItemDeleteText", MessageStrings.resourceCulture);
            }
        }

        internal static string ImageItemDeleteTitle {
            get {
                return MessageStrings.ResourceManager.GetString("ImageItemDeleteTitle", MessageStrings.resourceCulture);
            }
        }

        internal static string InvalidValue {
            get {
                return MessageStrings.ResourceManager.GetString("InvalidValue", MessageStrings.resourceCulture);
            }
        }

        internal static string IzkupicekValidationText {
            get {
                return MessageStrings.ResourceManager.GetString("IzkupicekValidationText", MessageStrings.resourceCulture);
            }
        }

        internal static string IzkupicekValidationTitle {
            get {
                return MessageStrings.ResourceManager.GetString("IzkupicekValidationTitle", MessageStrings.resourceCulture);
            }
        }

        internal static string DimenzijeValidationText {
            get {
                return MessageStrings.ResourceManager.GetString("DimenzijeValidationText", MessageStrings.resourceCulture);
            }
        }

        internal static string DimenzijeValidationTitle {
            get {
                return MessageStrings.ResourceManager.GetString("DimenzijeValidationTitle", MessageStrings.resourceCulture);
            }
        }

        internal static string MoneyValidation0 {
            get {
                return MessageStrings.ResourceManager.GetString("MoneyValidation0", MessageStrings.resourceCulture);
            }
        }

        internal static string SettingsLanguageChangeText {
            get {
                return MessageStrings.ResourceManager.GetString("SettingsLanguageChangeText", MessageStrings.resourceCulture);
            }
        }

        internal static string SettingsStorageNotSetText {
            get {
                return MessageStrings.ResourceManager.GetString("SettingsStorageNotSetText", MessageStrings.resourceCulture);
            }
        }

        internal MessageStrings() {
        }
    }
}
