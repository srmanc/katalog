﻿namespace KatalogUmetnin
{
    partial class RefForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxEnglish = new System.Windows.Forms.TextBox();
            this.labelEnglish = new System.Windows.Forms.Label();
            this.textBoxDeutsch = new System.Windows.Forms.TextBox();
            this.labelDeutsch = new System.Windows.Forms.Label();
            this.textBoxSlovensko = new System.Windows.Forms.TextBox();
            this.labelSlovensko = new System.Windows.Forms.Label();
            this.comboBoxRef = new System.Windows.Forms.ComboBox();
            this.labelRef = new System.Windows.Forms.Label();
            this.listBoxAppear = new System.Windows.Forms.ListBox();
            this.labelAppear = new System.Windows.Forms.Label();
            this.buttonAddNew = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonOpen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxEnglish
            // 
            this.textBoxEnglish.Location = new System.Drawing.Point(12, 81);
            this.textBoxEnglish.Name = "textBoxEnglish";
            this.textBoxEnglish.Size = new System.Drawing.Size(150, 20);
            this.textBoxEnglish.TabIndex = 3;
            this.textBoxEnglish.TextChanged += new System.EventHandler(this.textBoxAnyLang_TextChanged);
            // 
            // labelEnglish
            // 
            this.labelEnglish.AutoSize = true;
            this.labelEnglish.Location = new System.Drawing.Point(9, 65);
            this.labelEnglish.Name = "labelEnglish";
            this.labelEnglish.Size = new System.Drawing.Size(41, 13);
            this.labelEnglish.TabIndex = 2;
            this.labelEnglish.Text = "English";
            // 
            // textBoxDeutsch
            // 
            this.textBoxDeutsch.Location = new System.Drawing.Point(167, 81);
            this.textBoxDeutsch.Name = "textBoxDeutsch";
            this.textBoxDeutsch.Size = new System.Drawing.Size(150, 20);
            this.textBoxDeutsch.TabIndex = 5;
            this.textBoxDeutsch.TextChanged += new System.EventHandler(this.textBoxAnyLang_TextChanged);
            // 
            // labelDeutsch
            // 
            this.labelDeutsch.AutoSize = true;
            this.labelDeutsch.Location = new System.Drawing.Point(164, 65);
            this.labelDeutsch.Name = "labelDeutsch";
            this.labelDeutsch.Size = new System.Drawing.Size(47, 13);
            this.labelDeutsch.TabIndex = 4;
            this.labelDeutsch.Text = "Deutsch";
            // 
            // textBoxSlovensko
            // 
            this.textBoxSlovensko.Location = new System.Drawing.Point(323, 81);
            this.textBoxSlovensko.Name = "textBoxSlovensko";
            this.textBoxSlovensko.Size = new System.Drawing.Size(150, 20);
            this.textBoxSlovensko.TabIndex = 7;
            this.textBoxSlovensko.TextChanged += new System.EventHandler(this.textBoxAnyLang_TextChanged);
            // 
            // labelSlovensko
            // 
            this.labelSlovensko.AutoSize = true;
            this.labelSlovensko.Location = new System.Drawing.Point(320, 65);
            this.labelSlovensko.Name = "labelSlovensko";
            this.labelSlovensko.Size = new System.Drawing.Size(57, 13);
            this.labelSlovensko.TabIndex = 6;
            this.labelSlovensko.Text = "Slovensko";
            // 
            // comboBoxRef
            // 
            this.comboBoxRef.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRef.FormattingEnabled = true;
            this.comboBoxRef.Location = new System.Drawing.Point(12, 29);
            this.comboBoxRef.Name = "comboBoxRef";
            this.comboBoxRef.Size = new System.Drawing.Size(254, 21);
            this.comboBoxRef.TabIndex = 8;
            this.comboBoxRef.SelectedIndexChanged += new System.EventHandler(this.comboBoxRef_SelectedIndexChanged);
            // 
            // labelRef
            // 
            this.labelRef.AutoSize = true;
            this.labelRef.Location = new System.Drawing.Point(9, 13);
            this.labelRef.Name = "labelRef";
            this.labelRef.Size = new System.Drawing.Size(62, 13);
            this.labelRef.TabIndex = 9;
            this.labelRef.Text = "References";
            // 
            // listBoxAppear
            // 
            this.listBoxAppear.FormattingEnabled = true;
            this.listBoxAppear.Location = new System.Drawing.Point(12, 141);
            this.listBoxAppear.Name = "listBoxAppear";
            this.listBoxAppear.Size = new System.Drawing.Size(254, 108);
            this.listBoxAppear.TabIndex = 10;
            this.listBoxAppear.SelectedIndexChanged += new System.EventHandler(this.listBoxAppear_SelectedIndexChanged);
            this.listBoxAppear.DoubleClick += new System.EventHandler(this.buttonOpen_Click);
            // 
            // labelAppear
            // 
            this.labelAppear.AutoSize = true;
            this.labelAppear.Location = new System.Drawing.Point(9, 125);
            this.labelAppear.Name = "labelAppear";
            this.labelAppear.Size = new System.Drawing.Size(165, 13);
            this.labelAppear.TabIndex = 11;
            this.labelAppear.Text = "Records with selected references";
            // 
            // buttonAddNew
            // 
            this.buttonAddNew.Location = new System.Drawing.Point(398, 168);
            this.buttonAddNew.Name = "buttonAddNew";
            this.buttonAddNew.Size = new System.Drawing.Size(75, 23);
            this.buttonAddNew.TabIndex = 12;
            this.buttonAddNew.Text = "Add new";
            this.buttonAddNew.UseVisualStyleBackColor = true;
            this.buttonAddNew.Click += new System.EventHandler(this.buttonAddNew_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(398, 197);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 13;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(398, 226);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 14;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonOpen
            // 
            this.buttonOpen.Enabled = false;
            this.buttonOpen.Location = new System.Drawing.Point(272, 226);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(75, 23);
            this.buttonOpen.TabIndex = 15;
            this.buttonOpen.Text = "Open";
            this.buttonOpen.UseVisualStyleBackColor = true;
            this.buttonOpen.Click += new System.EventHandler(this.buttonOpen_Click);
            // 
            // RefForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 261);
            this.Controls.Add(this.buttonOpen);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonAddNew);
            this.Controls.Add(this.labelAppear);
            this.Controls.Add(this.listBoxAppear);
            this.Controls.Add(this.labelRef);
            this.Controls.Add(this.comboBoxRef);
            this.Controls.Add(this.textBoxSlovensko);
            this.Controls.Add(this.labelSlovensko);
            this.Controls.Add(this.textBoxDeutsch);
            this.Controls.Add(this.labelDeutsch);
            this.Controls.Add(this.textBoxEnglish);
            this.Controls.Add(this.labelEnglish);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RefForm";
            this.Text = "RefForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxEnglish;
        private System.Windows.Forms.Label labelEnglish;
        private System.Windows.Forms.TextBox textBoxDeutsch;
        private System.Windows.Forms.Label labelDeutsch;
        private System.Windows.Forms.TextBox textBoxSlovensko;
        private System.Windows.Forms.Label labelSlovensko;
        private System.Windows.Forms.ComboBox comboBoxRef;
        private System.Windows.Forms.Label labelRef;
        private System.Windows.Forms.ListBox listBoxAppear;
        private System.Windows.Forms.Label labelAppear;
        private System.Windows.Forms.Button buttonAddNew;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonOpen;
    }
}