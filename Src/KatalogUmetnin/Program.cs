using System;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace KatalogUmetnin
{
	internal static class Program
	{
        public static string Version {
            get {
                return "2.0";
            }
        }

        public static Decimal VersionNum {
            get {
                Decimal versionNum;
                Decimal.TryParse(Version, out versionNum);
                return versionNum;
            }
        }

        [STAThread]
		private static void Main()
	    {
            Settings.Load();
            if (!Debugger.IsAttached) {
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(OnUnhandledException);
                Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(OnThreadException);
            }
           
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();

            Boolean release = true;         
            DateTime modified = new DateTime(636026354640000000);
            if (Debugger.IsAttached || release)
                modified = new FileInfo(assembly.Location).LastWriteTime;
            DateTime validUntil = modified.AddDays(31);
            DateTime now = DateTime.Now;
            TimeSpan span = now - validUntil;
            if (span.TotalDays > 1 && !release) {
                MessageBox.Show(string.Format("This demo has expired {0} ago. Please install a release version.",
                    (int)Math.Floor(span.TotalDays)), "KatalogUmetnin");
            } else {
                if (!(Debugger.IsAttached || release))
                    MessageBox.Show(string.Format("This demo version is valid for {0} days. After this trial period expires, the program will not work until a release version is installed.",
                        (int)Math.Ceiling(-span.TotalDays)));
                System.Windows.Forms.Application.EnableVisualStyles();
			    System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
                System.Windows.Forms.Application.Run(new Form1());                                
            }
        }

        static void OnThreadException(object sender, System.Threading.ThreadExceptionEventArgs e) {
            HandleException(e.Exception, "Thread exception");       
        }

        static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e) {
            HandleException(e.ExceptionObject as Exception, "Unhandled exception");       
        }

        static void HandleException(Exception exception, string exceptionType) {
            string log = Settings.GetLogPath();
            StreamWriter writer = new StreamWriter(log);
            writer.WriteLine(DateTime.Now.ToString() + ": " + exceptionType + " [" + 
                exception.GetType() + "] (" + exception.Message + ")");
            writer.WriteLine(exception.StackTrace);
            writer.Close();

            if (!exception.GetType().Equals(typeof(ThreadAbortException))) { 
                var form = new ErrorForm();
                form.ShowDialog();
            }
        }
	}
}
