using KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters;
using KatalogUmetnin.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace KatalogUmetnin
{
	public class PrintRecordSelection : System.Windows.Forms.Form
	{
		public List<string> SelectedArts = new List<string>();

		private string Selected = "";

		private IContainer components = null;

		private System.Windows.Forms.ComboBox comboBox1;

		private KatalogUmetnin_dbDataSet katalogUmetnin_dbDataSet;

		private System.Windows.Forms.BindingSource umetnineStUmDelaBindingSource;

		private UmetnineStUmDelaTableAdapter umetnineStUmDelaTableAdapter;

		private System.Windows.Forms.ComboBox comboBox2;

		private System.Windows.Forms.BindingSource umetnineStUmDelaBindingSource1;

		private System.Windows.Forms.Label label1;

		private System.Windows.Forms.Label label2;

		private System.Windows.Forms.Button button3;

		private System.Windows.Forms.Button button2;

		public PrintRecordSelection(string st_umdela)
		{
			this.InitializeComponent();
			this.Selected = st_umdela;
		}

		private void PrintRecordSelection_Load(object sender, EventArgs e)
		{
			this.umetnineStUmDelaTableAdapter.Fill(this.katalogUmetnin_dbDataSet.UmetnineStUmDela);
			if (!string.IsNullOrEmpty(this.Selected))
			{
				this.comboBox1.SelectedValue = this.Selected;
				this.comboBox2.SelectedValue = this.Selected;
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
		}

		private void DoOK()
		{
			int a = this.comboBox1.SelectedIndex;
			int b = this.comboBox2.SelectedIndex;
			if (this.comboBox1.SelectedIndex > this.comboBox2.SelectedIndex)
			{
				a = b;
				b = this.comboBox1.SelectedIndex;
			}
			for (int i = a; i <= b; i++)
			{
				this.SelectedArts.Add(this.katalogUmetnin_dbDataSet.UmetnineStUmDela[i].st_um_dela);
			}
			base.DialogResult = System.Windows.Forms.DialogResult.OK;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			this.DoOK();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			base.DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintRecordSelection));
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.umetnineStUmDelaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.katalogUmetnin_dbDataSet = new KatalogUmetnin.KatalogUmetnin_dbDataSet();
            this.umetnineStUmDelaTableAdapter = new KatalogUmetnin.KatalogUmetnin_dbDataSetTableAdapters.UmetnineStUmDelaTableAdapter();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.umetnineStUmDelaBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.umetnineStUmDelaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.katalogUmetnin_dbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.umetnineStUmDelaBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.umetnineStUmDelaBindingSource;
            this.comboBox1.DisplayMember = "st_um_dela";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(47, 22);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(152, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.ValueMember = "st_um_dela";
            // 
            // umetnineStUmDelaBindingSource
            // 
            this.umetnineStUmDelaBindingSource.DataMember = "UmetnineStUmDela";
            this.umetnineStUmDelaBindingSource.DataSource = this.katalogUmetnin_dbDataSet;
            // 
            // katalogUmetnin_dbDataSet
            // 
            this.katalogUmetnin_dbDataSet.DataSetName = "KatalogUmetnin_dbDataSet";
            this.katalogUmetnin_dbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // umetnineStUmDelaTableAdapter
            // 
            this.umetnineStUmDelaTableAdapter.ClearBeforeFill = true;
            // 
            // comboBox2
            // 
            this.comboBox2.DataSource = this.umetnineStUmDelaBindingSource1;
            this.comboBox2.DisplayMember = "st_um_dela";
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(48, 49);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(151, 21);
            this.comboBox2.TabIndex = 1;
            this.comboBox2.ValueMember = "st_um_dela";
            // 
            // umetnineStUmDelaBindingSource1
            // 
            this.umetnineStUmDelaBindingSource1.DataMember = "UmetnineStUmDela";
            this.umetnineStUmDelaBindingSource1.DataSource = this.katalogUmetnin_dbDataSet;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Von:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Bis:";
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button3.Location = new System.Drawing.Point(96, 80);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(103, 32);
            this.button3.TabIndex = 6;
            this.button3.Text = "Abbrechen";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button2.Location = new System.Drawing.Point(15, 80);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 32);
            this.button2.TabIndex = 5;
            this.button2.Text = "OK";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // PrintRecordSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(211, 124);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "PrintRecordSelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Drucken";
            this.Load += new System.EventHandler(this.PrintRecordSelection_Load);
            ((System.ComponentModel.ISupportInitialize)(this.umetnineStUmDelaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.katalogUmetnin_dbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.umetnineStUmDelaBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
	}
}
