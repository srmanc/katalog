﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ADOX;
using ADODB;
using System.IO;
using System.Runtime.Remoting;
using System.Globalization;
using System.Threading;

namespace KatalogUmetnin
{
    public static class TransformDB
    {
        public static string DateTimeToString(DateTime dateTime) {
            return "#" + dateTime.ToString("yyyy-MM-dd HH:mm:ss") + "#";
        }

        public static DateTime StringToDateTime(String s) {
            return DateTime.Parse(s.Replace("#", ""));
        }

        public static void ToVer2(string oldDBPath, string newDBPath) {
            CultureLock cl = new CultureLock();

            if (File.Exists(newDBPath))
                File.Delete(newDBPath);

            Connection conOld = new Connection(), conNew = null;
            Catalog catNew = new Catalog(), catOld = new Catalog();
            object records;

            string dbOldConnectionString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}", Path.GetFileName(oldDBPath));
            string dbNewConnectionString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Jet OLEDB:Database Password='" + Settings.DefaultPassword + "';", Path.GetFileName(newDBPath));
            conOld.Open(dbOldConnectionString);
            catOld.ActiveConnection = conOld;
            catNew.Create(dbNewConnectionString);
            conNew = catNew.ActiveConnection;

            CreateTables(conNew);

            Recordset set = conOld.Execute("SELECT * FROM Umetnine ORDER BY st_um_dela ASC", out records);            
            Dictionary<int,int> materialIndexes = new Dictionary<int,int>(),
                typeIndexes = new Dictionary<int,int>();
            List<string> uniqueMaterials = new List<string>(),
                uniqueTypes = new List<string>();
            List<string> fields = new List<string>(), values = new List<string>();
            GetMaterials(set, materialIndexes, uniqueMaterials);
            GetTypes(set, typeIndexes, uniqueTypes);

            InsertUniques(conNew, "Materiali", "ImeMateriala", uniqueMaterials);
            InsertUniques(conNew, "Tipi", "ImeTipa", uniqueTypes);

            for (set.MoveFirst(); !set.EOF; set.MoveNext()) {
                int ID = set.Fields[0].Value;
                string st_um_dela = set.Fields["st_um_dela"].Value;
                string title = set.Fields["naslov"].Value;
                string size = set.Fields["velikost"].Value;
                string comment = set.Fields["zaznamek"].Value;
                string newTitle, newType, newWidth, newHeight, newDepth, newComment;

                BreakupTitle(title, comment, out newTitle, out newType, out newComment);
                comment = newComment;
                BreakupSize(size, comment, out newWidth, out newHeight, out newDepth, out newComment);

                fields.Clear();
                values.Clear();
                for (int i = 0; i < set.Fields.Count; i++) {                    
                    string name = set.Fields[i].Name;
                    string value = set.Fields[i].Value.ToString();
                    dynamic v = set.Fields[i].Value;
                    Type t = v.GetType();

                    if (name.Equals("naslov")) {
                        int idx = set.Fields["Nr"].Value;
                        idx = typeIndexes[idx];
                        fields.Add("tip");
                        values.Add(idx.ToString());

                        value = newTitle;
                    }
                    else if (name.Equals("zaznamek"))
                        value = newComment;
                    else if (name.Equals("material")) {
                        int idx = set.Fields["Nr"].Value;
                        idx = materialIndexes[idx];
                        value = idx.ToString();
                        t = typeof(int);
                    }

                    if (name.Equals("velikost")) {
                        fields.Add("dolzina");
                        values.Add(newWidth);
                        fields.Add("visina");
                        values.Add(newHeight);
                        fields.Add("sirina");
                        values.Add(newDepth);
                    }
                    else if (name.Equals("naslov")) {
                        fields.Add("naslov_en");
                        values.Add("'" + newTitle + "'");
                        fields.Add("naslov_de");
                        values.Add("'" + newTitle + "'");
                        fields.Add("naslov_si");
                        values.Add("'" + newTitle + "'");
                    }
                    else {
                        if (t == typeof(string))
                            value = "'" + value + "'";
                        else if (t == typeof(DateTime))
                            value = DateTimeToString(v);
                        fields.Add(name);
                        values.Add(value); 
                    }
                }
                string sqlInsert = "INSERT INTO Umetnine (" +
                    String.Join(",", fields) +
                    ") VALUES (" + 
                    String.Join(",", values) + ")";
                Recordset inset = conNew.Execute(sqlInsert, out records);                
            }
            set.Close();

            set = conOld.Execute("SELECT * FROM Slike", out records);
            fields.Clear();
            values.Clear();
            for (int i = 0; i < set.Fields.Count; i++)
                fields.Add(set.Fields[i].Name);
            fields.Add("Rotation");
            for (set.MoveFirst(); !set.EOF; set.MoveNext()) {
                values.Clear();                
                for (int i = 0; i < set.Fields.Count; i++) {
                    string value = set.Fields[i].Value.ToString();
                    dynamic v = set.Fields[i].Value;
                    Type t = v.GetType();

                    if (t == typeof(string))
                        value = "'" + value + "'";
                    else if (t == typeof(DateTime))
                        value = DateTimeToString(v);                    
                    values.Add(value);                    
                }
                values.Add("0");
                string sqlInsert = "INSERT INTO Slike (" +
                    String.Join(",", fields) +
                    ") VALUES (" +
                    String.Join(",", values) + ")";
                Recordset inset = conNew.Execute(sqlInsert, out records);
            }
            set.Close();

            conOld.Close();
            conNew.Close();

            cl.Unlock();
        }

        private static void InsertUniques(Connection con, string tableName, string uniqueName, List<string> uniques) {
            string sqlInsert = "INSERT INTO {0} ({1}, {2}_en, {2}_de, {2}_si) VALUES ({3}, '{4}', '{4}', '{4}')";
            object records;
            int idx = 0;
            foreach (string unique in uniques) {
                string s = string.Format(sqlInsert, tableName, "ID", uniqueName, idx++, unique);
                Recordset set = con.Execute(s, out records);
            }
        }

        private static void BreakupTitle(string title, string comment, out string newTitle, out string newType, out string newComment) {
            if (title.Contains("B-FLO"))
                title = title.Replace("B-FLO", "BFLO");
            int iLine = title.IndexOf("-");

            if (iLine <= 0) {
                int c = 0;
                while (c < title.Length && Char.IsUpper(title[c]))
                    c++;

                if (c > 0 && c < title.Length && title[c] == ' ') {
                    title = title.Insert(c, "-");
                    iLine = c;
                }
            }

            if (iLine > 0) {
                newType = title.Substring(0, iLine).Trim();
                newTitle = title.Substring(iLine + 1).Trim().Replace("   ", " ").Replace("  ", " ");

                newTitle = InsertAfter(newTitle, ' ', ',');
                newTitle = DeleteAfter(newTitle, ' ', '(');
                newTitle = DeleteBefore(newTitle, ' ', ')');

                newComment = comment;
                int iComma = newTitle.IndexOf(',');
                if (iComma > 0) {
                    if (newComment.Length > 0)
                        newComment += ",";
                    newComment += newTitle.Substring(iComma + 1);
                    newComment = newComment.Trim();
                    newTitle = newTitle.Substring(0, iComma).Trim();
                }
            }
            else {
                newType = "";
                newTitle = title;
                newComment = comment;
            }
            newType = newType.ToUpper();

            if (newType == "RELL")
                newType = "REL";
            else if (newType == "TEI")
                newType = "ZEI";
        }

        private static void BreakupSize(string size, string comment, out string newWidth, out string newHeight, out string newDepth, out string newComment) {
            string oldSize = size;
            newWidth = "";
            newHeight = "";
            newDepth = "";
            newComment = comment.Trim();

            if (size.Contains("ca")) {
                if (newComment.Length > 0)
                    newComment += ", ";
                newComment += oldSize;
                size = size.Replace("ca.", "").Replace("ca", "").Trim();
            }
            if (size.Contains("O/")) {
                if (newComment.Length > 0)
                    newComment += ", ";
                newComment += oldSize;
                size = size.Replace("O/", "").Trim();
            }
            if (size.Contains("?")) {
                if (newComment.Length > 0)
                    newComment += ", ";
                newComment += oldSize;
                size = size.Replace("?", "").Trim();
            }
            if (size.Contains("cm")) {
                /*if (newZaznamek.Length > 0)
                    newZaznamek += ", ";
                newZaznamek += oldvelikost;
                
                 * zaznamek ni potreben, ce je enota cm
                */
                size = size.Replace("cm", "").Trim();
            }
            if (size.Contains("m")) {
                if (newComment.Length > 0)
                    newComment += ", ";
                newComment += oldSize;
                size = size.Replace("m", "").Trim();
            }

            string[] arrs = size.Split(new char[] { 'x', 'X' }, StringSplitOptions.RemoveEmptyEntries);
            if (arrs.Length == 1 && arrs[0].Contains('/'))
                arrs = size.Split('/');
            List<string> s = arrs.ToList();
            while (s.Count < 3)
                s.Add("0");
            double d;


            newWidth = s[0].Trim().Replace(",", ".");
            newHeight = s[1].Trim().Replace(",", ".");
            newDepth = s[2].Trim().Replace(",", ".");

            if (!double.TryParse(newDepth, out d)) {
                int idx = newDepth.IndexOf(" ");
                if (idx > 0) {
                    if (newComment.Length > 0)
                        newComment += ", ";
                    newComment += newDepth.Substring(idx + 1);
                    newDepth = newDepth.Substring(0, idx);
                }
            }

            if (!double.TryParse(newWidth, out d) ||
                !double.TryParse(newHeight, out  d) ||
                !double.TryParse(newDepth, out d))
                BreakPrimitiveSize(oldSize, comment, out newWidth, out newHeight, out newDepth, out newComment);
        }

        private static void BreakPrimitiveSize(string size, string comment, out string newWidth, out string newHeight, out string newDepth, out string newComment) {
            newComment = comment;
            if (newComment.Length > 0)
                newComment += ", ";
            newComment += size;
            newWidth = "";
            newHeight = "";
            newDepth = "";

            for (int i = 0; i < size.Length; i++)
                if (!Char.IsNumber(size[i])) {
                    size = size.Remove(i, 1);
                    size = size.Insert(i, " ");
                }
            string[] s = size.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < s.Length; i++)
                switch (i) {
                    case 0: newWidth = s[i].Replace(",", "."); break;
                    case 1: newHeight = s[i].Replace(",", "."); break;
                    case 2: newDepth = s[i].Replace(",", "."); break;
                }

            double d;
            if (!double.TryParse(newWidth, out d))
                newWidth = "0";
            if (!double.TryParse(newHeight, out d))
                newHeight = "0";
            if (!double.TryParse(newDepth, out d))
                newDepth = "0";
        }

        private static void GetMaterials(Recordset set, Dictionary<int,int> indexes, List<string> uniques) {            
            for (set.MoveFirst(); !set.EOF; set.MoveNext()) {
                int ID = set.Fields["Nr"].Value;
                string material = set.Fields["material"].Value;
                material = material.ToLower();
                if (material.Length > 0) {
                    material = Char.ToUpper(material[0]) + material.Substring(1);
                }                

                if (uniques.Contains(material))
                    indexes.Add(ID, uniques.IndexOf(material));
                else {
                    if (material.Equals("Zei"))
                        material = "Zeichnung";

                    int idx = -1;
                    for (int i = 0; i < uniques.Count; i++) {
                        int d = LevenshteinDistance.Compute(uniques[i], material);
                        double k = (double)(2 * d) / (material.Length + uniques[i].Length);

                        if (k < 0.35) {
                            if (k > 0.3)
                                idx = idx;
                            idx = i;
                            break;
                        }
                    }

                    if (idx >= 0)
                        indexes.Add(ID, idx);
                    else {
                        indexes.Add(ID, uniques.Count);
                        uniques.Add(material);
                    }
                }
            }
        }

        private static void GetTypes(Recordset set, Dictionary<int, int> indexes, List<string> uniques) {
            for (set.MoveFirst(); !set.EOF; set.MoveNext()) {
                int ID = set.Fields["Nr"].Value;
                string title = set.Fields["naslov"].Value;
                string comment = set.Fields["zaznamek"].Value;
                string newTitle, newType, newComment;

                BreakupTitle(title, comment, out newTitle, out newType, out newComment);

                if (uniques.Contains(newType))
                    indexes.Add(ID, uniques.IndexOf(newType));
                else {
                    indexes.Add(ID, uniques.Count);
                    uniques.Add(newType);
                }
            }
        }



        private static string InsertAfter(string s, char sp, char c) {
            int idx = s.IndexOf(c);
            while (idx > 0) {
                if ((idx < s.Length - 1) &&
                    (s[idx + 1] != sp))
                    s = s.Insert(idx + 1, " ");
                idx = s.IndexOf(c, idx + 1);
            }

            return s;
        }

        private static string DeleteAfter(string s, char sp, char c) {
            int idx = s.IndexOf(c);
            while (idx > 0) {
                if ((idx < s.Length - 1) &&
                    (s[idx + 1] == sp))
                    s = s.Remove(idx + 1, 1);
                idx = s.IndexOf(c, idx + 1);
            }

            return s;
        }

        private static string DeleteBefore(string s, char sp, char c) {
            int idx = s.IndexOf(c);
            while (idx > 0) {
                if ((idx < s.Length) &&
                    (s[idx - 1] == sp))
                    s = s.Remove(--idx, 1);
                idx = s.IndexOf(c, idx + 1);
            }

            return s;
        }

        private static void CreateTables(Connection conNew) {
            string sqlCreateTable;
            object recs;

            sqlCreateTable = @"CREATE TABLE [Umetnine] 
            (   [Nr] COUNTER,
                [st_um_dela] TEXT(255),
                [tip] INTEGER,
                [naslov_en] TEXT(255),
                [naslov_de] TEXT(255),
                [naslov_si] TEXT(255),    
                [leto_izdelave] TEXT(255),                             
                [material] INTEGER,
                [dolzina] INTEGER,
                [visina] INTEGER,
                [sirina] INTEGER,
                [zaznamek] TEXT(255),
                [st_negativ] TEXT(255),
                [cena] MEMO,
                [cena_dem] MEMO,
                [st_slik] TEXT(255),
                [opis] TEXT(255),
                [ID] TEXT(255),
                [datum] DATETIME,
                [izkupicek] MEMO 
            )";
            conNew.Execute(sqlCreateTable, out recs);

            sqlCreateTable = @"CREATE TABLE [Slike] 
            (   [ID] COUNTER,
                [Nr] INTEGER,
                [Ime] TEXT(255),
                [VrstniRed] INTEGER,
                [Zoom] FLOAT,
                [X] INTEGER,
                [Y] INTEGER,
                [Visible] YESNO,
                [Rotation] INTEGER

            )";
            conNew.Execute(sqlCreateTable, out recs);

            sqlCreateTable = @"CREATE TABLE [Napisi] 
            (  [Nr] INTEGER,
               [IDNapisa] INTEGER,
               [Tekst] MEMO,
               [Font] TEXT(255),
               [Color] TEXT(255),
               [Size] INTEGER,
               [X] INTEGER,
               [Y] INTEGER 

            )";
            conNew.Execute(sqlCreateTable, out recs);

            /* -- novo, sifranti -- */
            sqlCreateTable = @"CREATE TABLE [Materiali] 
            (  [ID] INTEGER,
               [ImeMateriala_en] TEXT(255),
               [ImeMateriala_de] TEXT(255),
               [ImeMateriala_si] TEXT(255)
            )";
            conNew.Execute(sqlCreateTable, out recs);

            sqlCreateTable = @"CREATE TABLE [Tipi] 
            (  [ID] INTEGER,
               [ImeTipa_en] TEXT(255),
               [ImeTipa_de] TEXT(255),
               [ImeTipa_si] TEXT(255) 
            )";
            conNew.Execute(sqlCreateTable, out recs);
        }
    }

    static class LevenshteinDistance
    {
        /// <summary>
        /// Compute the distance between two strings.
        /// </summary>
        public static int Compute(string s, string t) {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0) {
                return m;
            }

            if (m == 0) {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++) {
            }

            for (int j = 0; j <= m; d[0, j] = j++) {
            }

            // Step 3
            for (int i = 1; i <= n; i++) {
                //Step 4
                for (int j = 1; j <= m; j++) {
                    // Step 5
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }
    }
}
