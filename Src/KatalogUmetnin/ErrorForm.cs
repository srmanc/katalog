﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using System.IO;
using System.Threading;

namespace KatalogUmetnin
{
	public partial class ErrorForm: Form
	{
		public ErrorForm()
		{            
            InitializeComponent();
            Settings.SetTexts(this);            
        }

        private void buttonSend_Click(object sender, EventArgs e) {
            var mail = new MailMessage("info@mail.ku-ku.si", "irmancnik@gmail.com");
            mail.Subject = "KatalogUmetnin Error Report";
            mail.Body = textBox1.Text;
            if (File.Exists(Settings.GetLogPath()))
                mail.Attachments.Add(new Attachment(Settings.GetLogPath()));

            var client = new SmtpClient("smtp.sparkpostmail.com"); 
            client.Port = 587;
            client.Credentials = new System.Net.NetworkCredential("SMTP_injection", "72d3abe5205df7375b5d5d9e686b8b98b116a6e0");

            buttonSend.Enabled = false;
            new Thread(() => {
                try
                {                    
                    client.Send(mail);
                }
                catch (Exception ex)
                {
                    ex = ex;
                }
                finally
                {
                    Invoke((MethodInvoker)delegate
                    {
                        buttonSend.Enabled = true;
                        Close();
                    });                    
                }
            }).Start();                    
        }

        private void buttonClose_Click(object sender, EventArgs e) {
            Close();
        }        
	}
}
