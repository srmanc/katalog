using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace KatalogUmetnin
{
	[HelpKeyword("vs.data.DataSet"), DesignerCategory("code"), ToolboxItem(true), XmlRoot("KatalogUmetnin_dbDataSet"), XmlSchemaProvider("GetTypedDataSetSchema")]
	[Serializable]
	public class KatalogUmetnin_dbDataSet : DataSet
	{
		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
		public delegate void UmetnineRowChangeEventHandler(object sender, KatalogUmetnin_dbDataSet.UmetnineRowChangeEvent e);

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
		public delegate void SlikeRowChangeEventHandler(object sender, KatalogUmetnin_dbDataSet.SlikeRowChangeEvent e);

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
		public delegate void UmetnineStUmDelaRowChangeEventHandler(object sender, KatalogUmetnin_dbDataSet.UmetnineStUmDelaRowChangeEvent e);

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
		public delegate void NapisiRowChangeEventHandler(object sender, KatalogUmetnin_dbDataSet.NapisiRowChangeEvent e);

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
		public delegate void UmetnineOpisRowChangeEventHandler(object sender, KatalogUmetnin_dbDataSet.UmetnineOpisRowChangeEvent e);

		[XmlSchemaProvider("GetTypedTableSchema")]
		[Serializable]
		public class UmetnineDataTable : TypedTableBase<KatalogUmetnin_dbDataSet.UmetnineRow>
		{
			private DataColumn columnNr;

			private DataColumn columnst_um_dela;

			private DataColumn columnnaslov;

			private DataColumn columnleto_izdelave;

			private DataColumn columnmaterial;

			private DataColumn columnvelikost;

			private DataColumn columnzaznamek;

			private DataColumn columnst_negativ;

			private DataColumn columncena;

			private DataColumn columncena_dem;

			private DataColumn columnst_slik;

			private DataColumn columnopis;

			private DataColumn columnID;

			private DataColumn columndatum;

			private DataColumn columnizkupicek;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.UmetnineRowChangeEventHandler UmetnineRowChanging;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.UmetnineRowChangeEventHandler UmetnineRowChanged;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.UmetnineRowChangeEventHandler UmetnineRowDeleting;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.UmetnineRowChangeEventHandler UmetnineRowDeleted;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn NrColumn
			{
				get
				{
					return this.columnNr;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn st_um_delaColumn
			{
				get
				{
					return this.columnst_um_dela;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn naslovColumn
			{
				get
				{
					return this.columnnaslov;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn leto_izdelaveColumn
			{
				get
				{
					return this.columnleto_izdelave;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn materialColumn
			{
				get
				{
					return this.columnmaterial;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn velikostColumn
			{
				get
				{
					return this.columnvelikost;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn zaznamekColumn
			{
				get
				{
					return this.columnzaznamek;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn st_negativColumn
			{
				get
				{
					return this.columnst_negativ;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn cenaColumn
			{
				get
				{
					return this.columncena;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn cena_demColumn
			{
				get
				{
					return this.columncena_dem;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn st_slikColumn
			{
				get
				{
					return this.columnst_slik;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn opisColumn
			{
				get
				{
					return this.columnopis;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn IDColumn
			{
				get
				{
					return this.columnID;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn datumColumn
			{
				get
				{
					return this.columndatum;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn izkupicekColumn
			{
				get
				{
					return this.columnizkupicek;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Browsable(false), DebuggerNonUserCode]
			public int Count
			{
				get
				{
					return base.Rows.Count;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineRow this[int index]
			{
				get
				{
					return (KatalogUmetnin_dbDataSet.UmetnineRow)base.Rows[index];
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public UmetnineDataTable()
			{
				base.TableName = "Umetnine";
				this.BeginInit();
				this.InitClass();
				this.EndInit();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal UmetnineDataTable(DataTable table)
			{
				base.TableName = table.TableName;
				if (table.CaseSensitive != table.DataSet.CaseSensitive)
				{
					base.CaseSensitive = table.CaseSensitive;
				}
				if (table.Locale.ToString() != table.DataSet.Locale.ToString())
				{
					base.Locale = table.Locale;
				}
				if (table.Namespace != table.DataSet.Namespace)
				{
					base.Namespace = table.Namespace;
				}
				base.Prefix = table.Prefix;
				base.MinimumCapacity = table.MinimumCapacity;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected UmetnineDataTable(SerializationInfo info, StreamingContext context) : base(info, context)
			{
				this.InitVars();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void AddUmetnineRow(KatalogUmetnin_dbDataSet.UmetnineRow row)
			{
				base.Rows.Add(row);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineRow AddUmetnineRow(string st_um_dela, string naslov, string leto_izdelave, string material, string velikost, string zaznamek, string st_negativ, decimal cena, decimal cena_dem, string st_slik, string opis, string ID, DateTime datum, decimal izkupicek)
			{
				KatalogUmetnin_dbDataSet.UmetnineRow rowUmetnineRow = (KatalogUmetnin_dbDataSet.UmetnineRow)base.NewRow();
				object[] columnValuesArray = new object[]
				{
					null,
					st_um_dela,
					naslov,
					leto_izdelave,
					material,
					velikost,
					zaznamek,
					st_negativ,
					cena,
					cena_dem,
					st_slik,
					opis,
					ID,
					datum,
					izkupicek
				};
				rowUmetnineRow.ItemArray = columnValuesArray;
				base.Rows.Add(rowUmetnineRow);
				return rowUmetnineRow;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineRow FindByNr(int Nr)
			{
				return (KatalogUmetnin_dbDataSet.UmetnineRow)base.Rows.Find(new object[]
				{
					Nr
				});
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public override DataTable Clone()
			{
				KatalogUmetnin_dbDataSet.UmetnineDataTable cln = (KatalogUmetnin_dbDataSet.UmetnineDataTable)base.Clone();
				cln.InitVars();
				return cln;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override DataTable CreateInstance()
			{
				return new KatalogUmetnin_dbDataSet.UmetnineDataTable();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal void InitVars()
			{
				this.columnNr = base.Columns["Nr"];
				this.columnst_um_dela = base.Columns["st_um_dela"];
				this.columnnaslov = base.Columns["naslov"];
				this.columnleto_izdelave = base.Columns["leto_izdelave"];
				this.columnmaterial = base.Columns["material"];
				this.columnvelikost = base.Columns["velikost"];
				this.columnzaznamek = base.Columns["zaznamek"];
				this.columnst_negativ = base.Columns["st_negativ"];
				this.columncena = base.Columns["cena"];
				this.columncena_dem = base.Columns["cena_dem"];
				this.columnst_slik = base.Columns["st_slik"];
				this.columnopis = base.Columns["opis"];
				this.columnID = base.Columns["ID"];
				this.columndatum = base.Columns["datum"];
				this.columnizkupicek = base.Columns["izkupicek"];
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			private void InitClass()
			{
				this.columnNr = new DataColumn("Nr", typeof(int), null, MappingType.Element);
				base.Columns.Add(this.columnNr);
				this.columnst_um_dela = new DataColumn("st_um_dela", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnst_um_dela);
				this.columnnaslov = new DataColumn("naslov", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnnaslov);
				this.columnleto_izdelave = new DataColumn("leto_izdelave", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnleto_izdelave);
				this.columnmaterial = new DataColumn("material", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnmaterial);
				this.columnvelikost = new DataColumn("velikost", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnvelikost);
				this.columnzaznamek = new DataColumn("zaznamek", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnzaznamek);
				this.columnst_negativ = new DataColumn("st_negativ", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnst_negativ);
				this.columncena = new DataColumn("cena", typeof(decimal), null, MappingType.Element);
				base.Columns.Add(this.columncena);
				this.columncena_dem = new DataColumn("cena_dem", typeof(decimal), null, MappingType.Element);
				base.Columns.Add(this.columncena_dem);
				this.columnst_slik = new DataColumn("st_slik", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnst_slik);
				this.columnopis = new DataColumn("opis", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnopis);
				this.columnID = new DataColumn("ID", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnID);
				this.columndatum = new DataColumn("datum", typeof(DateTime), null, MappingType.Element);
				base.Columns.Add(this.columndatum);
				this.columnizkupicek = new DataColumn("izkupicek", typeof(decimal), null, MappingType.Element);
				base.Columns.Add(this.columnizkupicek);
				base.Constraints.Add(new UniqueConstraint("Constraint1", new DataColumn[]
				{
					this.columnNr
				}, true));
				this.columnNr.AutoIncrement = true;
				this.columnNr.AutoIncrementSeed = -1L;
				this.columnNr.AutoIncrementStep = -1L;
				this.columnNr.AllowDBNull = false;
				this.columnNr.Unique = true;
				this.columnst_um_dela.MaxLength = 255;
				this.columnnaslov.MaxLength = 255;
				this.columnleto_izdelave.MaxLength = 255;
				this.columnmaterial.MaxLength = 255;
				this.columnvelikost.MaxLength = 255;
				this.columnzaznamek.MaxLength = 255;
				this.columnst_negativ.MaxLength = 255;
				this.columnst_slik.MaxLength = 255;
				this.columnopis.MaxLength = 255;
				this.columnID.MaxLength = 255;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineRow NewUmetnineRow()
			{
				return (KatalogUmetnin_dbDataSet.UmetnineRow)base.NewRow();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
			{
				return new KatalogUmetnin_dbDataSet.UmetnineRow(builder);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override Type GetRowType()
			{
				return typeof(KatalogUmetnin_dbDataSet.UmetnineRow);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowChanged(DataRowChangeEventArgs e)
			{
				base.OnRowChanged(e);
				if (this.UmetnineRowChanged != null)
				{
					this.UmetnineRowChanged(this, new KatalogUmetnin_dbDataSet.UmetnineRowChangeEvent((KatalogUmetnin_dbDataSet.UmetnineRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowChanging(DataRowChangeEventArgs e)
			{
				base.OnRowChanging(e);
				if (this.UmetnineRowChanging != null)
				{
					this.UmetnineRowChanging(this, new KatalogUmetnin_dbDataSet.UmetnineRowChangeEvent((KatalogUmetnin_dbDataSet.UmetnineRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowDeleted(DataRowChangeEventArgs e)
			{
				base.OnRowDeleted(e);
				if (this.UmetnineRowDeleted != null)
				{
					this.UmetnineRowDeleted(this, new KatalogUmetnin_dbDataSet.UmetnineRowChangeEvent((KatalogUmetnin_dbDataSet.UmetnineRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowDeleting(DataRowChangeEventArgs e)
			{
				base.OnRowDeleting(e);
				if (this.UmetnineRowDeleting != null)
				{
					this.UmetnineRowDeleting(this, new KatalogUmetnin_dbDataSet.UmetnineRowChangeEvent((KatalogUmetnin_dbDataSet.UmetnineRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void RemoveUmetnineRow(KatalogUmetnin_dbDataSet.UmetnineRow row)
			{
				base.Rows.Remove(row);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
			{
				XmlSchemaComplexType type = new XmlSchemaComplexType();
				XmlSchemaSequence sequence = new XmlSchemaSequence();
				KatalogUmetnin_dbDataSet ds = new KatalogUmetnin_dbDataSet();
				XmlSchemaAny any = new XmlSchemaAny();
				any.Namespace = "http://www.w3.org/2001/XMLSchema";
				any.MinOccurs = 0m;
				any.MaxOccurs = 79228162514264337593543950335m;
				any.ProcessContents = XmlSchemaContentProcessing.Lax;
				sequence.Items.Add(any);
				XmlSchemaAny any2 = new XmlSchemaAny();
				any2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
				any2.MinOccurs = 1m;
				any2.ProcessContents = XmlSchemaContentProcessing.Lax;
				sequence.Items.Add(any2);
				XmlSchemaAttribute attribute = new XmlSchemaAttribute();
				attribute.Name = "namespace";
				attribute.FixedValue = ds.Namespace;
				type.Attributes.Add(attribute);
				XmlSchemaAttribute attribute2 = new XmlSchemaAttribute();
				attribute2.Name = "tableTypeName";
				attribute2.FixedValue = "UmetnineDataTable";
				type.Attributes.Add(attribute2);
				type.Particle = sequence;
				XmlSchema dsSchema = ds.GetSchemaSerializable();
				XmlSchemaComplexType result;
				if (xs.Contains(dsSchema.TargetNamespace))
				{
					MemoryStream s = new MemoryStream();
					MemoryStream s2 = new MemoryStream();
					try
					{
						dsSchema.Write(s);
						IEnumerator schemas = xs.Schemas(dsSchema.TargetNamespace).GetEnumerator();
						while (schemas.MoveNext())
						{
							XmlSchema schema = (XmlSchema)schemas.Current;
							s2.SetLength(0L);
							schema.Write(s2);
							if (s.Length == s2.Length)
							{
								s.Position = 0L;
								s2.Position = 0L;
								while (s.Position != s.Length && s.ReadByte() == s2.ReadByte())
								{
								}
								if (s.Position == s.Length)
								{
									result = type;
									return result;
								}
							}
						}
					}
					finally
					{
						if (s != null)
						{
							s.Close();
						}
						if (s2 != null)
						{
							s2.Close();
						}
					}
				}
				xs.Add(dsSchema);
				result = type;
				return result;
			}
		}

		[XmlSchemaProvider("GetTypedTableSchema")]
		[Serializable]
		public class SlikeDataTable : TypedTableBase<KatalogUmetnin_dbDataSet.SlikeRow>
		{
			private DataColumn columnID;

			private DataColumn columnNr;

			private DataColumn columnIme;

			private DataColumn columnVrstniRed;

			private DataColumn columnZoom;

			private DataColumn columnX;

			private DataColumn columnY;

			private DataColumn columnVisible;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.SlikeRowChangeEventHandler SlikeRowChanging;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.SlikeRowChangeEventHandler SlikeRowChanged;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.SlikeRowChangeEventHandler SlikeRowDeleting;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.SlikeRowChangeEventHandler SlikeRowDeleted;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn IDColumn
			{
				get
				{
					return this.columnID;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn NrColumn
			{
				get
				{
					return this.columnNr;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn ImeColumn
			{
				get
				{
					return this.columnIme;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn VrstniRedColumn
			{
				get
				{
					return this.columnVrstniRed;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn ZoomColumn
			{
				get
				{
					return this.columnZoom;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn XColumn
			{
				get
				{
					return this.columnX;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn YColumn
			{
				get
				{
					return this.columnY;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn VisibleColumn
			{
				get
				{
					return this.columnVisible;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Browsable(false), DebuggerNonUserCode]
			public int Count
			{
				get
				{
					return base.Rows.Count;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.SlikeRow this[int index]
			{
				get
				{
					return (KatalogUmetnin_dbDataSet.SlikeRow)base.Rows[index];
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public SlikeDataTable()
			{
				base.TableName = "Slike";
				this.BeginInit();
				this.InitClass();
				this.EndInit();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal SlikeDataTable(DataTable table)
			{
				base.TableName = table.TableName;
				if (table.CaseSensitive != table.DataSet.CaseSensitive)
				{
					base.CaseSensitive = table.CaseSensitive;
				}
				if (table.Locale.ToString() != table.DataSet.Locale.ToString())
				{
					base.Locale = table.Locale;
				}
				if (table.Namespace != table.DataSet.Namespace)
				{
					base.Namespace = table.Namespace;
				}
				base.Prefix = table.Prefix;
				base.MinimumCapacity = table.MinimumCapacity;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected SlikeDataTable(SerializationInfo info, StreamingContext context) : base(info, context)
			{
				this.InitVars();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void AddSlikeRow(KatalogUmetnin_dbDataSet.SlikeRow row)
			{
				base.Rows.Add(row);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.SlikeRow AddSlikeRow(int Nr, string Ime, int VrstniRed, double Zoom, int X, int Y, bool Visible)
			{
				KatalogUmetnin_dbDataSet.SlikeRow rowSlikeRow = (KatalogUmetnin_dbDataSet.SlikeRow)base.NewRow();
				object[] columnValuesArray = new object[]
				{
					null,
					Nr,
					Ime,
					VrstniRed,
					Zoom,
					X,
					Y,
					Visible
				};
				rowSlikeRow.ItemArray = columnValuesArray;
				base.Rows.Add(rowSlikeRow);
				return rowSlikeRow;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.SlikeRow FindByID(int ID)
			{
				return (KatalogUmetnin_dbDataSet.SlikeRow)base.Rows.Find(new object[]
				{
					ID
				});
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public override DataTable Clone()
			{
				KatalogUmetnin_dbDataSet.SlikeDataTable cln = (KatalogUmetnin_dbDataSet.SlikeDataTable)base.Clone();
				cln.InitVars();
				return cln;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override DataTable CreateInstance()
			{
				return new KatalogUmetnin_dbDataSet.SlikeDataTable();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal void InitVars()
			{
				this.columnID = base.Columns["ID"];
				this.columnNr = base.Columns["Nr"];
				this.columnIme = base.Columns["Ime"];
				this.columnVrstniRed = base.Columns["VrstniRed"];
				this.columnZoom = base.Columns["Zoom"];
				this.columnX = base.Columns["X"];
				this.columnY = base.Columns["Y"];
				this.columnVisible = base.Columns["Visible"];
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			private void InitClass()
			{
				this.columnID = new DataColumn("ID", typeof(int), null, MappingType.Element);
				base.Columns.Add(this.columnID);
				this.columnNr = new DataColumn("Nr", typeof(int), null, MappingType.Element);
				base.Columns.Add(this.columnNr);
				this.columnIme = new DataColumn("Ime", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnIme);
				this.columnVrstniRed = new DataColumn("VrstniRed", typeof(int), null, MappingType.Element);
				base.Columns.Add(this.columnVrstniRed);
				this.columnZoom = new DataColumn("Zoom", typeof(double), null, MappingType.Element);
				base.Columns.Add(this.columnZoom);
				this.columnX = new DataColumn("X", typeof(int), null, MappingType.Element);
				base.Columns.Add(this.columnX);
				this.columnY = new DataColumn("Y", typeof(int), null, MappingType.Element);
				base.Columns.Add(this.columnY);
				this.columnVisible = new DataColumn("Visible", typeof(bool), null, MappingType.Element);
				base.Columns.Add(this.columnVisible);
				base.Constraints.Add(new UniqueConstraint("Constraint1", new DataColumn[]
				{
					this.columnID
				}, true));
				this.columnID.AutoIncrement = true;
				this.columnID.AutoIncrementSeed = -1L;
				this.columnID.AutoIncrementStep = -1L;
				this.columnID.AllowDBNull = false;
				this.columnID.Unique = true;
				this.columnIme.MaxLength = 255;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.SlikeRow NewSlikeRow()
			{
				return (KatalogUmetnin_dbDataSet.SlikeRow)base.NewRow();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
			{
				return new KatalogUmetnin_dbDataSet.SlikeRow(builder);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override Type GetRowType()
			{
				return typeof(KatalogUmetnin_dbDataSet.SlikeRow);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowChanged(DataRowChangeEventArgs e)
			{
				base.OnRowChanged(e);
				if (this.SlikeRowChanged != null)
				{
					this.SlikeRowChanged(this, new KatalogUmetnin_dbDataSet.SlikeRowChangeEvent((KatalogUmetnin_dbDataSet.SlikeRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowChanging(DataRowChangeEventArgs e)
			{
				base.OnRowChanging(e);
				if (this.SlikeRowChanging != null)
				{
					this.SlikeRowChanging(this, new KatalogUmetnin_dbDataSet.SlikeRowChangeEvent((KatalogUmetnin_dbDataSet.SlikeRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowDeleted(DataRowChangeEventArgs e)
			{
				base.OnRowDeleted(e);
				if (this.SlikeRowDeleted != null)
				{
					this.SlikeRowDeleted(this, new KatalogUmetnin_dbDataSet.SlikeRowChangeEvent((KatalogUmetnin_dbDataSet.SlikeRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowDeleting(DataRowChangeEventArgs e)
			{
				base.OnRowDeleting(e);
				if (this.SlikeRowDeleting != null)
				{
					this.SlikeRowDeleting(this, new KatalogUmetnin_dbDataSet.SlikeRowChangeEvent((KatalogUmetnin_dbDataSet.SlikeRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void RemoveSlikeRow(KatalogUmetnin_dbDataSet.SlikeRow row)
			{
				base.Rows.Remove(row);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
			{
				XmlSchemaComplexType type = new XmlSchemaComplexType();
				XmlSchemaSequence sequence = new XmlSchemaSequence();
				KatalogUmetnin_dbDataSet ds = new KatalogUmetnin_dbDataSet();
				XmlSchemaAny any = new XmlSchemaAny();
				any.Namespace = "http://www.w3.org/2001/XMLSchema";
				any.MinOccurs = 0m;
				any.MaxOccurs = 79228162514264337593543950335m;
				any.ProcessContents = XmlSchemaContentProcessing.Lax;
				sequence.Items.Add(any);
				XmlSchemaAny any2 = new XmlSchemaAny();
				any2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
				any2.MinOccurs = 1m;
				any2.ProcessContents = XmlSchemaContentProcessing.Lax;
				sequence.Items.Add(any2);
				XmlSchemaAttribute attribute = new XmlSchemaAttribute();
				attribute.Name = "namespace";
				attribute.FixedValue = ds.Namespace;
				type.Attributes.Add(attribute);
				XmlSchemaAttribute attribute2 = new XmlSchemaAttribute();
				attribute2.Name = "tableTypeName";
				attribute2.FixedValue = "SlikeDataTable";
				type.Attributes.Add(attribute2);
				type.Particle = sequence;
				XmlSchema dsSchema = ds.GetSchemaSerializable();
				XmlSchemaComplexType result;
				if (xs.Contains(dsSchema.TargetNamespace))
				{
					MemoryStream s = new MemoryStream();
					MemoryStream s2 = new MemoryStream();
					try
					{
						dsSchema.Write(s);
						IEnumerator schemas = xs.Schemas(dsSchema.TargetNamespace).GetEnumerator();
						while (schemas.MoveNext())
						{
							XmlSchema schema = (XmlSchema)schemas.Current;
							s2.SetLength(0L);
							schema.Write(s2);
							if (s.Length == s2.Length)
							{
								s.Position = 0L;
								s2.Position = 0L;
								while (s.Position != s.Length && s.ReadByte() == s2.ReadByte())
								{
								}
								if (s.Position == s.Length)
								{
									result = type;
									return result;
								}
							}
						}
					}
					finally
					{
						if (s != null)
						{
							s.Close();
						}
						if (s2 != null)
						{
							s2.Close();
						}
					}
				}
				xs.Add(dsSchema);
				result = type;
				return result;
			}
		}

		[XmlSchemaProvider("GetTypedTableSchema")]
		[Serializable]
		public class UmetnineStUmDelaDataTable : TypedTableBase<KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow>
		{
			private DataColumn columnst_um_dela;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.UmetnineStUmDelaRowChangeEventHandler UmetnineStUmDelaRowChanging;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.UmetnineStUmDelaRowChangeEventHandler UmetnineStUmDelaRowChanged;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.UmetnineStUmDelaRowChangeEventHandler UmetnineStUmDelaRowDeleting;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.UmetnineStUmDelaRowChangeEventHandler UmetnineStUmDelaRowDeleted;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn st_um_delaColumn
			{
				get
				{
					return this.columnst_um_dela;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Browsable(false), DebuggerNonUserCode]
			public int Count
			{
				get
				{
					return base.Rows.Count;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow this[int index]
			{
				get
				{
					return (KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow)base.Rows[index];
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public UmetnineStUmDelaDataTable()
			{
				base.TableName = "UmetnineStUmDela";
				this.BeginInit();
				this.InitClass();
				this.EndInit();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal UmetnineStUmDelaDataTable(DataTable table)
			{
				base.TableName = table.TableName;
				if (table.CaseSensitive != table.DataSet.CaseSensitive)
				{
					base.CaseSensitive = table.CaseSensitive;
				}
				if (table.Locale.ToString() != table.DataSet.Locale.ToString())
				{
					base.Locale = table.Locale;
				}
				if (table.Namespace != table.DataSet.Namespace)
				{
					base.Namespace = table.Namespace;
				}
				base.Prefix = table.Prefix;
				base.MinimumCapacity = table.MinimumCapacity;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected UmetnineStUmDelaDataTable(SerializationInfo info, StreamingContext context) : base(info, context)
			{
				this.InitVars();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void AddUmetnineStUmDelaRow(KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow row)
			{
				base.Rows.Add(row);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow AddUmetnineStUmDelaRow(string st_um_dela)
			{
				KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow rowUmetnineStUmDelaRow = (KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow)base.NewRow();
				object[] columnValuesArray = new object[]
				{
					st_um_dela
				};
				rowUmetnineStUmDelaRow.ItemArray = columnValuesArray;
				base.Rows.Add(rowUmetnineStUmDelaRow);
				return rowUmetnineStUmDelaRow;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public override DataTable Clone()
			{
				KatalogUmetnin_dbDataSet.UmetnineStUmDelaDataTable cln = (KatalogUmetnin_dbDataSet.UmetnineStUmDelaDataTable)base.Clone();
				cln.InitVars();
				return cln;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override DataTable CreateInstance()
			{
				return new KatalogUmetnin_dbDataSet.UmetnineStUmDelaDataTable();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal void InitVars()
			{
				this.columnst_um_dela = base.Columns["st_um_dela"];
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			private void InitClass()
			{
				this.columnst_um_dela = new DataColumn("st_um_dela", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnst_um_dela);
				this.columnst_um_dela.MaxLength = 255;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow NewUmetnineStUmDelaRow()
			{
				return (KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow)base.NewRow();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
			{
				return new KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow(builder);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override Type GetRowType()
			{
				return typeof(KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowChanged(DataRowChangeEventArgs e)
			{
				base.OnRowChanged(e);
				if (this.UmetnineStUmDelaRowChanged != null)
				{
					this.UmetnineStUmDelaRowChanged(this, new KatalogUmetnin_dbDataSet.UmetnineStUmDelaRowChangeEvent((KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowChanging(DataRowChangeEventArgs e)
			{
				base.OnRowChanging(e);
				if (this.UmetnineStUmDelaRowChanging != null)
				{
					this.UmetnineStUmDelaRowChanging(this, new KatalogUmetnin_dbDataSet.UmetnineStUmDelaRowChangeEvent((KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowDeleted(DataRowChangeEventArgs e)
			{
				base.OnRowDeleted(e);
				if (this.UmetnineStUmDelaRowDeleted != null)
				{
					this.UmetnineStUmDelaRowDeleted(this, new KatalogUmetnin_dbDataSet.UmetnineStUmDelaRowChangeEvent((KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowDeleting(DataRowChangeEventArgs e)
			{
				base.OnRowDeleting(e);
				if (this.UmetnineStUmDelaRowDeleting != null)
				{
					this.UmetnineStUmDelaRowDeleting(this, new KatalogUmetnin_dbDataSet.UmetnineStUmDelaRowChangeEvent((KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void RemoveUmetnineStUmDelaRow(KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow row)
			{
				base.Rows.Remove(row);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
			{
				XmlSchemaComplexType type = new XmlSchemaComplexType();
				XmlSchemaSequence sequence = new XmlSchemaSequence();
				KatalogUmetnin_dbDataSet ds = new KatalogUmetnin_dbDataSet();
				XmlSchemaAny any = new XmlSchemaAny();
				any.Namespace = "http://www.w3.org/2001/XMLSchema";
				any.MinOccurs = 0m;
				any.MaxOccurs = 79228162514264337593543950335m;
				any.ProcessContents = XmlSchemaContentProcessing.Lax;
				sequence.Items.Add(any);
				XmlSchemaAny any2 = new XmlSchemaAny();
				any2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
				any2.MinOccurs = 1m;
				any2.ProcessContents = XmlSchemaContentProcessing.Lax;
				sequence.Items.Add(any2);
				XmlSchemaAttribute attribute = new XmlSchemaAttribute();
				attribute.Name = "namespace";
				attribute.FixedValue = ds.Namespace;
				type.Attributes.Add(attribute);
				XmlSchemaAttribute attribute2 = new XmlSchemaAttribute();
				attribute2.Name = "tableTypeName";
				attribute2.FixedValue = "UmetnineStUmDelaDataTable";
				type.Attributes.Add(attribute2);
				type.Particle = sequence;
				XmlSchema dsSchema = ds.GetSchemaSerializable();
				XmlSchemaComplexType result;
				if (xs.Contains(dsSchema.TargetNamespace))
				{
					MemoryStream s = new MemoryStream();
					MemoryStream s2 = new MemoryStream();
					try
					{
						dsSchema.Write(s);
						IEnumerator schemas = xs.Schemas(dsSchema.TargetNamespace).GetEnumerator();
						while (schemas.MoveNext())
						{
							XmlSchema schema = (XmlSchema)schemas.Current;
							s2.SetLength(0L);
							schema.Write(s2);
							if (s.Length == s2.Length)
							{
								s.Position = 0L;
								s2.Position = 0L;
								while (s.Position != s.Length && s.ReadByte() == s2.ReadByte())
								{
								}
								if (s.Position == s.Length)
								{
									result = type;
									return result;
								}
							}
						}
					}
					finally
					{
						if (s != null)
						{
							s.Close();
						}
						if (s2 != null)
						{
							s2.Close();
						}
					}
				}
				xs.Add(dsSchema);
				result = type;
				return result;
			}
		}

		[XmlSchemaProvider("GetTypedTableSchema")]
		[Serializable]
		public class NapisiDataTable : TypedTableBase<KatalogUmetnin_dbDataSet.NapisiRow>
		{
			private DataColumn columnNr;

			private DataColumn columnIDNapisa;

			private DataColumn columnTekst;

			private DataColumn columnFont;

			private DataColumn columnColor;

			private DataColumn columnSize;

			private DataColumn columnX;

			private DataColumn columnY;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.NapisiRowChangeEventHandler NapisiRowChanging;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.NapisiRowChangeEventHandler NapisiRowChanged;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.NapisiRowChangeEventHandler NapisiRowDeleting;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.NapisiRowChangeEventHandler NapisiRowDeleted;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn NrColumn
			{
				get
				{
					return this.columnNr;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn IDNapisaColumn
			{
				get
				{
					return this.columnIDNapisa;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn TekstColumn
			{
				get
				{
					return this.columnTekst;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn FontColumn
			{
				get
				{
					return this.columnFont;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn ColorColumn
			{
				get
				{
					return this.columnColor;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn SizeColumn
			{
				get
				{
					return this.columnSize;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn XColumn
			{
				get
				{
					return this.columnX;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn YColumn
			{
				get
				{
					return this.columnY;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Browsable(false), DebuggerNonUserCode]
			public int Count
			{
				get
				{
					return base.Rows.Count;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.NapisiRow this[int index]
			{
				get
				{
					return (KatalogUmetnin_dbDataSet.NapisiRow)base.Rows[index];
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public NapisiDataTable()
			{
				base.TableName = "Napisi";
				this.BeginInit();
				this.InitClass();
				this.EndInit();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal NapisiDataTable(DataTable table)
			{
				base.TableName = table.TableName;
				if (table.CaseSensitive != table.DataSet.CaseSensitive)
				{
					base.CaseSensitive = table.CaseSensitive;
				}
				if (table.Locale.ToString() != table.DataSet.Locale.ToString())
				{
					base.Locale = table.Locale;
				}
				if (table.Namespace != table.DataSet.Namespace)
				{
					base.Namespace = table.Namespace;
				}
				base.Prefix = table.Prefix;
				base.MinimumCapacity = table.MinimumCapacity;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected NapisiDataTable(SerializationInfo info, StreamingContext context) : base(info, context)
			{
				this.InitVars();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void AddNapisiRow(KatalogUmetnin_dbDataSet.NapisiRow row)
			{
				base.Rows.Add(row);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.NapisiRow AddNapisiRow(int Nr, int IDNapisa, string Tekst, string Font, string Color, int Size, int X, int Y)
			{
				KatalogUmetnin_dbDataSet.NapisiRow rowNapisiRow = (KatalogUmetnin_dbDataSet.NapisiRow)base.NewRow();
				object[] columnValuesArray = new object[]
				{
					Nr,
					IDNapisa,
					Tekst,
					Font,
					Color,
					Size,
					X,
					Y
				};
				rowNapisiRow.ItemArray = columnValuesArray;
				base.Rows.Add(rowNapisiRow);
				return rowNapisiRow;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.NapisiRow FindByNrIDNapisa(int Nr, int IDNapisa)
			{
				return (KatalogUmetnin_dbDataSet.NapisiRow)base.Rows.Find(new object[]
				{
					Nr,
					IDNapisa
				});
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public override DataTable Clone()
			{
				KatalogUmetnin_dbDataSet.NapisiDataTable cln = (KatalogUmetnin_dbDataSet.NapisiDataTable)base.Clone();
				cln.InitVars();
				return cln;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override DataTable CreateInstance()
			{
				return new KatalogUmetnin_dbDataSet.NapisiDataTable();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal void InitVars()
			{
				this.columnNr = base.Columns["Nr"];
				this.columnIDNapisa = base.Columns["IDNapisa"];
				this.columnTekst = base.Columns["Tekst"];
				this.columnFont = base.Columns["Font"];
				this.columnColor = base.Columns["Color"];
				this.columnSize = base.Columns["Size"];
				this.columnX = base.Columns["X"];
				this.columnY = base.Columns["Y"];
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			private void InitClass()
			{
				this.columnNr = new DataColumn("Nr", typeof(int), null, MappingType.Element);
				base.Columns.Add(this.columnNr);
				this.columnIDNapisa = new DataColumn("IDNapisa", typeof(int), null, MappingType.Element);
				base.Columns.Add(this.columnIDNapisa);
				this.columnTekst = new DataColumn("Tekst", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnTekst);
				this.columnFont = new DataColumn("Font", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnFont);
				this.columnColor = new DataColumn("Color", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnColor);
				this.columnSize = new DataColumn("Size", typeof(int), null, MappingType.Element);
				base.Columns.Add(this.columnSize);
				this.columnX = new DataColumn("X", typeof(int), null, MappingType.Element);
				base.Columns.Add(this.columnX);
				this.columnY = new DataColumn("Y", typeof(int), null, MappingType.Element);
				base.Columns.Add(this.columnY);
				base.Constraints.Add(new UniqueConstraint("Constraint1", new DataColumn[]
				{
					this.columnNr,
					this.columnIDNapisa
				}, true));
				this.columnNr.AllowDBNull = false;
				this.columnIDNapisa.AllowDBNull = false;
				this.columnTekst.MaxLength = 536870910;
				this.columnFont.MaxLength = 255;
				this.columnColor.MaxLength = 255;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.NapisiRow NewNapisiRow()
			{
				return (KatalogUmetnin_dbDataSet.NapisiRow)base.NewRow();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
			{
				return new KatalogUmetnin_dbDataSet.NapisiRow(builder);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override Type GetRowType()
			{
				return typeof(KatalogUmetnin_dbDataSet.NapisiRow);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowChanged(DataRowChangeEventArgs e)
			{
				base.OnRowChanged(e);
				if (this.NapisiRowChanged != null)
				{
					this.NapisiRowChanged(this, new KatalogUmetnin_dbDataSet.NapisiRowChangeEvent((KatalogUmetnin_dbDataSet.NapisiRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowChanging(DataRowChangeEventArgs e)
			{
				base.OnRowChanging(e);
				if (this.NapisiRowChanging != null)
				{
					this.NapisiRowChanging(this, new KatalogUmetnin_dbDataSet.NapisiRowChangeEvent((KatalogUmetnin_dbDataSet.NapisiRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowDeleted(DataRowChangeEventArgs e)
			{
				base.OnRowDeleted(e);
				if (this.NapisiRowDeleted != null)
				{
					this.NapisiRowDeleted(this, new KatalogUmetnin_dbDataSet.NapisiRowChangeEvent((KatalogUmetnin_dbDataSet.NapisiRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowDeleting(DataRowChangeEventArgs e)
			{
				base.OnRowDeleting(e);
				if (this.NapisiRowDeleting != null)
				{
					this.NapisiRowDeleting(this, new KatalogUmetnin_dbDataSet.NapisiRowChangeEvent((KatalogUmetnin_dbDataSet.NapisiRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void RemoveNapisiRow(KatalogUmetnin_dbDataSet.NapisiRow row)
			{
				base.Rows.Remove(row);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
			{
				XmlSchemaComplexType type = new XmlSchemaComplexType();
				XmlSchemaSequence sequence = new XmlSchemaSequence();
				KatalogUmetnin_dbDataSet ds = new KatalogUmetnin_dbDataSet();
				XmlSchemaAny any = new XmlSchemaAny();
				any.Namespace = "http://www.w3.org/2001/XMLSchema";
				any.MinOccurs = 0m;
				any.MaxOccurs = 79228162514264337593543950335m;
				any.ProcessContents = XmlSchemaContentProcessing.Lax;
				sequence.Items.Add(any);
				XmlSchemaAny any2 = new XmlSchemaAny();
				any2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
				any2.MinOccurs = 1m;
				any2.ProcessContents = XmlSchemaContentProcessing.Lax;
				sequence.Items.Add(any2);
				XmlSchemaAttribute attribute = new XmlSchemaAttribute();
				attribute.Name = "namespace";
				attribute.FixedValue = ds.Namespace;
				type.Attributes.Add(attribute);
				XmlSchemaAttribute attribute2 = new XmlSchemaAttribute();
				attribute2.Name = "tableTypeName";
				attribute2.FixedValue = "NapisiDataTable";
				type.Attributes.Add(attribute2);
				type.Particle = sequence;
				XmlSchema dsSchema = ds.GetSchemaSerializable();
				XmlSchemaComplexType result;
				if (xs.Contains(dsSchema.TargetNamespace))
				{
					MemoryStream s = new MemoryStream();
					MemoryStream s2 = new MemoryStream();
					try
					{
						dsSchema.Write(s);
						IEnumerator schemas = xs.Schemas(dsSchema.TargetNamespace).GetEnumerator();
						while (schemas.MoveNext())
						{
							XmlSchema schema = (XmlSchema)schemas.Current;
							s2.SetLength(0L);
							schema.Write(s2);
							if (s.Length == s2.Length)
							{
								s.Position = 0L;
								s2.Position = 0L;
								while (s.Position != s.Length && s.ReadByte() == s2.ReadByte())
								{
								}
								if (s.Position == s.Length)
								{
									result = type;
									return result;
								}
							}
						}
					}
					finally
					{
						if (s != null)
						{
							s.Close();
						}
						if (s2 != null)
						{
							s2.Close();
						}
					}
				}
				xs.Add(dsSchema);
				result = type;
				return result;
			}
		}

		[XmlSchemaProvider("GetTypedTableSchema")]
		[Serializable]
		public class UmetnineOpisDataTable : TypedTableBase<KatalogUmetnin_dbDataSet.UmetnineOpisRow>
		{
			private DataColumn columnNr;

			private DataColumn columnopis;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.UmetnineOpisRowChangeEventHandler UmetnineOpisRowChanging;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.UmetnineOpisRowChangeEventHandler UmetnineOpisRowChanged;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.UmetnineOpisRowChangeEventHandler UmetnineOpisRowDeleting;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
			public event KatalogUmetnin_dbDataSet.UmetnineOpisRowChangeEventHandler UmetnineOpisRowDeleted;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn NrColumn
			{
				get
				{
					return this.columnNr;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataColumn opisColumn
			{
				get
				{
					return this.columnopis;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Browsable(false), DebuggerNonUserCode]
			public int Count
			{
				get
				{
					return base.Rows.Count;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineOpisRow this[int index]
			{
				get
				{
					return (KatalogUmetnin_dbDataSet.UmetnineOpisRow)base.Rows[index];
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public UmetnineOpisDataTable()
			{
				base.TableName = "UmetnineOpis";
				this.BeginInit();
				this.InitClass();
				this.EndInit();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal UmetnineOpisDataTable(DataTable table)
			{
				base.TableName = table.TableName;
				if (table.CaseSensitive != table.DataSet.CaseSensitive)
				{
					base.CaseSensitive = table.CaseSensitive;
				}
				if (table.Locale.ToString() != table.DataSet.Locale.ToString())
				{
					base.Locale = table.Locale;
				}
				if (table.Namespace != table.DataSet.Namespace)
				{
					base.Namespace = table.Namespace;
				}
				base.Prefix = table.Prefix;
				base.MinimumCapacity = table.MinimumCapacity;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected UmetnineOpisDataTable(SerializationInfo info, StreamingContext context) : base(info, context)
			{
				this.InitVars();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void AddUmetnineOpisRow(KatalogUmetnin_dbDataSet.UmetnineOpisRow row)
			{
				base.Rows.Add(row);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineOpisRow AddUmetnineOpisRow(string opis)
			{
				KatalogUmetnin_dbDataSet.UmetnineOpisRow rowUmetnineOpisRow = (KatalogUmetnin_dbDataSet.UmetnineOpisRow)base.NewRow();
				object[] columnValuesArray = new object[]
				{
					null,
					opis
				};
				rowUmetnineOpisRow.ItemArray = columnValuesArray;
				base.Rows.Add(rowUmetnineOpisRow);
				return rowUmetnineOpisRow;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineOpisRow FindByNr(int Nr)
			{
				return (KatalogUmetnin_dbDataSet.UmetnineOpisRow)base.Rows.Find(new object[]
				{
					Nr
				});
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public override DataTable Clone()
			{
				KatalogUmetnin_dbDataSet.UmetnineOpisDataTable cln = (KatalogUmetnin_dbDataSet.UmetnineOpisDataTable)base.Clone();
				cln.InitVars();
				return cln;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override DataTable CreateInstance()
			{
				return new KatalogUmetnin_dbDataSet.UmetnineOpisDataTable();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal void InitVars()
			{
				this.columnNr = base.Columns["Nr"];
				this.columnopis = base.Columns["opis"];
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			private void InitClass()
			{
				this.columnNr = new DataColumn("Nr", typeof(int), null, MappingType.Element);
				base.Columns.Add(this.columnNr);
				this.columnopis = new DataColumn("opis", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnopis);
				base.Constraints.Add(new UniqueConstraint("Constraint1", new DataColumn[]
				{
					this.columnNr
				}, true));
				this.columnNr.AutoIncrement = true;
				this.columnNr.AutoIncrementSeed = -1L;
				this.columnNr.AutoIncrementStep = -1L;
				this.columnNr.AllowDBNull = false;
				this.columnNr.Unique = true;
				this.columnopis.MaxLength = 255;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineOpisRow NewUmetnineOpisRow()
			{
				return (KatalogUmetnin_dbDataSet.UmetnineOpisRow)base.NewRow();
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
			{
				return new KatalogUmetnin_dbDataSet.UmetnineOpisRow(builder);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override Type GetRowType()
			{
				return typeof(KatalogUmetnin_dbDataSet.UmetnineOpisRow);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowChanged(DataRowChangeEventArgs e)
			{
				base.OnRowChanged(e);
				if (this.UmetnineOpisRowChanged != null)
				{
					this.UmetnineOpisRowChanged(this, new KatalogUmetnin_dbDataSet.UmetnineOpisRowChangeEvent((KatalogUmetnin_dbDataSet.UmetnineOpisRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowChanging(DataRowChangeEventArgs e)
			{
				base.OnRowChanging(e);
				if (this.UmetnineOpisRowChanging != null)
				{
					this.UmetnineOpisRowChanging(this, new KatalogUmetnin_dbDataSet.UmetnineOpisRowChangeEvent((KatalogUmetnin_dbDataSet.UmetnineOpisRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowDeleted(DataRowChangeEventArgs e)
			{
				base.OnRowDeleted(e);
				if (this.UmetnineOpisRowDeleted != null)
				{
					this.UmetnineOpisRowDeleted(this, new KatalogUmetnin_dbDataSet.UmetnineOpisRowChangeEvent((KatalogUmetnin_dbDataSet.UmetnineOpisRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			protected override void OnRowDeleting(DataRowChangeEventArgs e)
			{
				base.OnRowDeleting(e);
				if (this.UmetnineOpisRowDeleting != null)
				{
					this.UmetnineOpisRowDeleting(this, new KatalogUmetnin_dbDataSet.UmetnineOpisRowChangeEvent((KatalogUmetnin_dbDataSet.UmetnineOpisRow)e.Row, e.Action));
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void RemoveUmetnineOpisRow(KatalogUmetnin_dbDataSet.UmetnineOpisRow row)
			{
				base.Rows.Remove(row);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
			{
				XmlSchemaComplexType type = new XmlSchemaComplexType();
				XmlSchemaSequence sequence = new XmlSchemaSequence();
				KatalogUmetnin_dbDataSet ds = new KatalogUmetnin_dbDataSet();
				XmlSchemaAny any = new XmlSchemaAny();
				any.Namespace = "http://www.w3.org/2001/XMLSchema";
				any.MinOccurs = 0m;
				any.MaxOccurs = 79228162514264337593543950335m;
				any.ProcessContents = XmlSchemaContentProcessing.Lax;
				sequence.Items.Add(any);
				XmlSchemaAny any2 = new XmlSchemaAny();
				any2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
				any2.MinOccurs = 1m;
				any2.ProcessContents = XmlSchemaContentProcessing.Lax;
				sequence.Items.Add(any2);
				XmlSchemaAttribute attribute = new XmlSchemaAttribute();
				attribute.Name = "namespace";
				attribute.FixedValue = ds.Namespace;
				type.Attributes.Add(attribute);
				XmlSchemaAttribute attribute2 = new XmlSchemaAttribute();
				attribute2.Name = "tableTypeName";
				attribute2.FixedValue = "UmetnineOpisDataTable";
				type.Attributes.Add(attribute2);
				type.Particle = sequence;
				XmlSchema dsSchema = ds.GetSchemaSerializable();
				XmlSchemaComplexType result;
				if (xs.Contains(dsSchema.TargetNamespace))
				{
					MemoryStream s = new MemoryStream();
					MemoryStream s2 = new MemoryStream();
					try
					{
						dsSchema.Write(s);
						IEnumerator schemas = xs.Schemas(dsSchema.TargetNamespace).GetEnumerator();
						while (schemas.MoveNext())
						{
							XmlSchema schema = (XmlSchema)schemas.Current;
							s2.SetLength(0L);
							schema.Write(s2);
							if (s.Length == s2.Length)
							{
								s.Position = 0L;
								s2.Position = 0L;
								while (s.Position != s.Length && s.ReadByte() == s2.ReadByte())
								{
								}
								if (s.Position == s.Length)
								{
									result = type;
									return result;
								}
							}
						}
					}
					finally
					{
						if (s != null)
						{
							s.Close();
						}
						if (s2 != null)
						{
							s2.Close();
						}
					}
				}
				xs.Add(dsSchema);
				result = type;
				return result;
			}
		}

		public class UmetnineRow : DataRow
		{
			private KatalogUmetnin_dbDataSet.UmetnineDataTable tableUmetnine;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public int Nr
			{
				get
				{
					return (int)base[this.tableUmetnine.NrColumn];
				}
				set
				{
					base[this.tableUmetnine.NrColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string st_um_dela
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableUmetnine.st_um_delaColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'st_um_dela' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.st_um_delaColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string naslov
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableUmetnine.naslovColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'naslov' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.naslovColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string leto_izdelave
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableUmetnine.leto_izdelaveColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'leto_izdelave' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.leto_izdelaveColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string material
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableUmetnine.materialColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'material' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.materialColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string velikost
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableUmetnine.velikostColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'velikost' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.velikostColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string zaznamek
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableUmetnine.zaznamekColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'zaznamek' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.zaznamekColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string st_negativ
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableUmetnine.st_negativColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'st_negativ' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.st_negativColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public decimal cena
			{
				get
				{
					decimal result;
					try
					{
						result = (decimal)base[this.tableUmetnine.cenaColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'cena' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.cenaColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public decimal cena_dem
			{
				get
				{
					decimal result;
					try
					{
						result = (decimal)base[this.tableUmetnine.cena_demColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'cena_dem' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.cena_demColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string st_slik
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableUmetnine.st_slikColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'st_slik' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.st_slikColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string opis
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableUmetnine.opisColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'opis' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.opisColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string ID
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableUmetnine.IDColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'ID' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.IDColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DateTime datum
			{
				get
				{
					DateTime result;
					try
					{
						result = (DateTime)base[this.tableUmetnine.datumColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'datum' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.datumColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public decimal izkupicek
			{
				get
				{
					decimal result;
					try
					{
						result = (decimal)base[this.tableUmetnine.izkupicekColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'izkupicek' in table 'Umetnine' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnine.izkupicekColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal UmetnineRow(DataRowBuilder rb) : base(rb)
			{
				this.tableUmetnine = (KatalogUmetnin_dbDataSet.UmetnineDataTable)base.Table;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool Isst_um_delaNull()
			{
				return base.IsNull(this.tableUmetnine.st_um_delaColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void Setst_um_delaNull()
			{
				base[this.tableUmetnine.st_um_delaColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsnaslovNull()
			{
				return base.IsNull(this.tableUmetnine.naslovColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetnaslovNull()
			{
				base[this.tableUmetnine.naslovColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool Isleto_izdelaveNull()
			{
				return base.IsNull(this.tableUmetnine.leto_izdelaveColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void Setleto_izdelaveNull()
			{
				base[this.tableUmetnine.leto_izdelaveColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsmaterialNull()
			{
				return base.IsNull(this.tableUmetnine.materialColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetmaterialNull()
			{
				base[this.tableUmetnine.materialColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsvelikostNull()
			{
				return base.IsNull(this.tableUmetnine.velikostColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetvelikostNull()
			{
				base[this.tableUmetnine.velikostColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IszaznamekNull()
			{
				return base.IsNull(this.tableUmetnine.zaznamekColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetzaznamekNull()
			{
				base[this.tableUmetnine.zaznamekColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool Isst_negativNull()
			{
				return base.IsNull(this.tableUmetnine.st_negativColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void Setst_negativNull()
			{
				base[this.tableUmetnine.st_negativColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IscenaNull()
			{
				return base.IsNull(this.tableUmetnine.cenaColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetcenaNull()
			{
				base[this.tableUmetnine.cenaColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool Iscena_demNull()
			{
				return base.IsNull(this.tableUmetnine.cena_demColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void Setcena_demNull()
			{
				base[this.tableUmetnine.cena_demColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool Isst_slikNull()
			{
				return base.IsNull(this.tableUmetnine.st_slikColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void Setst_slikNull()
			{
				base[this.tableUmetnine.st_slikColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsopisNull()
			{
				return base.IsNull(this.tableUmetnine.opisColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetopisNull()
			{
				base[this.tableUmetnine.opisColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsIDNull()
			{
				return base.IsNull(this.tableUmetnine.IDColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetIDNull()
			{
				base[this.tableUmetnine.IDColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsdatumNull()
			{
				return base.IsNull(this.tableUmetnine.datumColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetdatumNull()
			{
				base[this.tableUmetnine.datumColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsizkupicekNull()
			{
				return base.IsNull(this.tableUmetnine.izkupicekColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetizkupicekNull()
			{
				base[this.tableUmetnine.izkupicekColumn] = Convert.DBNull;
			}
		}

		public class SlikeRow : DataRow
		{
			private KatalogUmetnin_dbDataSet.SlikeDataTable tableSlike;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public int ID
			{
				get
				{
					return (int)base[this.tableSlike.IDColumn];
				}
				set
				{
					base[this.tableSlike.IDColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public int Nr
			{
				get
				{
					int result;
					try
					{
						result = (int)base[this.tableSlike.NrColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'Nr' in table 'Slike' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableSlike.NrColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string Ime
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableSlike.ImeColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'Ime' in table 'Slike' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableSlike.ImeColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public int VrstniRed
			{
				get
				{
					int result;
					try
					{
						result = (int)base[this.tableSlike.VrstniRedColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'VrstniRed' in table 'Slike' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableSlike.VrstniRedColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public double Zoom
			{
				get
				{
					double result;
					try
					{
						result = (double)base[this.tableSlike.ZoomColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'Zoom' in table 'Slike' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableSlike.ZoomColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public int X
			{
				get
				{
					int result;
					try
					{
						result = (int)base[this.tableSlike.XColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'X' in table 'Slike' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableSlike.XColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public int Y
			{
				get
				{
					int result;
					try
					{
						result = (int)base[this.tableSlike.YColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'Y' in table 'Slike' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableSlike.YColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool Visible
			{
				get
				{
					bool result;
					try
					{
						result = (bool)base[this.tableSlike.VisibleColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'Visible' in table 'Slike' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableSlike.VisibleColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal SlikeRow(DataRowBuilder rb) : base(rb)
			{
				this.tableSlike = (KatalogUmetnin_dbDataSet.SlikeDataTable)base.Table;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsNrNull()
			{
				return base.IsNull(this.tableSlike.NrColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetNrNull()
			{
				base[this.tableSlike.NrColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsImeNull()
			{
				return base.IsNull(this.tableSlike.ImeColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetImeNull()
			{
				base[this.tableSlike.ImeColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsVrstniRedNull()
			{
				return base.IsNull(this.tableSlike.VrstniRedColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetVrstniRedNull()
			{
				base[this.tableSlike.VrstniRedColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsZoomNull()
			{
				return base.IsNull(this.tableSlike.ZoomColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetZoomNull()
			{
				base[this.tableSlike.ZoomColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsXNull()
			{
				return base.IsNull(this.tableSlike.XColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetXNull()
			{
				base[this.tableSlike.XColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsYNull()
			{
				return base.IsNull(this.tableSlike.YColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetYNull()
			{
				base[this.tableSlike.YColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsVisibleNull()
			{
				return base.IsNull(this.tableSlike.VisibleColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetVisibleNull()
			{
				base[this.tableSlike.VisibleColumn] = Convert.DBNull;
			}
		}

		public class UmetnineStUmDelaRow : DataRow
		{
			private KatalogUmetnin_dbDataSet.UmetnineStUmDelaDataTable tableUmetnineStUmDela;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string st_um_dela
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableUmetnineStUmDela.st_um_delaColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'st_um_dela' in table 'UmetnineStUmDela' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnineStUmDela.st_um_delaColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal UmetnineStUmDelaRow(DataRowBuilder rb) : base(rb)
			{
				this.tableUmetnineStUmDela = (KatalogUmetnin_dbDataSet.UmetnineStUmDelaDataTable)base.Table;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool Isst_um_delaNull()
			{
				return base.IsNull(this.tableUmetnineStUmDela.st_um_delaColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void Setst_um_delaNull()
			{
				base[this.tableUmetnineStUmDela.st_um_delaColumn] = Convert.DBNull;
			}
		}

		public class NapisiRow : DataRow
		{
			private KatalogUmetnin_dbDataSet.NapisiDataTable tableNapisi;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public int Nr
			{
				get
				{
					return (int)base[this.tableNapisi.NrColumn];
				}
				set
				{
					base[this.tableNapisi.NrColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public int IDNapisa
			{
				get
				{
					return (int)base[this.tableNapisi.IDNapisaColumn];
				}
				set
				{
					base[this.tableNapisi.IDNapisaColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string Tekst
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableNapisi.TekstColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'Tekst' in table 'Napisi' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableNapisi.TekstColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string Font
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableNapisi.FontColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'Font' in table 'Napisi' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableNapisi.FontColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string Color
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableNapisi.ColorColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'Color' in table 'Napisi' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableNapisi.ColorColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public int Size
			{
				get
				{
					int result;
					try
					{
						result = (int)base[this.tableNapisi.SizeColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'Size' in table 'Napisi' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableNapisi.SizeColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public int X
			{
				get
				{
					int result;
					try
					{
						result = (int)base[this.tableNapisi.XColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'X' in table 'Napisi' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableNapisi.XColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public int Y
			{
				get
				{
					int result;
					try
					{
						result = (int)base[this.tableNapisi.YColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'Y' in table 'Napisi' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableNapisi.YColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal NapisiRow(DataRowBuilder rb) : base(rb)
			{
				this.tableNapisi = (KatalogUmetnin_dbDataSet.NapisiDataTable)base.Table;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsTekstNull()
			{
				return base.IsNull(this.tableNapisi.TekstColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetTekstNull()
			{
				base[this.tableNapisi.TekstColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsFontNull()
			{
				return base.IsNull(this.tableNapisi.FontColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetFontNull()
			{
				base[this.tableNapisi.FontColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsColorNull()
			{
				return base.IsNull(this.tableNapisi.ColorColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetColorNull()
			{
				base[this.tableNapisi.ColorColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsSizeNull()
			{
				return base.IsNull(this.tableNapisi.SizeColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetSizeNull()
			{
				base[this.tableNapisi.SizeColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsXNull()
			{
				return base.IsNull(this.tableNapisi.XColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetXNull()
			{
				base[this.tableNapisi.XColumn] = Convert.DBNull;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsYNull()
			{
				return base.IsNull(this.tableNapisi.YColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetYNull()
			{
				base[this.tableNapisi.YColumn] = Convert.DBNull;
			}
		}

		public class UmetnineOpisRow : DataRow
		{
			private KatalogUmetnin_dbDataSet.UmetnineOpisDataTable tableUmetnineOpis;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public int Nr
			{
				get
				{
					return (int)base[this.tableUmetnineOpis.NrColumn];
				}
				set
				{
					base[this.tableUmetnineOpis.NrColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public string opis
			{
				get
				{
					string result;
					try
					{
						result = (string)base[this.tableUmetnineOpis.opisColumn];
					}
					catch (InvalidCastException e)
					{
						throw new StrongTypingException("The value for column 'opis' in table 'UmetnineOpis' is DBNull.", e);
					}
					return result;
				}
				set
				{
					base[this.tableUmetnineOpis.opisColumn] = value;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			internal UmetnineOpisRow(DataRowBuilder rb) : base(rb)
			{
				this.tableUmetnineOpis = (KatalogUmetnin_dbDataSet.UmetnineOpisDataTable)base.Table;
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public bool IsopisNull()
			{
				return base.IsNull(this.tableUmetnineOpis.opisColumn);
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public void SetopisNull()
			{
				base[this.tableUmetnineOpis.opisColumn] = Convert.DBNull;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
		public class UmetnineRowChangeEvent : EventArgs
		{
			private KatalogUmetnin_dbDataSet.UmetnineRow eventRow;

			private DataRowAction eventAction;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineRow Row
			{
				get
				{
					return this.eventRow;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataRowAction Action
			{
				get
				{
					return this.eventAction;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public UmetnineRowChangeEvent(KatalogUmetnin_dbDataSet.UmetnineRow row, DataRowAction action)
			{
				this.eventRow = row;
				this.eventAction = action;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
		public class SlikeRowChangeEvent : EventArgs
		{
			private KatalogUmetnin_dbDataSet.SlikeRow eventRow;

			private DataRowAction eventAction;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.SlikeRow Row
			{
				get
				{
					return this.eventRow;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataRowAction Action
			{
				get
				{
					return this.eventAction;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public SlikeRowChangeEvent(KatalogUmetnin_dbDataSet.SlikeRow row, DataRowAction action)
			{
				this.eventRow = row;
				this.eventAction = action;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
		public class UmetnineStUmDelaRowChangeEvent : EventArgs
		{
			private KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow eventRow;

			private DataRowAction eventAction;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow Row
			{
				get
				{
					return this.eventRow;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataRowAction Action
			{
				get
				{
					return this.eventAction;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public UmetnineStUmDelaRowChangeEvent(KatalogUmetnin_dbDataSet.UmetnineStUmDelaRow row, DataRowAction action)
			{
				this.eventRow = row;
				this.eventAction = action;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
		public class NapisiRowChangeEvent : EventArgs
		{
			private KatalogUmetnin_dbDataSet.NapisiRow eventRow;

			private DataRowAction eventAction;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.NapisiRow Row
			{
				get
				{
					return this.eventRow;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataRowAction Action
			{
				get
				{
					return this.eventAction;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public NapisiRowChangeEvent(KatalogUmetnin_dbDataSet.NapisiRow row, DataRowAction action)
			{
				this.eventRow = row;
				this.eventAction = action;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
		public class UmetnineOpisRowChangeEvent : EventArgs
		{
			private KatalogUmetnin_dbDataSet.UmetnineOpisRow eventRow;

			private DataRowAction eventAction;

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public KatalogUmetnin_dbDataSet.UmetnineOpisRow Row
			{
				get
				{
					return this.eventRow;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public DataRowAction Action
			{
				get
				{
					return this.eventAction;
				}
			}

			[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
			public UmetnineOpisRowChangeEvent(KatalogUmetnin_dbDataSet.UmetnineOpisRow row, DataRowAction action)
			{
				this.eventRow = row;
				this.eventAction = action;
			}
		}

		private KatalogUmetnin_dbDataSet.UmetnineDataTable tableUmetnine;

		private KatalogUmetnin_dbDataSet.SlikeDataTable tableSlike;

		private KatalogUmetnin_dbDataSet.UmetnineStUmDelaDataTable tableUmetnineStUmDela;

		private KatalogUmetnin_dbDataSet.NapisiDataTable tableNapisi;

		private KatalogUmetnin_dbDataSet.UmetnineOpisDataTable tableUmetnineOpis;

		private SchemaSerializationMode _schemaSerializationMode = SchemaSerializationMode.IncludeSchema;

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), DebuggerNonUserCode]
		public KatalogUmetnin_dbDataSet.UmetnineDataTable Umetnine
		{
			get
			{
				return this.tableUmetnine;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), DebuggerNonUserCode]
		public KatalogUmetnin_dbDataSet.SlikeDataTable Slike
		{
			get
			{
				return this.tableSlike;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), DebuggerNonUserCode]
		public KatalogUmetnin_dbDataSet.UmetnineStUmDelaDataTable UmetnineStUmDela
		{
			get
			{
				return this.tableUmetnineStUmDela;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), DebuggerNonUserCode]
		public KatalogUmetnin_dbDataSet.NapisiDataTable Napisi
		{
			get
			{
				return this.tableNapisi;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), DebuggerNonUserCode]
		public KatalogUmetnin_dbDataSet.UmetnineOpisDataTable UmetnineOpis
		{
			get
			{
				return this.tableUmetnineOpis;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), Browsable(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DebuggerNonUserCode]
		public override SchemaSerializationMode SchemaSerializationMode
		{
			get
			{
				return this._schemaSerializationMode;
			}
			set
			{
				this._schemaSerializationMode = value;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), DebuggerNonUserCode]
		public new DataTableCollection Tables
		{
			get
			{
				return base.Tables;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), DebuggerNonUserCode]
		public new DataRelationCollection Relations
		{
			get
			{
				return base.Relations;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public KatalogUmetnin_dbDataSet()
		{
			base.BeginInit();
			this.InitClass();
			CollectionChangeEventHandler schemaChangedHandler = new CollectionChangeEventHandler(this.SchemaChanged);
			base.Tables.CollectionChanged += schemaChangedHandler;
			base.Relations.CollectionChanged += schemaChangedHandler;
			base.EndInit();
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected KatalogUmetnin_dbDataSet(SerializationInfo info, StreamingContext context) : base(info, context, false)
		{
			if (base.IsBinarySerialized(info, context))
			{
				this.InitVars(false);
				CollectionChangeEventHandler schemaChangedHandler = new CollectionChangeEventHandler(this.SchemaChanged);
				this.Tables.CollectionChanged += schemaChangedHandler;
				this.Relations.CollectionChanged += schemaChangedHandler;
			}
			else
			{
				string strSchema = (string)info.GetValue("XmlSchema", typeof(string));
				if (base.DetermineSchemaSerializationMode(info, context) == SchemaSerializationMode.IncludeSchema)
				{
					DataSet ds = new DataSet();
					ds.ReadXmlSchema(new XmlTextReader(new StringReader(strSchema)));
					if (ds.Tables["Umetnine"] != null)
					{
						base.Tables.Add(new KatalogUmetnin_dbDataSet.UmetnineDataTable(ds.Tables["Umetnine"]));
					}
					if (ds.Tables["Slike"] != null)
					{
						base.Tables.Add(new KatalogUmetnin_dbDataSet.SlikeDataTable(ds.Tables["Slike"]));
					}
					if (ds.Tables["UmetnineStUmDela"] != null)
					{
						base.Tables.Add(new KatalogUmetnin_dbDataSet.UmetnineStUmDelaDataTable(ds.Tables["UmetnineStUmDela"]));
					}
					if (ds.Tables["Napisi"] != null)
					{
						base.Tables.Add(new KatalogUmetnin_dbDataSet.NapisiDataTable(ds.Tables["Napisi"]));
					}
					if (ds.Tables["UmetnineOpis"] != null)
					{
						base.Tables.Add(new KatalogUmetnin_dbDataSet.UmetnineOpisDataTable(ds.Tables["UmetnineOpis"]));
					}
					base.DataSetName = ds.DataSetName;
					base.Prefix = ds.Prefix;
					base.Namespace = ds.Namespace;
					base.Locale = ds.Locale;
					base.CaseSensitive = ds.CaseSensitive;
					base.EnforceConstraints = ds.EnforceConstraints;
					base.Merge(ds, false, MissingSchemaAction.Add);
					this.InitVars();
				}
				else
				{
					base.ReadXmlSchema(new XmlTextReader(new StringReader(strSchema)));
				}
				base.GetSerializationData(info, context);
				CollectionChangeEventHandler schemaChangedHandler2 = new CollectionChangeEventHandler(this.SchemaChanged);
				base.Tables.CollectionChanged += schemaChangedHandler2;
				this.Relations.CollectionChanged += schemaChangedHandler2;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected override void InitializeDerivedDataSet()
		{
			base.BeginInit();
			this.InitClass();
			base.EndInit();
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public override DataSet Clone()
		{
			KatalogUmetnin_dbDataSet cln = (KatalogUmetnin_dbDataSet)base.Clone();
			cln.InitVars();
			cln.SchemaSerializationMode = this.SchemaSerializationMode;
			return cln;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected override bool ShouldSerializeTables()
		{
			return false;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected override bool ShouldSerializeRelations()
		{
			return false;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected override void ReadXmlSerializable(XmlReader reader)
		{
			if (base.DetermineSchemaSerializationMode(reader) == SchemaSerializationMode.IncludeSchema)
			{
				this.Reset();
				DataSet ds = new DataSet();
				ds.ReadXml(reader);
				if (ds.Tables["Umetnine"] != null)
				{
					base.Tables.Add(new KatalogUmetnin_dbDataSet.UmetnineDataTable(ds.Tables["Umetnine"]));
				}
				if (ds.Tables["Slike"] != null)
				{
					base.Tables.Add(new KatalogUmetnin_dbDataSet.SlikeDataTable(ds.Tables["Slike"]));
				}
				if (ds.Tables["UmetnineStUmDela"] != null)
				{
					base.Tables.Add(new KatalogUmetnin_dbDataSet.UmetnineStUmDelaDataTable(ds.Tables["UmetnineStUmDela"]));
				}
				if (ds.Tables["Napisi"] != null)
				{
					base.Tables.Add(new KatalogUmetnin_dbDataSet.NapisiDataTable(ds.Tables["Napisi"]));
				}
				if (ds.Tables["UmetnineOpis"] != null)
				{
					base.Tables.Add(new KatalogUmetnin_dbDataSet.UmetnineOpisDataTable(ds.Tables["UmetnineOpis"]));
				}
				base.DataSetName = ds.DataSetName;
				base.Prefix = ds.Prefix;
				base.Namespace = ds.Namespace;
				base.Locale = ds.Locale;
				base.CaseSensitive = ds.CaseSensitive;
				base.EnforceConstraints = ds.EnforceConstraints;
				base.Merge(ds, false, MissingSchemaAction.Add);
				this.InitVars();
			}
			else
			{
				base.ReadXml(reader);
				this.InitVars();
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		protected override XmlSchema GetSchemaSerializable()
		{
			MemoryStream stream = new MemoryStream();
			base.WriteXmlSchema(new XmlTextWriter(stream, null));
			stream.Position = 0L;
			return XmlSchema.Read(new XmlTextReader(stream), null);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		internal void InitVars()
		{
			this.InitVars(true);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		internal void InitVars(bool initTable)
		{
			this.tableUmetnine = (KatalogUmetnin_dbDataSet.UmetnineDataTable)base.Tables["Umetnine"];
			if (initTable)
			{
				if (this.tableUmetnine != null)
				{
					this.tableUmetnine.InitVars();
				}
			}
			this.tableSlike = (KatalogUmetnin_dbDataSet.SlikeDataTable)base.Tables["Slike"];
			if (initTable)
			{
				if (this.tableSlike != null)
				{
					this.tableSlike.InitVars();
				}
			}
			this.tableUmetnineStUmDela = (KatalogUmetnin_dbDataSet.UmetnineStUmDelaDataTable)base.Tables["UmetnineStUmDela"];
			if (initTable)
			{
				if (this.tableUmetnineStUmDela != null)
				{
					this.tableUmetnineStUmDela.InitVars();
				}
			}
			this.tableNapisi = (KatalogUmetnin_dbDataSet.NapisiDataTable)base.Tables["Napisi"];
			if (initTable)
			{
				if (this.tableNapisi != null)
				{
					this.tableNapisi.InitVars();
				}
			}
			this.tableUmetnineOpis = (KatalogUmetnin_dbDataSet.UmetnineOpisDataTable)base.Tables["UmetnineOpis"];
			if (initTable)
			{
				if (this.tableUmetnineOpis != null)
				{
					this.tableUmetnineOpis.InitVars();
				}
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void InitClass()
		{
			base.DataSetName = "KatalogUmetnin_dbDataSet";
			base.Prefix = "";
			base.Namespace = "http://tempuri.org/KatalogUmetnin_dbDataSet.xsd";
			base.EnforceConstraints = true;
			this.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;
			this.tableUmetnine = new KatalogUmetnin_dbDataSet.UmetnineDataTable();
			base.Tables.Add(this.tableUmetnine);
			this.tableSlike = new KatalogUmetnin_dbDataSet.SlikeDataTable();
			base.Tables.Add(this.tableSlike);
			this.tableUmetnineStUmDela = new KatalogUmetnin_dbDataSet.UmetnineStUmDelaDataTable();
			base.Tables.Add(this.tableUmetnineStUmDela);
			this.tableNapisi = new KatalogUmetnin_dbDataSet.NapisiDataTable();
			base.Tables.Add(this.tableNapisi);
			this.tableUmetnineOpis = new KatalogUmetnin_dbDataSet.UmetnineOpisDataTable();
			base.Tables.Add(this.tableUmetnineOpis);
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private bool ShouldSerializeUmetnine()
		{
			return false;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private bool ShouldSerializeSlike()
		{
			return false;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private bool ShouldSerializeUmetnineStUmDela()
		{
			return false;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private bool ShouldSerializeNapisi()
		{
			return false;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private bool ShouldSerializeUmetnineOpis()
		{
			return false;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		private void SchemaChanged(object sender, CollectionChangeEventArgs e)
		{
			if (e.Action == CollectionChangeAction.Remove)
			{
				this.InitVars();
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "4.0.0.0"), DebuggerNonUserCode]
		public static XmlSchemaComplexType GetTypedDataSetSchema(XmlSchemaSet xs)
		{
			KatalogUmetnin_dbDataSet ds = new KatalogUmetnin_dbDataSet();
			XmlSchemaComplexType type = new XmlSchemaComplexType();
			XmlSchemaSequence sequence = new XmlSchemaSequence();
			XmlSchemaAny any = new XmlSchemaAny();
			any.Namespace = ds.Namespace;
			sequence.Items.Add(any);
			type.Particle = sequence;
			XmlSchema dsSchema = ds.GetSchemaSerializable();
			XmlSchemaComplexType result;
			if (xs.Contains(dsSchema.TargetNamespace))
			{
				MemoryStream s = new MemoryStream();
				MemoryStream s2 = new MemoryStream();
				try
				{
					dsSchema.Write(s);
					IEnumerator schemas = xs.Schemas(dsSchema.TargetNamespace).GetEnumerator();
					while (schemas.MoveNext())
					{
						XmlSchema schema = (XmlSchema)schemas.Current;
						s2.SetLength(0L);
						schema.Write(s2);
						if (s.Length == s2.Length)
						{
							s.Position = 0L;
							s2.Position = 0L;
							while (s.Position != s.Length && s.ReadByte() == s2.ReadByte())
							{
							}
							if (s.Position == s.Length)
							{
								result = type;
								return result;
							}
						}
					}
				}
				finally
				{
					if (s != null)
					{
						s.Close();
					}
					if (s2 != null)
					{
						s2.Close();
					}
				}
			}
			xs.Add(dsSchema);
			result = type;
			return result;
		}
	}
}
