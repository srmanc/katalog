using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace KatalogUmetnin
{
	public class ImageCanvasUC : System.Windows.Forms.UserControl
	{
		private IContainer components = null;

		public ImageCanvasUC()
		{
			this.InitializeComponent();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			base.SuspendLayout();
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.MaximumSize = new System.Drawing.Size(440, 618);
			this.MinimumSize = new System.Drawing.Size(440, 618);
			base.Name = "ImageCanvasUC";
			base.Size = new System.Drawing.Size(440, 618);
			base.ResumeLayout(false);
		}
	}
}
